This is an assignment in Principles Of Programming Languages course in 2018.

This figure represents the result of assignment. It means that I've built a compiler for MP programming language. The left window is the program written by MP language, while the right one is the result of program after running.

![IMAGE_DESCRIPTION](./PPL_demo.png)