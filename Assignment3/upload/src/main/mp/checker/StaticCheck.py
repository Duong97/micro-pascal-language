
"""
 * @author nhphung
"""
from AST import *
from Visitor import *
from Utils import Utils
from StaticError import *
from functools import reduce

class MType:
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype
    def __str__(self):
        return 'MType(['+','.join(str(i) for i in self.partype)+']'+','+str(self.rettype)+')'

class Symbol:
    def __init__(self,name,mtype,value = None):
        self.name = name
        self.mtype = mtype
        self.value = value
    def __str__(self):
        return 'Symbol('+self.name+','+str(self.mtype)+')'

class StaticChecker(BaseVisitor,Utils):
    check_global = [] #Place to storage initial names of variable and function before running program
    check_array = 0 #check return type of array type
    check_program = 0 #Switch to import initial names of variable and function before running program
    check_loop = 0 #check appearance of for and while statement
   
    Function_return = [] #Place to storage initial return type of function or procedure
    Function_name = [] #Place to storage initial name of function or procedure
    Function_param = [] ##Place to storage initial parameter of function or procedure
    Function_returnCurrent = [] #Hold the current return type of function or procedure 
    Function_nameCurrent = [] #Hold the current name of function or procedure 
    Function_use = [] #The number of using which function or procedure is used in program
    
    global_envi = [Symbol("getInt",MType([],IntType())),
                   Symbol("putInt",MType([IntType()],VoidType())),
                   Symbol("putIntLn",MType([IntType()],VoidType())),
                   Symbol("getFloat",MType([],FloatType())),
                   Symbol("putFloat",MType([FloatType()],VoidType())),
                   Symbol("putFloatLn",MType([FloatType()],VoidType())),
                   Symbol("putBool",MType([BoolType()],VoidType())),
                   Symbol("putBoolLn",MType([BoolType()],VoidType())),
                   Symbol("putString",MType([StringType()],VoidType())),
                   Symbol("putStringLn",MType([StringType()],VoidType())),
                   Symbol("putLn",MType([],VoidType()))]

    def __init__(self,ast):
        self.ast = ast

    def checkRedeclared(self, sym, kind, env):
        if self.lookup(sym.name, env, lambda x: x.name):
            raise Redeclared(kind, sym.name)
        else:
            return sym

    def check(self):
        #Initial setup storage
        StaticChecker.check_global = [] 
        StaticChecker.check_array = 0 
        StaticChecker.check_program = 0 
        StaticChecker.check_loop = 0 
       
        StaticChecker.Function_return = [] 
        StaticChecker.Function_name = [] 
        StaticChecker.Function_param = [] 
        StaticChecker.Function_returnCurrent = [] 
        StaticChecker.Function_nameCurrent = [] 
        StaticChecker.Function_use = []
    
        #Start running program
        #print('START')
        #Start import initile variables and functions
        temp = self.visit(self.ast,StaticChecker.global_envi)

        #Running Program offcially
        StaticChecker.check_program = 1
        StaticChecker.check_global = StaticChecker.global_envi + StaticChecker.check_global
        temp = self.visit(self.ast,StaticChecker.check_global)

        #check appearance of main function
        check_main = 0
        for x in zip(StaticChecker.Function_name, StaticChecker.Function_param, StaticChecker.Function_return):
            if x[0] == 'main' and x[1] == [] and type(x[2]) is VoidType: 
                check_main = 1
        if check_main == 0:
            raise NoEntryPoint()

        #check the number of using of each function and procedure
        for x in zip(StaticChecker.Function_name, StaticChecker.Function_use, StaticChecker.Function_return):
            if x[0] != 'main' and x[1] == 0:
                if x[2] is not VoidType:
                    kind =  Function()
                else:
                    kind = Procedure()
                raise Unreachable(kind,x[0])

        #Finishing program
        #print('STOP')
        return temp
        
    def visitProgram(self,ast, c):
        return reduce(lambda x,y: [self.visit(y,x+c)] + x, ast.decl, [])
        
    def visitVarDecl(self,ast, c):
        variable = Symbol(ast.variable.name, ast.varType)
        #Starting import initial variables
        if StaticChecker.check_program == 0:
            temp = self.checkRedeclared(variable, Variable(), c)
            StaticChecker.check_global.append(variable) 
        return variable
       
    def visitFuncDecl(self,ast, c):
        Func_kind = Procedure() if type(ast.returnType) is VoidType else Function()
        function = Symbol(ast.name.name,MType([x.varType for x in ast.param], ast.returnType))

        ##Starting import initial Functions and procedures
        if StaticChecker.check_program == 0:
            StaticChecker.Function_use.append(0)
            StaticChecker.Function_name.append(ast.name.name)
            StaticChecker.Function_return.append(ast.returnType)
            StaticChecker.Function_param.append([x.varType for x in ast.param])
            StaticChecker.check_global.append(function)
            temp =  self.checkRedeclared(function,Func_kind,c)

        else:
            StaticChecker.Function_returnCurrent.append(ast.returnType)
            StaticChecker.Function_nameCurrent.append(ast.name.name)

            check = []
            Func_param = list(map(lambda x: Symbol(x.variable.name, x.varType), ast.param))
            for param in Func_param:
                param_temp = self.checkRedeclared(param,Parameter(),check)
                check.append(param_temp)
            Func_local = list(map(lambda x: Symbol(x.variable.name, x.varType), ast.local))
            for local in Func_local:
                param_local = self.checkRedeclared(local, Variable(), check)
                check.append(param_local)

            body = list(map(lambda x: self.visit(x,check + StaticChecker.check_global), ast.body))

        return function

    def visitIf(self,ast,c):
        expr = self.visit(ast.expr,c)
        if type(expr) is not BoolType:
            raise TypeMismatchInStatement(ast)

        thenStmt = list(map(lambda x: self.visit(x,c), ast.thenStmt))
       
        elseStmt = list(map(lambda x: self.visit(x,c), ast.elseStmt))
       
    def visitReturn(self,ast,c):
        StaticChecker.check_array = 1

        Return_Type = list(reversed(StaticChecker.Function_returnCurrent))
        Not_return = list(reversed(StaticChecker.Function_nameCurrent))
        
        if ast.expr:
            expr = self.visit(ast.expr,c)
        else:
            expr = None

        if type(Return_Type[0]) is not ArrayType:
            if expr is None:
                if type(Return_Type[0]) is not VoidType:
                    raise TypeMismatchInStatement(ast)
            else:
                if type(Return_Type[0]) is not type(expr):
                    raise TypeMismatchInStatement(ast)
        else:
            if expr is None:
                raise TypeMismatchInStatement(ast)
            else:
                if type(expr) is not ArrayType:
                    raise TypeMismatchInStatement(ast)
                else:
                    if not (Return_Type[0].upper == expr.upper and Return_Type[0].lower == expr.lower and Return_Type[0].eleType == expr.eleType):
                        raise TypeMismatchInStatement(ast)

        StaticChecker.check_array = 0

    def visitFor(self, ast, c):
        StaticChecker.check_loop = 1
        
        Id = self.visit(ast.id,c)
        expr1 = self.visit(ast.expr1,c)
        expr2 = self.visit(ast.expr2,c)
        if not (type(Id) == type(expr1) and type(expr1) == type(expr2) and type(expr2) is IntType):
            raise TypeMismatchInStatement(ast)
        loop = list(map(lambda x: self.visit(x,c), ast.loop))

        StaticChecker.check_loop = 0

    def visitWhile(self,ast,c):
        StaticChecker.check_loop = 1
        
        exp = self.visit(ast.exp,c)
        if type(exp) is not BoolType:
            raise TypeMismatchInStatement(ast)
        sl = list(map(lambda x: self.visit(x,c), ast.sl))
       
        StaticChecker.check_loop = 0

    def visitWith(self,ast,c):
        Func_param = list(map(lambda x: Symbol(x.variable.name, x.varType), ast.decl))
        check = []
        for param in Func_param:
            param_temp = self.checkRedeclared(param,Variable(),check)
            check.append(param_temp)

        decl = list(map(lambda x: self.visit(x,[]), ast.decl))
        stmt1 = list(map(lambda x: self.visit(x,decl+c), ast.stmt))

    def visitArrayType(self,ast,c):
        lower = self.visit(ast.lower,c)
        upper = self.visit(ast.upper,c)
        eleType = self.visit(ast.eleType)

    def visitArrayCell(self,ast,c):
        arr = self.visit(ast.arr,c)
        idx = self.visit(ast.idx,c)
        
        if (type(arr) is ArrayType):
            temp = type(arr.eleType)
        else:
            temp = None

        if not (type(arr) is ArrayType and type(idx) is IntType):
            raise TypeMismatchInExpression(ast)
        else:
            if StaticChecker.check_array == 0:
                if temp is IntType:
                    return IntType()
                elif temp is FloatType:
                    return FloatType()
                elif temp is StringType:
                    return StringType()
                elif temp is BoolType:
                    return BoolType()
            else:
                return arr
            
    def visitCallExpr(self, ast, c):
        StaticChecker.check_array = 1

        res = self.lookup(ast.method.name, c, lambda x: x.name)
        if not res or type(res.mtype) is not MType or type(res.mtype.rettype) is VoidType:
            raise Undeclared(Function(), ast.method.name)
        elif len(res.mtype.partype) != len(ast.param):
            raise TypeMismatchInExpression(ast)
        else:
            temp = list(zip([self.visit(p, c) for p in ast.param], res.mtype.partype))
            for x in temp:
                if not (type(x[0]) == type(x[1])):
                    raise TypeMismatchInExpression(ast)
                elif type(x[0]) is ArrayType and type(x[0]) == type(x[1]):
                    if not (x[0].lower == x[1].lower and x[0].upper == x[1].upper):

                        raise TypeMismatchInExpression(ast)
        
        F = StaticChecker.Function_name
        U = StaticChecker.Function_use
        recursive = len(StaticChecker.Function_nameCurrent) - 1
        for x in range(0, len(F)):

            if F[x] == ast.method.name and x != recursive:
                U[x] +=1
            
        StaticChecker.check_array = 0
        
        return res.mtype.rettype
        
    def visitCallStmt(self, ast, c):
        StaticChecker.check_array = 1

        res = self.lookup(ast.method.name, c, lambda x: x.name)
        if res is None or type(res.mtype) is not MType:
            raise Undeclared(Procedure(), ast.method.name)
        elif len(res.mtype.partype) != len(ast.param):
            raise TypeMismatchInStatement(ast)

        else:
            temp = list(zip([self.visit(p, c) for p in ast.param], res.mtype.partype))
            for x in temp:
                if not (type(x[0]) == type(x[1])):
                    raise TypeMismatchInStatement(ast)
                elif type(x[0]) is ArrayType and type(x[0]) == type(x[1]):
                    if not (x[0].lower == x[1].lower and x[0].upper == x[1].upper):
                        raise TypeMismatchInStatement(ast)

        F = StaticChecker.Function_name
        U = StaticChecker.Function_use
        recursive = len(StaticChecker.Function_nameCurrent) - 1
        for x in range(0, len(F)):
            if F[x] == ast.method.name and x != recursive:
                U[x] +=1

        StaticChecker.check_array = 0

    def visitId(self, ast, c):
        res = self.lookup(ast.name, c, lambda x: x.name)
        if res is None or type(res.mtype) is MType:
            raise Undeclared(Identifier(), ast.name)
        return res.mtype

    def visitAssign(self,ast,c):
        lhs = self.visit(ast.lhs,c)
        exp = self.visit(ast.exp,c)
        
        if type(exp) is not ArrayType:
            if type(lhs) is not type (exp) and not(type(lhs) is FloatType and type(exp) is IntType):
                raise TypeMismatchInStatement(ast)
        else:
            if type(lhs) is not type (exp.eleType) and not(type(lhs) is FloatType and type(exp.eleType) is IntType):
                raise TypeMismatchInStatement(ast)

    def visitBreak(self,ast,c):
        if (StaticChecker.check_loop == 0):
            raise BreakNotInLoop()
        else:
            return Break()

    def visitContinue(self,ast,c):
        if (StaticChecker.check_loop == 0):
            raise ContinueNotInLoop()
        else:
            return Continue()

    def visitIntLiteral(self,ast, c):
        return IntType()

    def visitFloatLiteral(self,ast, c):
        return FloatType()

    def visitStringLiteral(self,ast, c):
        return StringType()

    def visitBooleanLiteral(self,ast, c):
        return BoolType()

    def visitBinaryOp(self,ast,c):
        left = self.visit(ast.left, c)
        right = self.visit(ast.right, c)
        if ast.op == "+" or ast.op == "-" or ast.op == "*":
            if type(left) is IntType and type(right) is IntType:
                return IntType()
            elif type(left) is IntType and type(right) is FloatType:
                return FloatType()
            elif type(left) is FloatType and type(right) is IntType:
                return FloatType()
            elif type(left) is FloatType and type(right) is FloatType:
                return FloatType()
            else:
                raise TypeMismatchInExpression(ast)

        elif ast.op == "div" or ast.op == "mod":
            if type(left) is IntType and type(right) is IntType:
                return IntType()
            else:
                raise TypeMismatchInExpression(ast)

        elif ast.op == "<" or ast.op == "<=" or ast.op == ">" or ast.op == ">=" or ast.op == "<>" or ast.op == "=":
            if type(left) is IntType and type(right) is IntType:
                return BoolType()
            elif type(left) is IntType and type(right) is FloatType:
                return BoolType()
            elif type(left) is FloatType and type(right) is IntType:
                return BoolType()
            elif type(left) is FloatType and type(right) is FloatType:
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)

        elif ast.op == "/":
            if type(left) is IntType and type(right) is IntType:
                return FloatType()
            elif type(left) is IntType and type(right) is FloatType:
                return FloatType()
            elif type(left) is FloatType and type(right) is IntType:
                return FloatType()
            elif type(left) is FloatType and type(right) is FloatType:
                return FloatType()
            else:
                raise TypeMismatchInExpression(ast)
        else:
            if type(left) is BoolType and type(right) is BoolType:
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)

    def visitUnaryOp(self,ast,c):
        body = self.visit(ast.body,c)
        if ast.op == "-":
            if type(body) is IntType: 
                return IntType()
            elif type(body) is FloatType:
                return FloatType()
            else:
                raise TypeMismatchInExpression(ast)

        else:
            if type(body) is BoolType:
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)