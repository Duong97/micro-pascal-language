from MPVisitor import MPVisitor
from MPParser import MPParser
from AST import *

def flatten(S): 
    if S == []: 
        return S 
    if isinstance(S[0], list): 
        return flatten(S[0]) + flatten(S[1:]) 
    return S[:1] + flatten(S[1:])

class ASTGeneration(MPVisitor):
    #program: deff+ EOF;
    def visitProgram(self,ctx:MPParser.ProgramContext):
        return Program(flatten([self.visit(x) for x in ctx.deff()]))

    #deff: vardec | funcdec | procdec;
    def visitDeff(self, ctx:MPParser.DeffContext):
        return self.visit(ctx.getChild(0))

    #nor_type: INTTYPE|BOOLEANTYPE|STRINGTYPE|FLOATTYPE;
    def visitNor_type(self,ctx:MPParser.Nor_typeContext):
        if ctx.INTTYPE(): return IntType()
        elif ctx.BOOLEANTYPE(): return BoolType()
        elif ctx.STRINGTYPE(): return StringType()
        elif ctx.FLOATTYPE(): return FloatType()

    #arraytype: ARRAY LSB expr DOUBLEDOT expr RSB OF nor_type;
    def visitArraytype(self,ctx:MPParser.ArraytypeContext):
        return ArrayType(int(ctx.getChild(2).getText()), int(ctx.getChild(4).getText()), self.visit(ctx.nor_type()))

    #vartype: nor_type|arraytype;
    def visitVartype(self,ctx:MPParser.VartypeContext):
        return self.visit(ctx.getChild(0))

    #vardec: VAR (varlist SEMI)+;
    def visitVardec(self,ctx:MPParser.VardecContext):
        return flatten([self.visit(x) for x in ctx.varlist()])

    #varlist: id_list COLON vartype;
    def visitVarlist(self,ctx:MPParser.VarlistContext):
        id_list = self.visit(ctx.id_list())
        vartype = self.visit(ctx.vartype())
        return list(map(lambda x: VarDecl(x, vartype),id_list))

    #id_list: ID (COMMA ID)*;
    def visitId_list(self,ctx:MPParser.Id_listContext):
        return [Id(x.getText()) for x in ctx.ID()]
    
    #funcdec: FUNCTION ID LB (paradec)* RB COLON vartype SEMI vardec? body;
    def visitFuncdec(self,ctx:MPParser.FuncdecContext):
        if ctx.paradec():
            param = [self.visit(x) for x in ctx.paradec()]
        else:
            param = []
 
        if ctx.vardec(): 
            local = self.visit(ctx.vardec())
        else:
            local = []
        
        body = self.visit(ctx.body())
        vartype = self.visit(ctx.vartype())
        return FuncDecl(Id(ctx.ID().getText()), flatten(param), local, flatten(body), vartype)

    #procdec: PROCEDURE ID LB (paradec)* RB SEMI vardec? body;
    def visitProcdec(self,ctx:MPParser.ProcdecContext):
        if ctx.paradec():
            param = [self.visit(x) for x in ctx.paradec()]
        else:
            param = []
 
        if ctx.vardec(): 
            local = self.visit(ctx.vardec())
        else:
            local = []

        body = self.visit(ctx.body())
        return FuncDecl(Id(ctx.ID().getText()), flatten(param), local, flatten(body))

    #paradec: varlist SEMI?;
    def visitParadec(self,ctx:MPParser.ParadecContext):
        return self.visit(ctx.varlist())

    #body: BEGIN statement* END;
    def visitBody(self,ctx:MPParser.BodyContext):
        if ctx.statement():
            return [self.visit(x) for x in ctx.statement()]
        else: 
            return []

    #statement: funcall  | assign_statement  | if_s | while_s | for_s | br_s  | cont_s  | rt_s  | with_s | body;
    def visitStatement(self,ctx:MPParser.StatementContext):
        return self.visit(ctx.getChild(0))

    #assign_statement: lhs expr SEMI;
    def visitAssign_statement(self,ctx:MPParser.Assign_statementContext):
        values = self.visit(ctx.lhs())
        values.append(self.visit(ctx.expr()))
        temp = list(map(lambda x, y: Assign(x, y),values, values[1:]))
        
        return list(reversed(temp))
        
    #lhs: (expr OP16)+;
    def visitLhs(self,ctx:MPParser.LhsContext):
        return [self.visit(x) for x in ctx.expr()]

    #if_s: IF expr THEN statement (ELSE statement)?;
    def visitIf_s(self,ctx:MPParser.If_sContext):
        if (ctx.getChildCount() == 4):
            return If(self.visit(ctx.expr()), flatten([self.visit(x) for x in ctx.statement()]))
        else:
            return If(self.visit(ctx.expr()), flatten([self.visit(ctx.getChild(3))]), flatten([self.visit(ctx.getChild(5))]))

    #while_s: WHILE expr DO statement;
    def visitWhile_s(self,ctx:MPParser.While_sContext):
        return While(self.visit(ctx.expr()), flatten([self.visit(ctx.statement())]))

    #for_s: FOR ID OP16 expr (TO | DOWNTO) expr DO statement;
    def visitFor_s(self,ctx:MPParser.For_sContext):
        if ctx.TO():
            eval = True
        elif ctx.DOWNTO():
            eval = False
        return For(Id(ctx.ID().getText()), self.visit(ctx.getChild(3)), self.visit(ctx.getChild(5)), eval , flatten([self.visit(ctx.statement())]))

    #with_s: WITH (varlist SEMI)+ DO statement;
    def visitWith_s(self,ctx:MPParser.With_sContext):
        return With(flatten([self.visit(x) for x in ctx.varlist()]), flatten([self.visit(ctx.statement())]))

    #funcall: ID LB func_list? RB SEMI;
    def visitFuncall(self,ctx:MPParser.FuncallContext):
        if ctx.func_list():
            values = [self.visit(ctx.func_list())]  
        else:
            values = []
        return CallStmt(Id(ctx.ID().getText()),flatten(values))

    #exprcall: ID LB func_list? RB;
    def visitExprcall(self,ctx:MPParser.ExprcallContext):
        if ctx.func_list():
            values = [self.visit(ctx.func_list())]  
        else:
            values = []
        return CallExpr(Id(ctx.ID().getText()),flatten(values))

    #func_list: expr (COMMA expr)*;
    def visitFunc_list(self,ctx:MPParser.Func_listContext):
        return [self.visit(x) for x in ctx.expr()]

    #br_s: BREAK SEMI;
    def visitBr_s(self,ctx:MPParser.Br_sContext):
        return Break()

    #cont_s: CONTINUE SEMI;
    def visitCont_s(self,ctx:MPParser.Cont_sContext):
        return Continue()

    def visitRt_s(self,ctx:MPParser.Rt_sContext):
        return Return((self.visit(ctx.expr()) if ctx.expr() else None))

    #expr: expr (OP7 ELSE | OP8 THEN) expr0 | expr0;
    def visitExpr(self,ctx:MPParser.ExprContext):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return BinaryOp(ctx.getChild(1).getText().lower() + ctx.getChild(2).getText().lower(), self.visit(ctx.getChild(0)), self.visit(ctx.getChild(3)))

    #expr0: expr1 (OP9 | OP10 | OP11 | OP12 | OP13 | OP14) expr1 | expr1;
    def visitExpr0(self,ctx:MPParser.Expr0Context):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.getChild(0)), self.visit(ctx.getChild(2)))
    
    #expr1: expr1 (OP1 | OP2 | OP7) expr2 | expr2;
    def visitExpr1(self,ctx:MPParser.Expr1Context):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.getChild(0)), self.visit(ctx.getChild(2)))

    #expr2: expr2 (OP4 | OP3 | OP6 | OP8 | OP15) expr3 | expr3;
    def visitExpr2(self,ctx:MPParser.Expr2Context):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.getChild(0)), self.visit(ctx.getChild(2)))

    #expr3: (OP2 | OP5) expr3  | expr4;
    def visitExpr3(self,ctx:MPParser.Expr3Context):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return UnaryOp(ctx.getChild(0).getText(), self.visit(ctx.getChild(1)))

    #expr4: expr5 LSB expr RSB | expr5;
    def visitExpr4(self,ctx:MPParser.Expr4Context):
        if (ctx.getChildCount() == 1):
            return self.visit(ctx.getChild(0))
        else:
            return ArrayCell(self.visit(ctx.getChild(0)), self.visit(ctx.getChild(2)))

    #expr5: LB expr RB | ID | funcall | exprcall | literal;
    def visitExpr5(self,ctx:MPParser.Expr5Context):
        if (ctx.getChildCount() == 1):
            if ctx.ID():
                return Id(ctx.ID().getText())
            else:
                return self.visit(ctx.getChild(0))
        else:
            return self.visit(ctx.getChild(1))

    #literal: INTLIT | FLOATLIT | BOOLLIT | STRINGLIT;
    def visitLiteral(self,ctx:MPParser.LiteralContext):
        if ctx.INTLIT(): 
            return IntLiteral(ctx.INTLIT().getText())
        elif ctx.BOOLLIT(): 
            return BooleanLiteral(ctx.BOOLLIT().getText())
        elif ctx.STRINGLIT(): 
            return StringLiteral(ctx.STRINGLIT().getText())
        elif ctx.FLOATLIT(): 
            return FloatLiteral(ctx.FLOATLIT().getText())