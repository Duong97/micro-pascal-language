import unittest
from TestUtils import TestChecker
from AST import *

class CheckerSuite(unittest.TestCase):
    def test_global_variable_redeclare(self):
        input = """var a: integer; a: real;"""

        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input,expect,401))

    def test_global_variable_redeclare2(self):
        input = """var a, a: integer;"""

        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input,expect,402))

    def test_global_variable_redeclare3(self):
        input = """var a: integer;
        var a: string;"""

        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input,expect,403))

    def test_global_variable_redeclare4(self):
        input = """var a: integer;
        function foo(): integer;
        begin
            return 1;
        end

        procedure main ();
        begin
        end
        var a: real;
        """

        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input,expect,404))

    def test_function_redeclare(self):
        input = """function foo(): integer;
        begin
            return 1;
        end

        function foo(): integer;
        begin
            return 1;
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,405))

    def test_function_redeclare2(self):
        input = """function foo(): integer;
        begin
            return 1;
        end

        function foo(a: integer): integer;
        begin
            return 1;
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,406))

    def test_function_redeclare3(self):
        input = """var foo: integer;

        function foo(a: integer): integer;
        begin
            return 1;
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,407))

    def test_function_redeclare4(self):
        input = """procedure foo();
        begin
        end

        function foo(a: integer): integer;
        begin
            return 1;
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,408))

    def test_procedure_redeclare(self):
        input = """procedure foo();
        begin
        end

        procedure foo();
        begin
            
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,409))

    def test_procedure_redeclare2(self):
        input = """procedure foo();
        begin
        end

        procedure foo(a: integer);
        begin
            
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,410))

    def test_procedure_redeclare3(self):
        input = """var foo: integer;

        procedure foo(a: integer);
        begin
            
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,411))

    def test_procedure_redeclare4(self):
        input = """function foo(a: integer): integer;
        begin
            return 1;
        end
        procedure foo(a: integer);
        begin
            
        end

        procedure main ();
        begin
        end
        """

        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,412))

    def test_parameter_redeclare(self):
        input = """function foo(a: integer; a: real): integer;
        begin
            return 1;
        end
        
        procedure main ();
        begin
        end
        """

        expect = "Redeclared Parameter: a"
        self.assertTrue(TestChecker.test(input,expect,413))

    def test_parameter_redeclare2(self):
        input = """function foo(a: integer; b: string; a: real): integer;

        begin
            return 1;
        end
        
        procedure main ();
        begin
        end
        """

        expect = "Redeclared Parameter: a"
        self.assertTrue(TestChecker.test(input,expect,414))

    def test_parameter_redeclare3(self):
        input = """procedure foo(a: integer; a: real);

        begin

        end
        
        procedure main ();
        begin
        end
        """

        expect = "Redeclared Parameter: a"
        self.assertTrue(TestChecker.test(input,expect,415))

    def test_parameter_redeclare4(self):
        input = """procedure foo(a: array [1 .. 2] of integer; a: real);

        begin

        end
        
        procedure main ();
        begin
        end
        """

        expect = "Redeclared Parameter: a"
        self.assertTrue(TestChecker.test(input,expect,416))

    def test_identifier_undeclare(self):
        input = """
        procedure main ();
        begin
            i := 1;
        end
        """

        expect = "Undeclared Identifier: i"
        self.assertTrue(TestChecker.test(input,expect,417))

    def test_identifier_undeclare2(self):
        input = """
        function foo (): integer;
        begin
            i := 1;
            return 1;
        end

        procedure main ();
        
        begin
          
        end
        """

        expect = "Undeclared Identifier: i"
        self.assertTrue(TestChecker.test(input,expect,418))

    def test_identifier_undeclare3(self):
        input = """
        procedure main ();
        
        begin
            if i = 1 then return 2;
          
        end
        """

        expect = "Undeclared Identifier: i"
        self.assertTrue(TestChecker.test(input,expect,419))

    def test_identifier_undeclare4(self):
        input = """
        procedure main ();
        
        begin
            if true then return i;
          
        end
        """

        expect = "Undeclared Identifier: i"
        self.assertTrue(TestChecker.test(input,expect,420))

    def test_function_undeclare(self):
        input = """
        procedure main ();
        begin
            if true then return foo();
          
        end
        """

        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,421))

    def test_function_undeclare2(self):
        input = """var foo: integer;
        procedure main ();
        begin
            foo := foo();
        end
        """

        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,422))

    def test_function_undeclare3(self):
        input = """

        procedure foo ();
        begin
            return foo();
        end
        """

        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,423))

    def test_function_undeclare4(self):
        input = """
        function main (): integer;
        begin
            if true then return foo();
        end
        """

        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input,expect,424))


    def test_procedure_undeclare(self):
        input = """
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,425))

    def test_procedure_undeclare2(self):
        input = """var foo: integer;
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,426))

    def test_procedure_undeclare3(self):
        input = """
        function foo1 (): integer;
        begin
            foo();
            return 1;
        end
        """

        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,427))

    def test_procedure_undeclare4(self):
        input = """
        function foo1 (): integer;
        begin
            if true then foo();
            return 1;
        end
        """

        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,428))

    def test_type_mismatch_in_if_statement(self):
        input = """
        procedure main ();
        begin
            if 1 then return haha;
           
        end
        """

        expect = "Type Mismatch In Statement: If(IntLiteral(1),[Return(Some(Id(haha)))],[])"
        self.assertTrue(TestChecker.test(input,expect,429))

    def test_type_mismatch_in_if_statement2(self):
        input = """
        procedure main ();
        begin
            if "haha" then return haha;
           
        end
        """

        expect = "Type Mismatch In Statement: If(StringLiteral(haha),[Return(Some(Id(haha)))],[])"
        self.assertTrue(TestChecker.test(input,expect,430))

    def test_type_mismatch_in_if_statement3(self):
        input = """
        procedure main ();
        begin
            if 1.1 then return haha;
        end
        """

        expect = "Type Mismatch In Statement: If(FloatLiteral(1.1),[Return(Some(Id(haha)))],[])"
        self.assertTrue(TestChecker.test(input,expect,431))

    def test_type_mismatch_in_if_statement4(self):
        input = """var a: integer;
        procedure main ();
        begin
            if 1 - 1 then a:= 1;
        end
        """

        expect = "Type Mismatch In Statement: If(BinaryOp(-,IntLiteral(1),IntLiteral(1)),[AssignStmt(Id(a),IntLiteral(1))],[])"
        self.assertTrue(TestChecker.test(input,expect,432))

    def test_type_mismatch_in_if_statement5(self):
        input = """var a: integer;
        procedure main ();
        begin
            if 1 + 1 then a:= 1;
        end
        """

        expect = "Type Mismatch In Statement: If(BinaryOp(+,IntLiteral(1),IntLiteral(1)),[AssignStmt(Id(a),IntLiteral(1))],[])"
        self.assertTrue(TestChecker.test(input,expect,433))

    def test_type_mismatch_in_for_statement(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:=1 to 10 do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),IntLiteral(1),IntLiteral(10),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,434))

    def test_type_mismatch_in_for_statement2(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= "a" to 10 do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),StringLiteral(a),IntLiteral(10),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,435))

    def test_type_mismatch_in_for_statement3(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= 1 to true do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),IntLiteral(1),BooleanLiteral(true),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,436))

    def test_type_mismatch_in_for_statement4(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= 1 to "haha" do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),IntLiteral(1),StringLiteral(haha),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,437))

    def test_type_mismatch_in_for_statement5(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= 1.1 to 10 do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),FloatLiteral(1.1),IntLiteral(10),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,438))

    def test_type_mismatch_in_for_statement6(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= a  to 10 do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),Id(a),IntLiteral(10),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,439))

    def test_type_mismatch_in_for_statement7(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            for i:= 1  to a do a:=1;
        end
        """

        expect = "Type Mismatch In Statement: For(Id(i),IntLiteral(1),Id(a),True,[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,440))

    def test_type_mismatch_in_while_statement(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while 1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(IntLiteral(1),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,441))

    def test_type_mismatch_in_while_statement2(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while a do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(Id(a),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,442))

    def test_type_mismatch_in_while_statement3(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while 1.1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(FloatLiteral(1.1),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,443))

    def test_type_mismatch_in_while_statement4(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while "haha" do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(StringLiteral(haha),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,444))

    def test_type_mismatch_in_while_statement5(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while true and 1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(and,BooleanLiteral(true),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input,expect,445))

    def test_type_mismatch_in_while_statement6(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while true and "haha" do a:=1 ;
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(and,BooleanLiteral(true),StringLiteral(haha))"
        self.assertTrue(TestChecker.test(input,expect,446))

    def test_type_mismatch_in_while_statement7(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            while true and then 1.1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(andthen,BooleanLiteral(true),FloatLiteral(1.1))"
        self.assertTrue(TestChecker.test(input,expect,447))

    def test_type_mismatch_in_while_statement8(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            if true then 
                while 1.1 and true  do a:=1 ;
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(and,FloatLiteral(1.1),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,448))

    def test_type_mismatch_in_while_statement9(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            if true then 
                while true  do 
                    while 1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(IntLiteral(1),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,449))

    def test_type_mismatch_in_while_statement10(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            if true then 
                while true  do 
                    if true then 
                        while 1 do a:=1 ;
        end
        """

        expect = "Type Mismatch In Statement: While(IntLiteral(1),[AssignStmt(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,450))

    def test_type_mismatch_in_assign_statement(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            a := "haha";
        end
        """

        expect = "Type Mismatch In Statement: AssignStmt(Id(a),StringLiteral(haha))"
        self.assertTrue(TestChecker.test(input,expect,451))

    def test_type_mismatch_in_assign_statement2(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            a := i;
        end
        """

        expect = "Type Mismatch In Statement: AssignStmt(Id(a),Id(i))"
        self.assertTrue(TestChecker.test(input,expect,452))

    def test_type_mismatch_in_assign_statement3(self):
        input = """var i: string; a: string;
        procedure main ();
        begin
            i:= a := true;
        end
        """

        expect = "Type Mismatch In Statement: AssignStmt(Id(a),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,453))

    def test_type_mismatch_in_assign_statement4(self):
        input = """var i: string; a: string;
        procedure main ();
        begin
            i:= true := a;
        end
        """

        expect = "Type Mismatch In Statement: AssignStmt(BooleanLiteral(true),Id(a))"
        self.assertTrue(TestChecker.test(input,expect,454))

    def test_type_mismatch_in_assign_statement5(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            a :=  1.1;
        end
        """

        expect = "Type Mismatch In Statement: AssignStmt(Id(a),FloatLiteral(1.1))"
        self.assertTrue(TestChecker.test(input,expect,455))

    def test_type_mismatch_in_return_statement(self):
        input = """var i: string; a: integer;
        procedure main ();
        begin
            return 1;
        end
        """

        expect = "Type Mismatch In Statement: Return(Some(IntLiteral(1)))"
        self.assertTrue(TestChecker.test(input,expect,456))

    def test_type_mismatch_in_return_statement2(self):
        input = """function foo(): integer;
        begin 
            return 1.1;
        end
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Type Mismatch In Statement: Return(Some(FloatLiteral(1.1)))"
        self.assertTrue(TestChecker.test(input,expect,457))

    def test_type_mismatch_in_return_statement3(self):
        input = """function foo(): integer;
        begin 
            return "haha";
        end
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Type Mismatch In Statement: Return(Some(StringLiteral(haha)))"
        self.assertTrue(TestChecker.test(input,expect,458))

    def test_type_mismatch_in_return_statement4(self):
        input = """function foo(): integer;
        begin 
            return;
        end
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Type Mismatch In Statement: Return(None)"
        self.assertTrue(TestChecker.test(input,expect,459))

    def test_type_mismatch_in_return_statement5(self):
        input = """var a: array [1 .. 2] of integer;
        function foo(): array [1 .. 3] of integer;
        begin 
            return a[1];
        end
        procedure main ();
        begin
            foo();
        end
        """

        expect = "Type Mismatch In Statement: Return(Some(ArrayCell(Id(a),IntLiteral(1))))"
        self.assertTrue(TestChecker.test(input,expect,460))

    def test_type_mismatch_in_procedure_call(self):
        input = """
        function foo(a : integer): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            foo("haha");
        end
        """

        expect = "Type Mismatch In Statement: CallStmt(Id(foo),[StringLiteral(haha)])"
        self.assertTrue(TestChecker.test(input,expect,461))

    def test_type_mismatch_in_procedure_call_2(self):
        input = """
        function foo(a : integer): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            foo(1.1);
        end
        """

        expect = "Type Mismatch In Statement: CallStmt(Id(foo),[FloatLiteral(1.1)])"
        self.assertTrue(TestChecker.test(input,expect,462))

    def test_type_mismatch_in_procedure_call_3(self):
        input = """
        function foo(a : integer; b :real): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            foo(1);
        end
        """

        expect = "Type Mismatch In Statement: CallStmt(Id(foo),[IntLiteral(1)])"
        self.assertTrue(TestChecker.test(input,expect,463))

    def test_type_mismatch_in_procedure_call_4(self):
        input = """
        function foo(a : integer; b :real): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            foo(1,2.2,3);
        end
        """

        expect = "Type Mismatch In Statement: CallStmt(Id(foo),[IntLiteral(1),FloatLiteral(2.2),IntLiteral(3)])"
        self.assertTrue(TestChecker.test(input,expect,464))

    def test_type_mismatch_in_procedure_call_5(self):
        input = """var a: array [1 .. 2] of integer;
        function foo(a : array [1 .. 3] of integer; b :real): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            a[1] := 1;
            foo(a[1],2.2);
        end
        """

        expect = "Type Mismatch In Statement: CallStmt(Id(foo),[ArrayCell(Id(a),IntLiteral(1)),FloatLiteral(2.2)])"
        self.assertTrue(TestChecker.test(input,expect,465))

    def test_type_mismatch_in_array(self):
        input = """var a: array [1 ..  2] of integer; b: integer;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            b[1] := 1;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: ArrayCell(Id(b),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input,expect,466))

    def test_type_mismatch_in_array_2(self):
        input = """var a: array [1 ..  2] of integer; b: string;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            a["haah"] := 1;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: ArrayCell(Id(a),StringLiteral(haah))"
        self.assertTrue(TestChecker.test(input,expect,467))

    def test_type_mismatch_in_array_3(self):
        input = """var a: array [1 ..  2] of integer; b: string;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            a[true] := 1;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: ArrayCell(Id(a),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,468))

    def test_type_mismatch_in_array_4(self):
        input = """var a: array [1 ..  2] of integer; b: string;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            a[b] := 1;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: ArrayCell(Id(a),Id(b))"
        self.assertTrue(TestChecker.test(input,expect,469))

    def test_type_mismatch_in_array_5(self):
        input = """var a: array [1 ..  2] of integer; c: real;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            a[c] := 1;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: ArrayCell(Id(a),Id(c))"
        self.assertTrue(TestChecker.test(input,expect,470))

    def test_type_mismatch_in_binary(self):
        input = """var c: integer;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := 1-true;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(-,IntLiteral(1),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,471))

    def test_type_mismatch_in_binary_2(self):
        input = """var c: integer;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := 1+true;
            foo();
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(+,IntLiteral(1),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,472))

    def test_type_mismatch_in_binary_3(self):
        input = """var c: integer;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := 1+1;
            c:= foo() / "haha";
            foo();
        end
        """

        expect = "Type Mismatch In Expression: BinaryOp(/,CallExpr(Id(foo),[]),StringLiteral(haha))"
        self.assertTrue(TestChecker.test(input,expect,473))

    def test_type_mismatch_in_unary(self):
        input = """var c: integer;
       
        procedure main ();
        begin
            c := - true;

        end
        """

        expect = "Type Mismatch In Expression: UnaryOp(-,BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input,expect,474))

    def test_type_mismatch_in_unary_2(self):
        input = """var c: integer;
       
        procedure main ();
        begin
            c := not 1;

        end
        """

        expect = "Type Mismatch In Expression: UnaryOp(not,IntLiteral(1))"
        self.assertTrue(TestChecker.test(input,expect,475))

    def test_type_mismatch_in_function_call(self):
        input = """var c: integer;
        function foo(): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := foo(1);

        end
        """

        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[IntLiteral(1)])"
        self.assertTrue(TestChecker.test(input,expect,476))

    def test_type_mismatch_in_function_call_2(self):
        input = """var c: integer;
        function foo(a : integer): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := foo(1,2);

        end
        """

        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[IntLiteral(1),IntLiteral(2)])"
        self.assertTrue(TestChecker.test(input,expect,477))

    def test_type_mismatch_in_function_call_3(self):
        input = """var c: integer;
        function foo(a : integer): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := foo("true");

        end
        """

        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[StringLiteral(true)])"
        self.assertTrue(TestChecker.test(input,expect,478))

    def test_type_mismatch_in_function_call_4(self):
        input = """var c: integer; a: array [1 .. 2] of integer;
        function foo(a : integer): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := foo(a[1]);

        end
        """

        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[ArrayCell(Id(a),IntLiteral(1))])"
        self.assertTrue(TestChecker.test(input,expect,479))

    def test_type_mismatch_in_function_call_5(self):
        input = """var c: integer; 
        function foo(a : integer; b: real): integer;
        begin 
            return 1;
        end
        procedure main ();
        begin
            c := foo(1);

        end
        """

        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[IntLiteral(1)])"
        self.assertTrue(TestChecker.test(input,expect,480))

    def test_break_not_in_loop(self):
        input = """
        procedure main ();
        begin
            break;

        end
        """

        expect = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,481))

    def test_break_not_in_loop_2(self):
        input = """
        procedure main ();
        begin
            with 
                a: integer;
            do
            begin
                break;
            end

        end
        """

        expect = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,482))

    def test_break_not_in_loop_3(self):
        input = """var a: integer;
        procedure main ();
        begin
            while (true)
            do
            begin
                if (true) then break;
                a := 1;
            end
            break;



        end
        """

        expect = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,483))

    def test_continue_not_in_loop(self):
        input = """var a: integer;
        procedure main ();
        begin
            continue;
        end
        """

        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,484))

    def test_continue_not_in_loop_2(self):
        input = """var a: integer;
        procedure main ();
        begin
             with 
                a: integer;
            do
            begin
                continue;
            end

        end
        """

        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,485))

    def test_continue_not_in_loop_3(self):
        input = """var a: integer;
        procedure main ();
        begin
            for a := 1 to 10 do 
            begin
                a := a +1;
                continue;
            end
            continue;

        end
        """

        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input,expect,486))

    def test_No_Entry_point(self):
        input = """
        procedure main1 ();
        begin
        
        end
        """

        expect = "No entry point"
        self.assertTrue(TestChecker.test(input,expect,487))

    def test_No_Entry_point2(self):
        input = """
        function main (): integer;
        begin
            return 1;
        
        end
        """

        expect = "No entry point"
        self.assertTrue(TestChecker.test(input,expect,488))

    def test_No_Entry_point3(self):
        input = """
        procedure main (a: integer);
        begin
        end
        """

        expect = "No entry point"
        self.assertTrue(TestChecker.test(input,expect,489))

    def test_unreachable_function(self):
        input = """
        procedure main (a: integer);
        begin
        end
        """

        expect = "No entry point"
        self.assertTrue(TestChecker.test(input,expect,489))

    def test_unreachable_function(self):
        input = """
        function foo():integer;
        begin
            return 1;
        end
        procedure main ();
        begin
        end
        """

        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input,expect,490))

    def test_unreachable_function_2(self):
        input = """
        function foo():integer;
        begin
            return foo();
        end
        procedure main ();
        begin
        end
        """

        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input,expect,491))

    def test_unreachable_function_3(self):
        input = """
         function foo1():integer;
        begin
            return foo();
        end
        procedure main ();
        begin
        end
         function foo():integer;
        begin
            return 1;
        end
        """

        expect = "Unreachable Function: foo1"
        self.assertTrue(TestChecker.test(input,expect,492))

    def test_unreachable_function_4(self):
        input = """
         function foo1():integer;
        begin
            return 1;
        end
        procedure main ();
        begin
            foo1();
        end
         function foo():integer;
        begin
            return foo1();
        end
        """

        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input,expect,493))

    def test_unreachable_function_5(self):
        input = """var a :integer;
         function foo1():integer;
        begin
            return 1;
        end
        procedure main ();
        begin
            a := foo1();
        end
         function foo():integer;
        begin
            return foo1();
        end
        """

        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input,expect,494))

    def test_some_mistakes_inside_a_program(self):
        input = """
        procedure main();
        var i, n, sum, mean: integer;
        begin
            for z := 1 to n do sum := sum+i;
            sum := sum/n;
        end"""

        expect = "Undeclared Identifier: z"
        self.assertTrue(TestChecker.test(input,expect,495))

    def test_some_mistakes_inside_a_program_2(self):
        input = """var i, sol: integer;
        procedure main();
        var n0, n1, n: integer;
        begin
            n0 := 0;
            n1 := 1;
            for i := 0 to n do
            begin
                if n = 0 then sol := "haha";
                if n = 1 then sol := 1;
                sol := n1+n0; 
                n0 := n1;
                n1 := sol;
            end
        end"""

        expect = "Type Mismatch In Statement: AssignStmt(Id(sol),StringLiteral(haha))"
        self.assertTrue(TestChecker.test(input,expect,496))

    def test_some_mistakes_inside_a_program_3(self):
        input = """
        function func(x: integer): integer;
        begin
            return x*3/4-24;
        end

        procedure main();
        var x: integer; y: array [0 .. 18] of integer;
        begin
            x := func(x)/3 + y[x-3*1/(6-2)] * y + 8/2;
        end"""

        expect = "Type Mismatch In Statement: Return(Some(BinaryOp(-,BinaryOp(/,BinaryOp(*,Id(x),IntLiteral(3)),IntLiteral(4)),IntLiteral(24))))"
        self.assertTrue(TestChecker.test(input,expect,497))

    def test_some_mistakes_inside_a_program_4(self):
        input = """
        procedure main();
        var side, surfacearea, volume: real;
        begin
            surfacearea := 6.0 * side * side;
            volume := pow(side, 3);
        end"""

        expect = "Undeclared Function: pow"
        self.assertTrue(TestChecker.test(input,expect,498))

    def test_some_mistakes_inside_a_program_5(self):
        input = """
        procedure main();
        begin
            if (x > 1) and Then (x < 2) then return true;
            else 
                if (x < 1) or ELSE (x > 2) then return false;
        end"""

        expect = "Undeclared Identifier: x"
        self.assertTrue(TestChecker.test(input,expect,499))

    def test_some_mistakes_inside_a_program_6(self):
        input = """
        function func(x, y: integer): integer;
        begin
             return x-3*y+1/4-(12+7/x);
        end

        function func2(x, y: integer): integer;
        begin
            y := x[1]*(3/6+2);
            return 2*x[2]/3+4/3*(x-3);
        end

        procedure main();
        var x,y: integer;
            z: array [0 .. 2] of integer;
        begin
            if x = y then func(x,z[0]);
            else
                if x > y then func(y,z[1]); else func2(z,z[2]);
         end"""

        expect = "Type Mismatch In Statement: Return(Some(BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(x),BinaryOp(*,IntLiteral(3),Id(y))),BinaryOp(/,IntLiteral(1),IntLiteral(4))),BinaryOp(+,IntLiteral(12),BinaryOp(/,IntLiteral(7),Id(x))))))"
        self.assertTrue(TestChecker.test(input,expect,500))