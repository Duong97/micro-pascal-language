import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_simple_program(self):
        input = """procedure main(); begin end"""
        expect = str(Program([FuncDecl(Id("main"),[],[],[])]))
        self.assertTrue(TestAST.test(input,expect,301))

    def test_simple_program1(self):
        input = """procedure main(); var x, y: real; begin end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),FloatType()),VarDecl(Id('y'),FloatType())],[])]))
        self.assertTrue(TestAST.test(input,expect,302))

    def test_simple_program2(self):
        input = """procedure main(); var x, y: real; begin return true; end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),FloatType()),VarDecl(Id('y'),FloatType())],[Return(BooleanLiteral('true'))])]))
        self.assertTrue(TestAST.test(input,expect,303))

    def test_a_simple_program_has_a_simple_call_putIntLn(self):
        input = """procedure main(); begin putIntLn(5); end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[CallStmt(Id('putIntLn'),[IntLiteral('5')])])]))
        self.assertTrue(TestAST.test(input,expect,304))

    def test_a_global_variable_declaration_with_int_type(self):
        input = """var a: integer;"""
        expect = str(Program([VarDecl(Id('a'),IntType())]))      
        self.assertTrue(TestAST.test(input,expect,305))

    def test_a_global_variable_declaration_with_bool_type(self):
        input = """var a: boolean;"""
        expect = str(Program([VarDecl(Id('a'),BoolType())]))       
        self.assertTrue(TestAST.test(input,expect,306))

    def test_a_list_of_global_variables_declaration_with_int_type(self):
        input = """var a, b: integer;"""
        expect = str(Program([VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType())]))       
        self.assertTrue(TestAST.test(input,expect,307))

    def test_a_list_of_global_variables_declaration_with_int_and_bool_type(self):
        input = """var a, b: integer; c: boolean;"""
        expect = str(Program([VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType()),VarDecl(Id('c'),BoolType())]))  
        self.assertTrue(TestAST.test(input,expect,308))

    def test_a_list_of_global_variables_declaration_with_bool_and_string(self):
        input = """var a, b: string; c: boolean;"""
        expect = str(Program([VarDecl(Id('a'),StringType()),VarDecl(Id('b'),StringType()),VarDecl(Id('c'),BoolType())]))  
        self.assertTrue(TestAST.test(input,expect,309))

    def test_a_global_variables_declaration_with_int_array_type(self):
        input = """var a: array [1 .. 2]  of integer;"""
        expect = str(Program([VarDecl(Id('a'),ArrayType('1','2',IntType()))]))
        self.assertTrue(TestAST.test(input,expect,310))

    def test_a_list_global_variables_declaration_with_int_array_and_int_type(self):
        input = """var a: array [1 .. 2]  of integer; b: integer;"""
        expect = str(Program([VarDecl(Id('a'),ArrayType('1','2',IntType())),VarDecl(Id('b'),IntType())]))
        self.assertTrue(TestAST.test(input,expect,311))

    def test_a_no_paralist_function_declaration_with_no_return_type(self):
        input = """procedure main(); begin return; end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Return()])]))
        self.assertTrue(TestAST.test(input,expect,312))

    def test_a_no_paralist_function_declaration_with_int_as_return_type(self):
        input = """procedure main(); begin return 1; end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Return(IntLiteral('1'))])]))
        self.assertTrue(TestAST.test(input,expect,313))

    def test_a_no_paralist_function_declaration_with_bool_as_return_type(self):
        input = """procedure main(); begin return true; end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Return(BooleanLiteral('true'))])]))
        self.assertTrue(TestAST.test(input,expect,314))

    def test_a_no_paralist_function_declaration_with_string_as_return_type(self):
        input = """procedure main(); begin return "Hey You"; end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Return(StringLiteral('Hey You'))])]))
        self.assertTrue(TestAST.test(input,expect,315))

    def test_a_function_declaration_with_int_type_parameter(self):
        input = """function foo (a: integer): integer; begin end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),IntType())],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,316))

    def test_a_function_declaration_with_string_type_parameter(self):
        input = """function foo (a: string): integer; begin end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),StringType())],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,317))

    def test_a_function_declaration_with_no_parameter(self):
        input = """function foo (): integer; begin end"""
        expect = str(Program([FuncDecl(Id('foo'),[],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,318))

    def test_a_function_declaration_with_int_and_bool_type_parameter(self):
        input = """function foo (a: integer; b: boolean): integer; begin end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),BoolType())],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,319))

    def test_a_function_declaration_with_int_array_and_boolean_type_parameters(self):
        input = """function foo (a: array [1 .. 2] of integer; b: boolean): integer; begin end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),ArrayType('1','2',IntType())),VarDecl(Id('b'),BoolType())],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,320))

    def test_a_local_variable_declaration_with_int_type(self):
        input = """procedure main ();
        var a: integer;
        begin end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),IntType())],[])]))
        self.assertTrue(TestAST.test(input,expect,321))

    def test_a_local_variables_declaration_with_bool_type(self):
        input = """procedure main ();
        var a, b: boolean ;
        begin end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),BoolType()),VarDecl(Id('b'),BoolType())],[])]))
        self.assertTrue(TestAST.test(input,expect,322))

    def test_a_list_of_local_variable_declarations_with_int_array_type_and_int_type(self):
        input = """procedure main ();
        var a: integer; b: array [1 .. 2] of integer;
        begin end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),ArrayType('1','2',IntType()))],[])]))
        self.assertTrue(TestAST.test(input,expect,323))

    def test_a_local_declarations_inside_a_with_scoop(self):
        input = """procedure main ();
        begin
        with
            x: integer;
        do
            begin
                x := 1;
            end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[With([VarDecl(Id('x'),IntType())],[Assign(Id('x'),IntLiteral('1'))])])]))
        self.assertTrue(TestAST.test(input,expect,324))

    def test_a_simple_line_comment(self):
        input = """procedure main ();
        begin
        //This is line comment
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[])]))        
        self.assertTrue(TestAST.test(input,expect,325))

    def test_a_simple_block_comment(self):
        input = """procedure main ();
        begin
        (*This is block comment*)
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[])]))        
        self.assertTrue(TestAST.test(input,expect,326))

    def test_a_variable_initialization_for_int_type(self):
        input = """procedure main ();
        var x: integer;
        begin
            x := 1;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[Assign(Id('x'),IntLiteral('1'))])]))        
        self.assertTrue(TestAST.test(input,expect,327))

    def test_a_variable_initialization_for_bool_type(self):
        input = """procedure main ();
        var x: boolean;
        begin
            x := true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),BoolType())],[Assign(Id('x'),BooleanLiteral('true'))])]))        
        self.assertTrue(TestAST.test(input,expect,328))

    def test_a_variable_initialization_for_string_type(self):
        input = """procedure main ();
        var x: string;
        begin
            x := "hello";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),StringType())],[Assign(Id('x'),StringLiteral('hello'))])]))        
        self.assertTrue(TestAST.test(input,expect,329))

    def test_a_more_complex_variable_initialization_for_string_type(self):
        input = """procedure main ();
        var x: string;
        begin
            x := "hey" + "you";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),StringType())],[Assign(Id('x'),BinaryOp('+',StringLiteral('hey'),StringLiteral('you')))])]))
        self.assertTrue(TestAST.test(input,expect,330))

    def test_a_variable_initialization_for_array_type(self):
        input = """procedure main ();
        var x: array [1 .. 10] of integer;
        begin
            x[2] := 12;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),ArrayType('1','10',IntType()))],[Assign(ArrayCell(Id('x'),IntLiteral('2')),IntLiteral('12'))])]))
        self.assertTrue(TestAST.test(input,expect,331))

    def test_a_more_complex_variable_initialization_for_array_type(self):
        input = """procedure main ();
        var x: array [1 .. 10] of integer;
        begin
            x[0] := 1;
            x[1] := x[0] + 1;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),ArrayType('1','10',IntType()))],[Assign(ArrayCell(Id('x'),IntLiteral('0')),IntLiteral('1')),Assign(ArrayCell(Id('x'),IntLiteral('1')),BinaryOp('+',ArrayCell(Id('x'),IntLiteral('0')),IntLiteral('1')))])]))
        self.assertTrue(TestAST.test(input,expect,332))

    def test_a_function_call_without_arguments(self):
        input = """function foo(): integer; begin return 1; end
        procedure main ();
        begin
            foo();
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[])])]))
        self.assertTrue(TestAST.test(input,expect,333))

    def test_a_function_call_with_an_int_type_argument(self):
        input = """function foo(i: integer): integer; begin return 1; end
        procedure main ();
        begin
            foo(1);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('i'),IntType())],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[IntLiteral('1')])])]))
        self.assertTrue(TestAST.test(input,expect,334))

    def test_a_function_call_with_an_bool_type_argument(self):
        input = """function foo(i: boolean): integer; begin return 1; end
        procedure main ();
        begin
            foo(true);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('i'),BoolType())],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[BooleanLiteral('true')])])]))
        self.assertTrue(TestAST.test(input,expect,335))

    def test_a_function_call_with_an_int_array_type_argument(self):
        input = """function foo(i: array [1 .. 2] of integer): integer; begin return 1; end
        procedure main ();
        begin
            foo(a[1]);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('i'),ArrayType('1','2',IntType()))],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[ArrayCell(Id('a'),IntLiteral('1'))])])]))
        self.assertTrue(TestAST.test(input,expect,336))

    def test_a_function_call_with_a_list_of_int_type_arguments(self):
        input = """function foo(a, b: integer): integer; begin return 1; end
        procedure main ();
        begin
            foo(1, 2);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType())],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[IntLiteral('1'),IntLiteral('2')])])]))
        self.assertTrue(TestAST.test(input,expect,337))

    def test_a_function_call_with_a_list_of_int_array_and_bool_type_arguments(self):
        input = """function foo(a: integer; b: array [1 .. 2] of integer): integer; begin return 1; end
        procedure main ();
        begin
            foo(1, b[2]);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),ArrayType('1','2',IntType()))],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo'),[IntLiteral('1'),ArrayCell(Id('b'),IntLiteral('2'))])])]))
        self.assertTrue(TestAST.test(input,expect,338))

    def test_more_complex_function_calls_with_int_type_argument(self):
        input = """function foo(a: integer): integer; begin return 1; end
        function foo1(b:integer): integer; begin return foo(1); end
        procedure main ();
        begin
            foo1(1);
        end"""
        expect = str(Program([FuncDecl(Id('foo'),[VarDecl(Id('a'),IntType())],[],[Return((IntLiteral('1')))],IntType()),FuncDecl(Id('foo1'),[VarDecl(Id('b'),IntType())],[],[Return((CallExpr(Id('foo'),[IntLiteral('1')])))],IntType()),FuncDecl(Id('main'),[],[],[CallStmt(Id('foo1'),[IntLiteral('1')])])]))        
        self.assertTrue(TestAST.test(input,expect,339))

    def test_simple_if_else_without_else_statement(self):
        input = """
        procedure main ();
        begin
            if true then return "hihi";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BooleanLiteral('true'),[Return((StringLiteral('hihi')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,340))

    def test_a_complex_if_else_without_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x = 1) or (x = 2) then return "hihi";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('or',BinaryOp('=',Id('x'),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((StringLiteral('hihi')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,341))

    def test_another_complex_if_else_without_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x = 1) or (x = 2) then 
                begin
                 x := 3;
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('or',BinaryOp('=',Id('x'),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Assign(Id('x'),IntLiteral('3'))],[])])]))
        self.assertTrue(TestAST.test(input,expect,342))

    def test_simple_if_else_statementt(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x = 1) or (x = 2) then x := 3; else x := 0;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('or',BinaryOp('=',Id('x'),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Assign(Id('x'),IntLiteral('3'))],[Assign(Id('x'),IntLiteral('0'))])])]))
        self.assertTrue(TestAST.test(input,expect,343))

    def test_a_complex_if_else_statementt(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x < 1) or (x > 2) then x := 3; else x := 0;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('or',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),IntLiteral('2'))),[Assign(Id('x'),IntLiteral('3'))],[Assign(Id('x'),IntLiteral('0'))])])]))
        self.assertTrue(TestAST.test(input,expect,344))

    def test_another_if_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x < 1) or (x > 2) then x := 3; else begin x := 0; end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('or',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),IntLiteral('2'))),[Assign(Id('x'),IntLiteral('3'))],[Assign(Id('x'),IntLiteral('0'))])])]))
        self.assertTrue(TestAST.test(input,expect,345))

    def test_a_simple_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            While (true) do return "Hey";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BooleanLiteral('true'),[Return((StringLiteral('Hey')))])])]))
        self.assertTrue(TestAST.test(input,expect,346))

    def test_a_complex_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            While (x <1) and (x > -2) do return "Hey";
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BinaryOp('and',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),UnaryOp('-',IntLiteral('2')))),[Return((StringLiteral('Hey')))])])]))
        self.assertTrue(TestAST.test(input,expect,347))

    def test_another_complex_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            While (x <1) and (x > -2) do 
                begin
                    x := x - 1;
                    return "Hey";
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BinaryOp('and',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),UnaryOp('-',IntLiteral('2')))),[Assign(Id('x'),BinaryOp('-',Id('x'),IntLiteral('1'))),Return((StringLiteral('Hey')))])])]))
        self.assertTrue(TestAST.test(input,expect,348))

    def test_a_complex_while_do_statement_with_if_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            While (x <1) and (x > -2) do if (x > 0) then x := x - 1;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BinaryOp('and',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),UnaryOp('-',IntLiteral('2')))),[If(BinaryOp('>',Id('x'),IntLiteral('0')),[Assign(Id('x'),BinaryOp('-',Id('x'),IntLiteral('1')))],[])])])]))
        self.assertTrue(TestAST.test(input,expect,349))

    def test_another_complex_while_do_statement_with_if_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            if (x > 0) then 
                While (x <1) and (x > -2) do x := x - 1;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[If(BinaryOp('>',Id('x'),IntLiteral('0')),[While(BinaryOp('and',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),UnaryOp('-',IntLiteral('2')))),[Assign(Id('x'),BinaryOp('-',Id('x'),IntLiteral('1')))])],[])])]))     
        self.assertTrue(TestAST.test(input,expect,350))

    def test_a_simple_for_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do putString("HaHa");
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[CallStmt(Id('putString'),[StringLiteral('HaHa')])])])]))
        self.assertTrue(TestAST.test(input,expect,351))

    def test_a_complex_for_statement_with_if_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do if x > 2 then putString("HaHa");
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[If(BinaryOp('>',Id('x'),IntLiteral('2')),[CallStmt(Id('putString'),[StringLiteral('HaHa')])],[])])])]))
        self.assertTrue(TestAST.test(input,expect,352))

    def test_another_complex_for_statement_with_if_else_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do if x > 2 then putString("HaHa"); else return 1;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[If(BinaryOp('>',Id('x'),IntLiteral('2')),[CallStmt(Id('putString'),[StringLiteral('HaHa')])],[Return((IntLiteral('1')))])])])]))
        self.assertTrue(TestAST.test(input,expect,353))

    def test_a_complex_for_statement_with_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do while x < 5 do putString("HaHa");
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[While(BinaryOp('<',Id('x'),IntLiteral('5')),[CallStmt(Id('putString'),[StringLiteral('HaHa')])])])])]))
        self.assertTrue(TestAST.test(input,expect,354))

    def test_another_complex_for_statement_with_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do while x < 5 do 
                begin
                    x := x + 1;
                    putString("HaHa");
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[While(BinaryOp('<',Id('x'),IntLiteral('5')),[Assign(Id('x'),BinaryOp('+',Id('x'),IntLiteral('1'))),CallStmt(Id('putString'),[StringLiteral('HaHa')])])])])]))
        self.assertTrue(TestAST.test(input,expect,355))

    def test_while_do_statement_inside_another_while_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            while true do
                while 1<2 do 
                    begin
                        putString("HaHa");
                    end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BooleanLiteral('true'),[While(BinaryOp('<',IntLiteral('1'),IntLiteral('2')),[CallStmt(Id('putString'),[StringLiteral('HaHa')])])])])]))
        self.assertTrue(TestAST.test(input,expect,356))

    def test_break_statement_inside_a_for_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do while x < 5 do 
                begin
                    if x > 8 then break;
                    putString("HaHa");
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[While(BinaryOp('<',Id('x'),IntLiteral('5')),[If(BinaryOp('>',Id('x'),IntLiteral('8')),['Break'],[]),CallStmt(Id('putString'),[StringLiteral('HaHa')])])])])]))
        self.assertTrue(TestAST.test(input,expect,357))

    def test_break_statement_inside_a_While_do_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            while (true) do 
                begin
                    if 1 > 2 then break;
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BooleanLiteral('true'),[If(BinaryOp('>',IntLiteral('1'),IntLiteral('2')),['Break'],[])])])]))
        self.assertTrue(TestAST.test(input,expect,358))

    def test_continue_statement_inside_a_for_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            for x := 1 to 10 do 
                begin
                    if 1 > 0 then continue;
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[For(Id('x'),IntLiteral('1'),IntLiteral('10'),'True',[If(BinaryOp('>',IntLiteral('1'),IntLiteral('0')),['Continue'],[])])])]))
        self.assertTrue(TestAST.test(input,expect,359))

    def test_continue_statement_inside_a_while_statement(self):
        input = """
        procedure main ();
        var x: integer;
        begin
            while (true) do 
                begin
                    if 1 > 2 then continue;
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType())],[While(BooleanLiteral('true'),[If(BinaryOp('>',IntLiteral('1'),IntLiteral('2')),['Continue'],[])])])]))
        self.assertTrue(TestAST.test(input,expect,360))

    def test_return_statement_for_a_function_caculating_sum_of_2_integers(self):
        input = """function sum(a, b: integer): integer; begin return a + b; end"""
        expect = str(Program([FuncDecl(Id('sum'),[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType())],[],[Return((BinaryOp('+',Id('a'),Id('b'))))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,361))

    def test_return_statement_for_a_function_having_no_return_value(self):
        input = """function sum(a, b: integer): integer; begin return; end"""
        expect = str(Program([FuncDecl(Id('sum'),[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType())],[],[Return(None)],IntType())]))
        self.assertTrue(TestAST.test(input,expect,362))

    def test_return_statement_for_a_function_checking_if_the_input_value_is_even_or_not(self):
        input = """function sum(a: integer): integer; begin if a div 2 = 0 then return true; else return false; end"""
        expect = str(Program([FuncDecl(Id('sum'),[VarDecl(Id('a'),IntType())],[],[If(BinaryOp('=',BinaryOp('div',Id('a'),IntLiteral('2')),IntLiteral('0')),[Return((BooleanLiteral('true')))],[Return((BooleanLiteral('false')))])],IntType())]))
        self.assertTrue(TestAST.test(input,expect,363))
    
    def test_return_statement_with_a_mix_of_expression(self):
        input = """function sum(a: integer): integer; begin return a*2 - 1; end"""
        expect = str(Program([FuncDecl(Id('sum'),[VarDecl(Id('a'),IntType())],[],[Return((BinaryOp('-',BinaryOp('*',Id('a'),IntLiteral('2')),IntLiteral('1'))))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,364))

    def test_return_statement_for_a_recursive_function(self):
        input = """function sum(a: integer): integer; begin return sum(a-1); end"""
        expect = str(Program([FuncDecl(Id('sum'),[VarDecl(Id('a'),IntType())],[],[Return((CallExpr(Id('sum'),[BinaryOp('-',Id('a'),IntLiteral('1'))])))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,365))

    def test_binary_operation_add_and_sub(self):
        input = """procedure main();
        begin
            x := 2 + 1;
            a := 1 - 2 + x;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),BinaryOp('+',IntLiteral('2'),IntLiteral('1'))),Assign(Id('a'),BinaryOp('+',BinaryOp('-',IntLiteral('1'),IntLiteral('2')),Id('x')))])]))
        self.assertTrue(TestAST.test(input,expect,366))

    def test_binary_operation_mul_and_divide(self):
        input = """procedure main();
        begin
            x := 2 * 1;
            a := 1/2*x;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),BinaryOp('*',IntLiteral('2'),IntLiteral('1'))),Assign(Id('a'),BinaryOp('*',BinaryOp('/',IntLiteral('1'),IntLiteral('2')),Id('x')))])]))
        self.assertTrue(TestAST.test(input,expect,367))

    def test_binary_operation_larger_or_smaller_than(self):
        input = """procedure main();
        begin
            x := 0;
            if (x > -1) and (x < 1) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),IntLiteral('0')),If(BinaryOp('and',BinaryOp('>',Id('x'),UnaryOp('-',IntLiteral('1'))),BinaryOp('<',Id('x'),IntLiteral('1'))),[Return((BooleanLiteral('true')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,368))

    def test_binary_operation_larger_and_smaller_than_or_equal(self):
        input = """procedure main();
        begin
            x := 0;
            if (x >= -1) and (x <= 1) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),IntLiteral('0')),If(BinaryOp('and',BinaryOp('>=',Id('x'),UnaryOp('-',IntLiteral('1'))),BinaryOp('<=',Id('x'),IntLiteral('1'))),[Return((BooleanLiteral('true')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,369))

    def test_binary_operation_equal_and_not_equal(self):
        input = """procedure main();
        begin
            x := 0;
            if (x = -1) oR (x <> 1) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),IntLiteral('0')),If(BinaryOp('oR',BinaryOp('=',Id('x'),UnaryOp('-',IntLiteral('1'))),BinaryOp('<>',Id('x'),IntLiteral('1'))),[Return((BooleanLiteral('true')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,370))

    def test_binary_operation_and_and_or(self):
        input = """procedure main();
        begin
            x := 0;
            if (x = -1) or (x <> 1) and (x = 0) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),IntLiteral('0')),If(BinaryOp('or',BinaryOp('=',Id('x'),UnaryOp('-',IntLiteral('1'))),BinaryOp('and',BinaryOp('<>',Id('x'),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('0')))),[Return((BooleanLiteral('true')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,371))

    def test_unary_operation_sub_and_not(self):
        input = """procedure main();
        begin
            x := -1;
            if (x = not 1) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),UnaryOp('-',IntLiteral('1'))),If(BinaryOp('=',Id('x'),UnaryOp('not',IntLiteral('1'))),[Return((BooleanLiteral('true')))],[])])]))
        self.assertTrue(TestAST.test(input,expect,372))

    def test_operation_association_between_div_and_plus(self):
        input = """procedure main();
        begin
            x := x+2/3+1;
            x := 3/(1+4+x);
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),BinaryOp('+',BinaryOp('+',Id('x'),BinaryOp('/',IntLiteral('2'),IntLiteral('3'))),IntLiteral('1'))),Assign(Id('x'),BinaryOp('/',IntLiteral('3'),BinaryOp('+',BinaryOp('+',IntLiteral('1'),IntLiteral('4')),Id('x'))))])]))
        self.assertTrue(TestAST.test(input,expect,373))

    def test_operation_association_between_mul_and_plus(self):
        input = """procedure main();
        begin
            x := x+2*3;
            x := 3*(x+1);
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),BinaryOp('+',Id('x'),BinaryOp('*',IntLiteral('2'),IntLiteral('3')))),Assign(Id('x'),BinaryOp('*',IntLiteral('3'),BinaryOp('+',Id('x'),IntLiteral('1'))))])]))
        self.assertTrue(TestAST.test(input,expect,374))

    def test_operation_association_between_mul_and_sub(self):
        input = """procedure main();
        begin
            x := x-4*2;
            x := 2*(12-x);
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),BinaryOp('-',Id('x'),BinaryOp('*',IntLiteral('4'),IntLiteral('2')))),Assign(Id('x'),BinaryOp('*',IntLiteral('2'),BinaryOp('-',IntLiteral('12'),Id('x'))))])]))
        self.assertTrue(TestAST.test(input,expect,375))

    def test_Operation_association_between_orelse_and_equal(self):
        input = """procedure main();
        begin
            x := 1;
            if (x = 1) or else (x = 2) then return true;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[Assign(Id('x'),IntLiteral('1')),If(BinaryOp('orelse',BinaryOp('=',Id('x'),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((BooleanLiteral('true')))],[])])]))        
        self.assertTrue(TestAST.test(input,expect,376))

    def test_a_simple_mixture_between_operations_and_statements(self):
        input = """procedure main();
        begin
            if ((2*y) = 1) or else (x = 2) then return y = y-2;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BinaryOp('orelse',BinaryOp('=',BinaryOp('*',IntLiteral('2'),Id('y')),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((BinaryOp('=',Id('y'),BinaryOp('-',Id('y'),IntLiteral('2')))))],[])])]))
        self.assertTrue(TestAST.test(input,expect,377))

    def test_another_simple_mixture_between_operations_and_statements(self):
        input = """procedure main();
        begin
            if ((2*y) = 1) or else (x = 2) then return y = y-2;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BinaryOp('orelse',BinaryOp('=',BinaryOp('*',IntLiteral('2'),Id('y')),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((BinaryOp('=',Id('y'),BinaryOp('-',Id('y'),IntLiteral('2')))))],[])])]))
        self.assertTrue(TestAST.test(input,expect,378))

    def test_a_complex_mixture_between_operations_and_statements(self):
        input = """procedure main();
        begin
            if ((2*y) = 1) or else (x = 2) then return y = y-2;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BinaryOp('orelse',BinaryOp('=',BinaryOp('*',IntLiteral('2'),Id('y')),IntLiteral('1')),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((BinaryOp('=',Id('y'),BinaryOp('-',Id('y'),IntLiteral('2')))))],[])])]))
        self.assertTrue(TestAST.test(input,expect,379))

    def test_another_complex_mixture_between_operations_and_statements(self):
        input = """procedure main();
        begin
            if (2*y = 1 - y) or else (x = 2) then return y = y-2;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BinaryOp('orelse',BinaryOp('=',BinaryOp('*',IntLiteral('2'),Id('y')),BinaryOp('-',IntLiteral('1'),Id('y'))),BinaryOp('=',Id('x'),IntLiteral('2'))),[Return((BinaryOp('=',Id('y'),BinaryOp('-',Id('y'),IntLiteral('2')))))],[])])]))
        self.assertTrue(TestAST.test(input,expect,380))

    def test_get_the_largest_number_in_an_array(self):
        input = """procedure  main();
        var a: array [0 .. 2] of integer; 
            max: integer;
        begin 
            max := 0;
            for i := 0 to 2 do if a[i] > max then max := a[i];
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType('0','2',IntType())),VarDecl(Id('max'),IntType())],[Assign(Id('max'),IntLiteral('0')),For(Id('i'),IntLiteral('0'),IntLiteral('2'),'True',[If(BinaryOp('>',ArrayCell(Id('a'),Id('i')),Id('max')),[Assign(Id('max'),ArrayCell(Id('a'),Id('i')))],[])])])]))
        self.assertTrue(TestAST.test(input,expect,381))

    def test_get_the_second_largest_number_in_an_array(self):
        input = """procedure main();
        var a: array [0 .. 9] of integer;
            highest, sechighest: integer;
        begin
            highest := 0;
            for i := 0 to 9 do
                if(a[i] >= highest) then
                begin
                    sechighest := highest;
                    highest := a[i];
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType('0','9',IntType())),VarDecl(Id('highest'),IntType()),VarDecl(Id('sechighest'),IntType())],[Assign(Id('highest'),IntLiteral('0')),For(Id('i'),IntLiteral('0'),IntLiteral('9'),'True',[If(BinaryOp('>=',ArrayCell(Id('a'),Id('i')),Id('highest')),[Assign(Id('sechighest'),Id('highest')),Assign(Id('highest'),ArrayCell(Id('a'),Id('i')))],[])])])]))
        self.assertTrue(TestAST.test(input,expect,382))

    def test_get_the_smallest_number_in_an_array(self):
        input = """procedure main();
        var a: array [0 .. 2] of integer;
            min: integer;
        begin
            min := 0;
            for i := 0 to 2 do if  a[i] < min then max :=a [i];
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType('0','2',IntType())),VarDecl(Id('min'),IntType())],[Assign(Id('min'),IntLiteral('0')),For(Id('i'),IntLiteral('0'),IntLiteral('2'),'True',[If(BinaryOp('<',ArrayCell(Id('a'),Id('i')),Id('min')),[Assign(Id('max'),ArrayCell(Id('a'),Id('i')))],[])])])]))        
        self.assertTrue(TestAST.test(input,expect,383))

    def test_get_the_first_even_number_in_an_array(self):
        input = """procedure main();
        var a: array [0 .. 2] of integer;
            evenNum: integer;
        begin
            for i:= 0 to 2 do 
                if a[i] div 2 = 0 then
                begin
                    evenNum := a[i];
                    break;
                end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType('0','2',IntType())),VarDecl(Id('evenNum'),IntType())],[For(Id('i'),IntLiteral('0'),IntLiteral('2'),'True',[If(BinaryOp('=',BinaryOp('div',ArrayCell(Id('a'),Id('i')),IntLiteral('2')),IntLiteral('0')),[Assign(Id('evenNum'),ArrayCell(Id('a'),Id('i'))),'Break'],[])])])]))
        self.assertTrue(TestAST.test(input,expect,384))

    def test_get_the_solution_x_for_linear_function(self):
        input = """function getX (y, a, b: real): real;
        begin
            return (y-b)/a;
        end

        procedure main();
        var a, b, y: real;
        begin
            getX(y,a,b);
        end"""
        expect = str(Program([FuncDecl(Id('getX'),[VarDecl(Id('y'),FloatType()),VarDecl(Id('a'),FloatType()),VarDecl(Id('b'),FloatType())],[],[Return((BinaryOp('/',BinaryOp('-',Id('y'),Id('b')),Id('a'))))],FloatType()),FuncDecl(Id('main'),[],[VarDecl(Id('a'),FloatType()),VarDecl(Id('b'),FloatType()),VarDecl(Id('y'),FloatType())],[CallStmt(Id('getX'),[Id('y'),Id('a'),Id('b')])])]))
        self.assertTrue(TestAST.test(input,expect,385))

    def test_get_the_area_of_a_triangle(self):
        input = """procedure main();
        var s, a, b, c, area: integer;
        begin
            s := (a + b + c) / 2; 
            area := sqrt(s * (s - a) * (s - b) * (s - c));
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('s'),IntType()),VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType()),VarDecl(Id('c'),IntType()),VarDecl(Id('area'),IntType())],[Assign(Id('s'),BinaryOp('/',BinaryOp('+',BinaryOp('+',Id('a'),Id('b')),Id('c')),IntLiteral('2'))),Assign(Id('area'),CallExpr(Id('sqrt'),[BinaryOp('*',BinaryOp('*',BinaryOp('*',Id('s'),BinaryOp('-',Id('s'),Id('a'))),BinaryOp('-',Id('s'),Id('b'))),BinaryOp('-',Id('s'),Id('c')))]))])]))
        self.assertTrue(TestAST.test(input,expect,386))

    def test_get_the_area_of_a_circle(self):
        input = """procedure main();
        var radius, area, PI: real;
        begin
            area := PI * pow(radius, 2);
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('radius'),FloatType()),VarDecl(Id('area'),FloatType()),VarDecl(Id('PI'),FloatType())],[Assign(Id('area'),BinaryOp('*',Id('PI'),CallExpr(Id('pow'),[Id('radius'),IntLiteral('2')])))])]))
        self.assertTrue(TestAST.test(input,expect,387))

    def test_get_the_area_of_a_trapezium(self):
        input = """procedure main();
        var a, b, h, area: real;
        begin
            area := 0.5 * (a + b) * h;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),FloatType()),VarDecl(Id('b'),FloatType()),VarDecl(Id('h'),FloatType()),VarDecl(Id('area'),FloatType())],[Assign(Id('area'),BinaryOp('*',BinaryOp('*',FloatLiteral('0.5'),BinaryOp('+',Id('a'),Id('b'))),Id('h')))])]))
        self.assertTrue(TestAST.test(input,expect,388))

    def test_get_the_perimeter_of_a_triangle(self):
        input = """procedure main();
        var a, b, c, perimeter: real;
        begin
            perimeter := a+b+c;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),FloatType()),VarDecl(Id('b'),FloatType()),VarDecl(Id('c'),FloatType()),VarDecl(Id('perimeter'),FloatType())],[Assign(Id('perimeter'),BinaryOp('+',BinaryOp('+',Id('a'),Id('b')),Id('c')))])]))
        self.assertTrue(TestAST.test(input,expect,389))

    def test_get_the_surface_area_and_volume_of_a_cube(self):
        input = """procedure main();
        var side, surfacearea, volume: real;
        begin
            surfacearea := 6.0 * side * side;
            volume := pow(side, 3);
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('side'),FloatType()),VarDecl(Id('surfacearea'),FloatType()),VarDecl(Id('volume'),FloatType())],[Assign(Id('surfacearea'),BinaryOp('*',BinaryOp('*',FloatLiteral('6.0'),Id('side')),Id('side'))),Assign(Id('volume'),CallExpr(Id('pow'),[Id('side'),IntLiteral('3')]))])]))
        self.assertTrue(TestAST.test(input,expect,390))

    def test_get_the_total_sum_of_1_to_n(self):
        input = """procedure main();
        var n, sum: integer;
        begin
            sum := 0;
            for i :=1 to n do sum := sum+i;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('n'),IntType()),VarDecl(Id('sum'),IntType())],[Assign(Id('sum'),IntLiteral('0')),For(Id('i'),IntLiteral('1'),Id('n'),'True',[Assign(Id('sum'),BinaryOp('+',Id('sum'),Id('i')))])])]))
        self.assertTrue(TestAST.test(input,expect,391))

    def test_get_the_total_sum_of_1_to_n_mul_n(self):
        input = """procedure main();
        var n,sum: integer;
        begin
            sum := 0;
            for i :=1 to n do sum := sum+i*i;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('n'),IntType()),VarDecl(Id('sum'),IntType())],[Assign(Id('sum'),IntLiteral('0')),For(Id('i'),IntLiteral('1'),Id('n'),'True',[Assign(Id('sum'),BinaryOp('+',Id('sum'),BinaryOp('*',Id('i'),Id('i'))))])])]))        
        self.assertTrue(TestAST.test(input,expect,392))

    def test_get_the_total_sum_of_1_to_1_divide_n(self):
        input = """
        procedure main();
        var n, sum: integer;
        begin
            sum := 0;
            for i := 1 to n do sum := sum + 1/i;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('n'),IntType()),VarDecl(Id('sum'),IntType())],[Assign(Id('sum'),IntLiteral('0')),For(Id('i'),IntLiteral('1'),Id('n'),'True',[Assign(Id('sum'),BinaryOp('+',Id('sum'),BinaryOp('/',IntLiteral('1'),Id('i'))))])])]))
        self.assertTrue(TestAST.test(input,expect,393))

    def test_get_all_the_even_number_in_an_array(self):
        input = """
        procedure main();
        var a, b: array [0 .. 4] of integer;
        begin
            for i := 0 to 3 do 
                if (a[i] div 2 = 0) then b[i] := a[i];
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType('0','4',IntType())),VarDecl(Id('b'),ArrayType('0','4',IntType()))],[For(Id('i'),IntLiteral('0'),IntLiteral('3'),'True',[If(BinaryOp('=',BinaryOp('div',ArrayCell(Id('a'),Id('i')),IntLiteral('2')),IntLiteral('0')),[Assign(ArrayCell(Id('b'),Id('i')),ArrayCell(Id('a'),Id('i')))],[])])])]))
        self.assertTrue(TestAST.test(input,expect,394))

    def test_get_the_mean_of_three_integers(self):
        input = """procedure main();
        var a, b, c: integer;
            mean: real;
        begin
            mean := (a+b+c)/3;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType()),VarDecl(Id('c'),IntType()),VarDecl(Id('mean'),FloatType())],[Assign(Id('mean'),BinaryOp('/',BinaryOp('+',BinaryOp('+',Id('a'),Id('b')),Id('c')),IntLiteral('3')))])]))
        self.assertTrue(TestAST.test(input,expect,395))

    def test_get_the_mean_of_n_integers(self):
        input = """procedure main();
        var i, n, sum, mean: integer;
        begin
            for i := 1 to n do sum := sum+i;
            sum := sum/n;
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('i'),IntType()),VarDecl(Id('n'),IntType()),VarDecl(Id('sum'),IntType()),VarDecl(Id('mean'),IntType())],[For(Id('i'),IntLiteral('1'),Id('n'),'True',[Assign(Id('sum'),BinaryOp('+',Id('sum'),Id('i')))]),Assign(Id('sum'),BinaryOp('/',Id('sum'),Id('n')))])]))
        self.assertTrue(TestAST.test(input,expect,396))

    def test_get_the_solution_of_fibonacci_function(self):
        input = """
        procedure main();
        var n0, n1, n: integer;
        begin
            n0 := 0;
            n1 := 1;
            for i := 0 to n do
            begin
                if n = 0 then sol := 0;
                if n = 1 then sol := 1;
                sol := n1+n0; 
                n0 := n1;
                n1 := sol;
            end
        end"""
        expect = str(Program([FuncDecl(Id('main'),[],[VarDecl(Id('n0'),IntType()),VarDecl(Id('n1'),IntType()),VarDecl(Id('n'),IntType())],[Assign(Id('n0'),IntLiteral('0')),Assign(Id('n1'),IntLiteral('1')),For(Id('i'),IntLiteral('0'),Id('n'),'True',[If(BinaryOp('=',Id('n'),IntLiteral('0')),[Assign(Id('sol'),IntLiteral('0'))],[]),If(BinaryOp('=',Id('n'),IntLiteral('1')),[Assign(Id('sol'),IntLiteral('1'))],[]),Assign(Id('sol'),BinaryOp('+',Id('n1'),Id('n0'))),Assign(Id('n0'),Id('n1')),Assign(Id('n1'),Id('sol'))])])]))
        self.assertTrue(TestAST.test(input,expect,397))

    def test_complex_mixture_of_expressions(self):
        input = """function func(x: integer): integer;
        begin
            return x*3/4-24;
        end

        procedure main();
        var x: integer; y: array [0 .. 18] of integer;
        begin
            x := func(x)/3 + y[x-3*1/(6-2)] * y + 8/2;
        end"""
        expect = str(Program([FuncDecl(Id('func'),[VarDecl(Id('x'),IntType())],[],[Return((BinaryOp('-',BinaryOp('/',BinaryOp('*',Id('x'),IntLiteral('3')),IntLiteral('4')),IntLiteral('24'))))],IntType()),FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType()),VarDecl(Id('y'),ArrayType('0','18',IntType()))],[Assign(Id('x'),BinaryOp('+',BinaryOp('+',BinaryOp('/',CallExpr(Id('func'),[Id('x')]),IntLiteral('3')),BinaryOp('*',ArrayCell(Id('y'),BinaryOp('-',Id('x'),BinaryOp('/',BinaryOp('*',IntLiteral('3'),IntLiteral('1')),BinaryOp('-',IntLiteral('6'),IntLiteral('2'))))),Id('y'))),BinaryOp('/',IntLiteral('8'),IntLiteral('2'))))])]))
        self.assertTrue(TestAST.test(input,expect,398))

    def test_AndThen_OrElse_Operation(self):
        input = """procedure main();
        begin
            if (x > 1) and Then (x < 2) then return true;
            else 
                if (x < 1) or ELSE (x > 2) then return false;
        end
        """
        expect = str(Program([FuncDecl(Id('main'),[],[],[If(BinaryOp('andthen',BinaryOp('>',Id('x'),IntLiteral('1')),BinaryOp('<',Id('x'),IntLiteral('2'))),[Return((BooleanLiteral('true')))],[If(BinaryOp('orelse',BinaryOp('<',Id('x'),IntLiteral('1')),BinaryOp('>',Id('x'),IntLiteral('2'))),[Return((BooleanLiteral('false')))],[])])])]))
        self.assertTrue(TestAST.test(input,expect,399))

    def test_complicated_program(self):
        input = """function func(x, y: integer): integer;
        begin
             return x-3*y+1/4-(12+7/x);
        end

        function func2(x, y: integer): integer;
        begin
            y := x[1]*(3/6+2);
            return 2*x[2]/3+4/3*(x-3);
        end

        procedure main();
        var x,y: integer;
            z: array [0 .. 2] of integer;
        begin
            if x = y then func(x,z[0]);
            else
                if x > y then func(y,z[1]); else func2(z,z[2]);
         end"""
        expect = str(Program([FuncDecl(Id('func'),[VarDecl(Id('x'),IntType()),VarDecl(Id('y'),IntType())],[],[Return((BinaryOp('-',BinaryOp('+',BinaryOp('-',Id('x'),BinaryOp('*',IntLiteral('3'),Id('y'))),BinaryOp('/',IntLiteral('1'),IntLiteral('4'))),BinaryOp('+',IntLiteral('12'),BinaryOp('/',IntLiteral('7'),Id('x'))))))],IntType()),FuncDecl(Id('func2'),[VarDecl(Id('x'),IntType()),VarDecl(Id('y'),IntType())],[],[Assign(Id('y'),BinaryOp('*',ArrayCell(Id('x'),IntLiteral('1')),BinaryOp('+',BinaryOp('/',IntLiteral('3'),IntLiteral('6')),IntLiteral('2')))),Return((BinaryOp('+',BinaryOp('/',BinaryOp('*',IntLiteral('2'),ArrayCell(Id('x'),IntLiteral('2'))),IntLiteral('3')),BinaryOp('*',BinaryOp('/',IntLiteral('4'),IntLiteral('3')),BinaryOp('-',Id('x'),IntLiteral('3'))))))],IntType()),FuncDecl(Id('main'),[],[VarDecl(Id('x'),IntType()),VarDecl(Id('y'),IntType()),VarDecl(Id('z'),ArrayType('0','2',IntType()))],[If(BinaryOp('=',Id('x'),Id('y')),[CallStmt(Id('func'),[Id('x'),ArrayCell(Id('z'),IntLiteral('0'))])],[If(BinaryOp('>',Id('x'),Id('y')),[CallStmt(Id('func'),[Id('y'),ArrayCell(Id('z'),IntLiteral('1'))])],[CallStmt(Id('func2'),[Id('z'),ArrayCell(Id('z'),IntLiteral('2'))])])])])]))
        self.assertTrue(TestAST.test(input,expect,400))