
        procedure main ();
        var i1, i2: integer;
        begin
            for i1 := 1 to 10 do
            begin
                for i2 := 1 to 10 do
                begin
                    putIntLn(i2);
                    if (i2 = i1 - 1) then
                    begin   
                        
                        continue;
                    end
                    if (i2 = i1) then 
                    begin
                        
                        break;
                    end
                end
                if (i1 = 3) then 
                begin
                    
                    Continue;
                end
                if (i1 = 5) then 
                begin
                
                    break;
                end
            end
            return;
        end