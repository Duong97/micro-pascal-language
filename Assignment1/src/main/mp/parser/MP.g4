/**
 * Student name: Nguyen Hai Duong
 * Student ID: 1552085
 */
grammar MP;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}

//PROGRAM
program: deff+ EOF;
deff: vardec | funcdec | procdec;

vartype: INTTYPE|BOOLEANTYPE|STRINGTYPE|FLOATTYPE|arraytype;
arraytype: ARRAY LSB expr DOUBLEDOT expr RSB OF vartype;

vardec: VAR (varlist SEMI)+;
varlist: id_list COLON vartype;
id_list: ID (COMMA ID)*;
funcdec: FUNCTION ID LB (paradec)* RB COLON (vartype|arraytype) SEMI vardec* body;

procdec: PROCEDURE ID LB (paradec)* RB SEMI vardec* body;
paradec: varlist SEMI | varlist;

body: BEGIN statement* END;

//STATEMENT
statement: funcall SEMI | assign_statement SEMI| if_s | while_s | for_s | br_s SEMI | cont_s SEMI | rt_s SEMI | with_s | body;
assign_statement: (expr OP16)+ expr;
if_s: IF expr THEN statement (ELSE statement)?;
while_s: WHILE expr DO statement;
for_s: FOR ID OP16 expr (TO | DOWNTO) expr DO statement;
br_s: BREAK;
cont_s: CONTINUE;
rt_s: RETURN expr?;
with_s: WITH (varlist SEMI)+ DO statement;
funcall: ID LB (expr (COMMA expr)*)? RB;

//EXPRESSION
expr: expr (OP7 ELSE | OP8 THEN) expr0
	| expr0;

expr0: expr1 (OP9 | OP10 | OP11 | OP12 | OP13 | OP14) expr1
	| expr1;

expr1: expr1 (OP1 | OP2 | OP7) expr2
	| expr2;

expr2: expr2 (OP4 | OP3 | OP6 | OP8 | OP15) expr3
	| expr3;

expr3: (OP2 | OP5) expr3 
	| expr4;

expr4: expr5 LSB expr RSB| expr5;

expr5: LB expr RB
	| ID
	| funcall
	| literal;

literal: INTLIT | FLOATLIT | BOOLLIT | STRINGLIT;

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
fragment Char: ('\\'[bfnrt'"\\]) | ~[\b\f\r\n\t'"\\];

//TYPE
INTTYPE: I N T E G E R ;
BOOLEANTYPE: B O O L E A N ;
STRINGTYPE: S T R I N G ;
FLOATTYPE: R E A L ;

//KEYWORD
BREAK: B R E A K ;
CONTINUE: C O N T I N U E ;
ELSE: E L S E ;
FOR: F O R ;
IF: I F ;
RETURN: R E T U R N ;
DO: D O ;
WHILE: W H I L E ;
TO: T O ; 
DOWNTO: D O W N T O ;
THEN: T H E N ;
BEGIN: B E G I N ;
END: E N D ;
FUNCTION: F U N C T I O N ;
PROCEDURE: P R O C E D U R E ;
VAR: V A R ;
ARRAY: A R R A Y ;
OF: O F ;
WITH: W I T H ;

//OPERATION
OP1: '+';
OP2: '-';
OP3: '*';
OP4: '/';
OP5: N O T;
OP6: M O D ;
OP7: O R ;
OP8: A N D ;
OP9: '<>';
OP10: '=';
OP11: '<';
OP12: '>';
OP13: '<=';
OP14: '>=';
OP15: D I V ;
OP16: COLON OP10;

//Lexical Speci?cation
LSB: '[' ;
RSB: ']' ;
LB: '(' ;
RB: ')' ;
LP: '{';
RP: '}';
SEMI: ';' ;
COMMA: ',' ;
DOUBLEDOT: '..';
COLON: ':';

FLOATLIT:  [0-9]+(('.'[0-9]*[eE][-]?[0-9]+) | ('.'[0-9]*) | ([eE][-]?[0-9]+)) | ('.'[0-9]+([eE][-]?[0-9]+)?);
INTLIT: [0-9]+;

BOOLLIT: T R U E | F A L S E;

STRINGLIT: '"'Char*'"' {self.text =  self.text[1:-1]}; 

ID: [A-Za-z_]+[A-Za-z0-9_]*;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
COMMENT: (('(''*'.*'*'')') | ('//'~[\n\r]*) | ('{'.*'}')) -> skip;
ILLEGAL_ESCAPE: '"'Char* '\\'~[bfrnt'"\\] { raise IllegalEscape(self.text[1:]) } ;
UNCLOSE_STRING: '"'Char* { raise UncloseString(self.text[1:]) } ;
ERROR_CHAR: . { raise ErrorToken(self.text) };