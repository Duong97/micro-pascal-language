import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        input = """ procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,201))

    def test_more_complex_program(self):
        input = """ procedure main();
        BEGIN
        putIntLn(4);
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,202))
      
    def test_wrong_program(self):
        input = """procedure main();
        BEGIN
        """ 
        expect ="Error on line 3 col 8: <EOF>"
        self.assertTrue(TestParser.test(input,expect,203))

    def test_line_comment1(self):
        input = """//line comment1
        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,204))

    def test_line_comment2(self):
        input = """procedure main();
        BEGIN
        //line comment2
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,205))

    def test_block_comment1(self):
        input = """(* block comment1 *)
        procedure main();
        BEGIN

        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,206))

    def test_block_comment2(self):
        input = """{ block comment2 }
        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,207))

    def test_block_comment3(self):
        input = """
        procedure main();
        BEGIN
        { block comment3 }
        (* block comment3 *)
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,208))

    def test_global_variable_declaration_1(self):
        input = """var a: integer;
        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,209))

    def test_global_variable_declaration_2(self):
        input = """var a, b: boolean;
        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,210))

    def test_global_variable_declaration_3(self):
        input = """var a: integer;
                        b: array [1 .. 3] of string;
        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,211))

    def test_function_declaration_1(self):
        input = """function foo(): real; 
        begin end 

        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,212))

    def test_function_declaration_2(self):
        input = """function foo(i: integer): real; 
        begin end 

        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,213))

    def test_function_declaration_3(self):
        input = """function foo(i: integer; a, b: real): real; 
        begin end 

        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,214))

    def test_function_declaration_4(self):
        input = """function foo(i: boolean): real; 
        var b: real;
        begin 
        end 

        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,215))

    def test_function_declaration_5(self):
        input = """function foo(i: boolean): real; 
        var b: string;
        begin 
            b := "hello";
        end 

        procedure main();
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,216))

    def test_local_variable_declaration_1(self):
        input = """procedure main();
        VAR b: real;
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,217))

    def test_local_variable_declaration_2(self):
        input = """procedure main();
        VAR a, b: real;
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,218))

    def test_local_variable_declaration_3(self):
        input = """procedure main();
        VAR a, b: real;
            c: array [1 .. 10] of integer;
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,219))

    def test_local_variable_declaration_4(self):
        input = """procedure main();
        VAR a, b: real;
            c, d: array [1 .. -1] of integer;
        BEGIN
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,220))

    def test_variable_initialization_1(self):
        input = """procedure main();
        VAR a: real;
        BEGIN
            a := 1.0;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,221))

    def test_variable_initialization_2(self):
        input = """procedure main();
        VAR a, b: real;
        BEGIN
            a := 1.0;
            b := 12.11;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,222))

    def test_variable_initialization_3(self):
        input = """procedure main();
        VAR a: real;
            b: string;
        BEGIN
            a := 1.0;
            b := "Hello";
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,223))

    def test_variable_initialization_4(self):
        input = """procedure main();
        VAR a: real;
            b: boolean;
        BEGIN
            a := 1.0;
            b := true;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,224))

    def test_call_function1(self):
        input = """function foo(i: boolean): real; 
        var b: real;
        begin 
            b := 1.0;
            return b;
        end 

        procedure main();
        BEGIN
            foo(true);
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,225))

    def test_call_function2(self):
        input = """function foo(i: boolean): string; 
        var b: string;
        begin 
            b := "hello";
            return b;
        end 

        procedure main();
        VaR b: string;
        BEGIN
            b := foo(true);
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,226))

    def test_call_function3(self):
        input = """function foo(i: boolean): string; 
        var b: string;
        begin 
            b := "hello";
            return b;
        end 

        procedure main();
        VaR b: string;
        BEGIN
            b := foo(foo(true));
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,227))

    def test_if_else1(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            b := true;
            if (b = true) then return true;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,228))

    def test_if_else2(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            b := true;
            if (b = true) then 
            beGin
                b := false;
                return b;
            eNd
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,229))

    def test_if_else3(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            b := "a";
            if (b <= "a") then return true;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,230))

    def test_if_else4(self):
        input = """procedure main();
        VaR b: string;
            a: real;
        BEGIN
            b := "a";
            if (b <= "a") then return true;
            else  a := 1.0;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,231))

    def test_while1(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            while (true) do putString("HEY");
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,232))

    def test_while2(self):
        input = """procedure main();
        VaR a: integer;
        BEGIN
            a := 1;
            while (a < 2) do putString("HEY");
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,233))

    def test_while3(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            while (true) do 
            begin
                putString("HEY");
            end
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,234))

    def test_for1(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            for x := "a" to "z" do putString("HaHa");
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,235))

    def test_for2(self):
        input = """procedure main();
        VaR b: string;
        BEGIN
            for x := "a" to "z" do 
            begIn
                putString("HaHa");
            enD
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,236))

    def test_break_statement(self):
        input = """procedure main();
        VaR x: integer;
        BEGIN
            x := 0;
            while (x <= 1000) do 
            begin
                x := x + 1;
                putString("HEY");
                if (x = 500) then break;
            end
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,237))

    def test_continue_statement(self):
        input = """procedure main();
        VaR x: integer;
        BEGIN
            x := 0;
            while (x <= 1000) do 
            begin
                x := x + 1;
                putString("HEY");
                if (x = 500) then break;
                else conTInuE;
            end
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,238))

    def test_return_statement1(self):
        input = """procedure main();
        VaR x: integer;
        BEGIN
            x := 12;
            return x;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,239))

    def test_return_statement2(self):
        input = """procedure main();
        BEGIN
            return 1;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,240))

    def test_return_statement3(self):
        input = """procedure main();
        BEGIN
            if (2<1) then return 1.0;
            else return "HEY";
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,241))

    def test_return_statement4(self):
        input = """procedure main();
        BEGIN
            if (1>2) then return true;
            else return false;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,242))

    def test_Assign_statement1(self):
        input = """procedure main();
        BEGIN
            a :=1;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,243))

    def test_Assign_statement2(self):
        input = """procedure main();
        BEGIN
            a := b := 1.0;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,244))

    def test_Assign_statement3(self):
        input = """procedure main();
        BEGIN
            a := true;
            b := "HEY";
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,245))

    def test_Assign_statement4(self):
        input = """procedure main();
        BEGIN
            a := b := f[3] := "HEY";
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,246))

    def test_Assign_statement5(self):
        input = """procedure main();
        BEGIN
            a := b[10] := foo ()[3] := x := 1 ; 
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,247))

    def test_Assign_statement6(self):
        input = """procedure main();
        BEGIN
            a := b[10] := foo ()[3] := x := 1 ; 
            b := "HEY";
            f()[13] := true;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,248))

    def test_Assign_statement7(self):
        input = """procedure main();
        BEGIN
            a := b[10] := foo ()[3] := x := 1 ; 
            a[3] := b[4] := c := 12.11;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,249))

    def test_operation_precedence1(self):
        input = """procedure main();
        BEGIN
            a := -1;
            b := NOT truE;
         
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,250))

    def test_operation_precedence2(self):
        input = """procedure main();
        BEGIN
            a := a/3 := b*6*a := l dIv c;
            if (2 >1) anD (1 < 3) then b:= c mOd 20;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,251))

    def test_operation_precedence3(self):
        input = """procedure main();
        BEGIN
            a := x + 1 := a - -1;
            if (2 >1) OR (1 <3) then b:= b + 100;
         
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,252))

    def test_operation_precedence4(self):
        input = """procedure main();
        BEGIN
            
            if (a = 2) and (b[3] <> 3) and (c <1) and (d <= 3) and (e(3) > 1) and (f() >= 100) then b:= b + 100;
         
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,253))

    def test_operation_precedence5(self):
        input = """procedure main();
        BEGIN
            if (2 >1) OR eLsE (1 <3) then b:= b + 100;
            if (2 >1) AND tHen (1 >3) then b:= b + 99;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,254))

    def test_several_statements1(self):
        input = """procedure main();
        BEGIN
            a := 2 >1;
            b := 3 -1 +3;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,255))

    def test_several_statements2(self):
        input = """procedure main();
        BEGIN
            a := 2 >1;
            b := 3 -1 +3;
            foo(2)[3+x] := a[b[2]] +3; 
         
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,256))

    def test_several_statements3(self):
        input = """procedure main();
        BEGIN
            if (a = 1) then a := 2 >1;
            else b := 3 -1 +3;
            while (true) do foo(2)[3+x] := a[b[2]] +3; 
            with a,b: integer ;c: array [1 .. 2] of real ; do return "hello";
         
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,257))

    def test_block_comment_error1(self):
        input = """{a
        procedure main();
        BEGIn
        END
        """ 
        expect ="Error on line 1 col 0: {"
        self.assertTrue(TestParser.test(input,expect,258))

    def test_block_comment_error2(self):
        input = """(* a
        procedure main();
        BEGIn
        END
        """ 
        expect ="Error on line 1 col 0: ("
        self.assertTrue(TestParser.test(input,expect,259))

    def test_line_comment_error1(self):
        input = """/a
        procedure main();
        BEGIn
        END
        """ 
        expect ="Error on line 1 col 0: /"
        self.assertTrue(TestParser.test(input,expect,260))

    def test_no_semicolon_error_1(self):
        input = """procedure main()
        BEGIn
        END
        """ 
        expect ="Error on line 2 col 8: BEGIn"
        self.assertTrue(TestParser.test(input,expect,261))

    def test_no_semicolon_error_2(self):
        input = """procedure main();
        BEGIn
            a := 1
        END
        """ 
        expect ="Error on line 4 col 8: END"
        self.assertTrue(TestParser.test(input,expect,262))

    def test_declaration_error1(self):
        input = """procedure main();
        i: integer;
        BEGIn
            i := 1;
        END
        """ 
        expect ="Error on line 2 col 8: i"
        self.assertTrue(TestParser.test(input,expect,263))

    def test_declaration_error2(self):
        input = """procedure main();
        var i integer;
        BEGIn
            i := 1;
        END
        """ 
        expect ="Error on line 2 col 14: integer"
        self.assertTrue(TestParser.test(input,expect,264))

    def test_declaration_error3(self):
        input = """procedure main();
        var i b: integer;
        BEGIn
            i := 1;
        END
        """ 
        expect ="Error on line 2 col 14: b"
        self.assertTrue(TestParser.test(input,expect,265))

    def test_declaration_error4(self):
        input = """procedure main();
        i, b: integer;
        BEGIn
            i := 1;
        END
        """ 
        expect ="Error on line 2 col 8: i"
        self.assertTrue(TestParser.test(input,expect,266))

    def test_declaration_error5(self):
        input = """procedure main();
        var i, b: integer;
            d: array [-2 .. 10] o f real;
        BEGIn
            i := 1;
        END
        """ 
        expect ="Error on line 3 col 32: o"
        self.assertTrue(TestParser.test(input,expect,267))

    def test_initialization_error1(self):
        input = """procedure main();
        var i, b: integer;
        BEGIn
            i = 1;
        END
        """ 
        expect ="Error on line 4 col 17: ;"
        self.assertTrue(TestParser.test(input,expect,268))

    def test_initialization_error2(self):
        input = """procedure main();
        var i, b: integer;
        BEGIn
            i = i + 1;
        END
        """ 
        expect ="Error on line 4 col 21: ;"
        self.assertTrue(TestParser.test(input,expect,269))

    def test_function_parameter_error1(self):
        input = """function f1 (i integer): boolean; 
        begin  
            return true; 
        end

        procedure main();
        BEGIn
        END
        """ 
        expect ="Error on line 1 col 15: integer"
        self.assertTrue(TestParser.test(input,expect,270))

    def test_function_parameter_error2(self):
        input = """function foo(a,b: integer ;c: real ) 
        var x,y: real ;
        begin 
        end

        procedure main();
        BEGIn
        END
        """ 
        expect ="Error on line 2 col 8: var"
        self.assertTrue(TestParser.test(input,expect,271))

    def test_if_error1(self):
        input = """procedure main();
        BEGIN
            If()  then return true;
        END
        """ 
        expect ="Error on line 3 col 15: )"
        self.assertTrue(TestParser.test(input,expect,272))

    def test_if_error2(self):
        input = """procedure main();
        BEGIN
            else a:= 1;
        END
        """ 
        expect ="Error on line 3 col 12: else"
        self.assertTrue(TestParser.test(input,expect,273))

    def test_while_error1(self):
        input = """procedure main();
        BEGIN
            wHile() do a[1] := "hello";
        END
        """ 
        expect ="Error on line 3 col 18: )"
        self.assertTrue(TestParser.test(input,expect,274))

    def test_while_error2(self):
        input = """procedure main();
        BEGIN
            wHile(true) a[1] := 0;
        END
        """ 
        expect ="Error on line 3 col 24: a"
        self.assertTrue(TestParser.test(input,expect,275))

    def test_while_error3(self):
        input = """procedure main();
        BEGIN
            dO a[1] := 0;
        END
        """ 
        expect ="Error on line 3 col 12: dO"
        self.assertTrue(TestParser.test(input,expect,276))

    def test_for_error1(self):
        input = """procedure main();
        BEGIN
            for i = 1 to 10 do a[i] := i;
        END
        """ 
        expect ="Error on line 3 col 18: ="
        self.assertTrue(TestParser.test(input,expect,277))

    def test_return_error1(self):
        input = """procedure main();
        BEGIN
            return integer;
        END
        """ 
        expect ="Error on line 3 col 19: integer"
        self.assertTrue(TestParser.test(input,expect,278))

    def test_return_error2(self):
        input = """procedure main();
        BEGIN
            return continuE;
        END
        """ 
        expect ="Error on line 3 col 19: continuE"
        self.assertTrue(TestParser.test(input,expect,279))

    def test_expression_error1(self):
        input = """procedure main();
        BEGIN
            while (x >) do a := 1;
        END
        """ 
        expect ="Error on line 3 col 22: )"
        self.assertTrue(TestParser.test(input,expect,280))

    def test_expression_error2(self):
        input = """procedure main();
        BEGIN
            a := 1 - ;
        END
        """ 
        expect ="Error on line 3 col 21: ;"
        self.assertTrue(TestParser.test(input,expect,281))

    def test_expression_error3(self):
        input = """procedure main();
        BEGIN
            a := 1 -3/;
        END
        """ 
        expect ="Error on line 3 col 22: ;"
        self.assertTrue(TestParser.test(input,expect,282))

    def test_expression_error4(self):
        input = """procedure main();
        BEGIN
            a -  := 1 -3;
        END
        """ 
        expect ="Error on line 3 col 17: :="
        self.assertTrue(TestParser.test(input,expect,283))

    def test_expression_error5(self):
        input = """procedure main();
        BEGIN
            while (not) do return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 22: )"
        self.assertTrue(TestParser.test(input,expect,284))

    def test_expression_error6(self):
        input = """procedure main();
        BEGIN
            a :=  x -[3];
        END
        """ 
        expect ="Error on line 3 col 21: ["
        self.assertTrue(TestParser.test(input,expect,285))

    def test_expression_error7(self):
        input = """procedure main();
        BEGIN
            while (=2) do return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 19: ="
        self.assertTrue(TestParser.test(input,expect,286))

    def test_expression_error8(self):
        input = """procedure main();
        BEGIN
            if (i>) then return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 18: )"
        self.assertTrue(TestParser.test(input,expect,287))

    def test_expression_error9(self):
        input = """procedure main();
        BEGIN
            if (<>) then return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 16: <>"
        self.assertTrue(TestParser.test(input,expect,288))

    def test_expression_error10(self):
        input = """procedure main();
        BEGIN
            if (1 <= ) then return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 21: )"
        self.assertTrue(TestParser.test(input,expect,289))

    def test_expression_error11(self):
        input = """procedure main();
        BEGIN
            if (1 >1 >2 ) then return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 21: >"
        self.assertTrue(TestParser.test(input,expect,290))

    def test_expression_error12(self):
        input = """procedure main();
        BEGIN
            if (1 +1 <= 2 aNd) then return "ha ha";
        END
        """ 
        expect ="Error on line 3 col 29: )"
        self.assertTrue(TestParser.test(input,expect,291))

    def test_expression_error13(self):
        input = """procedure main();
        BEGIN
            a := 2 -;
        END
        """ 
        expect ="Error on line 3 col 20: ;"
        self.assertTrue(TestParser.test(input,expect,292))

    def test_expression_error14(self):
        input = """procedure main();
        BEGIN
            a := 2 > 1 >= 0;
        END
        """ 
        expect ="Error on line 3 col 23: >="
        self.assertTrue(TestParser.test(input,expect,293))

    def test_expression_error15(self):
        input = """procedure main();
        BEGIN
            b := a[3>1>1];
        END
        """ 
        expect ="Error on line 3 col 22: >"
        self.assertTrue(TestParser.test(input,expect,294))

    def test_expression_error16(self):
        input = """procedure main();
        BEGIN
            b := a[3] (*) b;
        END
        """ 
        expect ="Error on line 3 col 22: ("
        self.assertTrue(TestParser.test(input,expect,295))

    def test_expression_error17(self):
        input = """procedure main();
        BEGIN
            b := b + c NOT;
        END
        """ 
        expect ="Error on line 3 col 23: NOT"
        self.assertTrue(TestParser.test(input,expect,296))

    def test_expression_error18(self):
        input = """procedure main();
        BEGIN
            if (a := 2) then return "hey";
        END
        """ 
        expect ="Error on line 3 col 18: :="
        self.assertTrue(TestParser.test(input,expect,297))

    def test_expression_error19(self):
        input = """procedure main();
        BEGIN
            a := 3/;
        END
        """ 
        expect ="Error on line 3 col 19: ;"
        self.assertTrue(TestParser.test(input,expect,298))

    def test_expression_error20(self):
        input = """procedure main();
        BEGIN
            if (a > 1 Or b < 2 <3) then return "hey";
        END
        """ 
        expect ="Error on line 3 col 27: <"
        self.assertTrue(TestParser.test(input,expect,299))

    def test_with_statement(self):
        input = """procedure main();
        BEGIN
            with a,b: integer; c: array [1 .. 2] of real ; do d := c[a] + b;
        END
        """ 
        expect ="successful"
        self.assertTrue(TestParser.test(input,expect,300))







    
    















