import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
      
    def test_simple_block_comment1(self):
        self.assertTrue(TestLexer.test("(* This is a block comment *) ","<EOF>",101))

    def test_simple_block_comment2(self):
        self.assertTrue(TestLexer.test("{ This is a block comment } ","<EOF>",102))

    def test_simple_line_comment1(self):
        self.assertTrue(TestLexer.test("//This is a line comment ","<EOF>",103))

    def test_simple_line_comment2(self):
        self.assertTrue(TestLexer.test("ab//cde","ab,<EOF>",104))

    def test_traditional_block_comment_1(self):
        self.assertTrue(TestLexer.test("hey(* This is a block comment *) ","hey,<EOF>",105))

    def test_traditional_block_comment_2(self):
        self.assertTrue(TestLexer.test("hey{ This is a block comment} ","hey,<EOF>",106))

    def test_traditional_block_comment_with_a_new_line(self):
        self.assertTrue(TestLexer.test("{ This is a block \n comment} ","<EOF>",107))

    def test_identifer1(self):
        self.assertTrue(TestLexer.test("abc","abc,<EOF>",108))

    def test_identifer2(self):
        self.assertTrue(TestLexer.test("Aa","Aa,<EOF>",109))

    def test_identifer3(self):
        self.assertTrue(TestLexer.test("_a","_a,<EOF>",110))

    def test_identifer4(self):
        self.assertTrue(TestLexer.test("__","__,<EOF>",111))

    def test_identifer5(self):
        self.assertTrue(TestLexer.test("a12","a12,<EOF>",112))

    def test_identifer6(self):
        self.assertTrue(TestLexer.test("_39","_39,<EOF>",113))

    def test_keyword_int(self):
        self.assertTrue(TestLexer.test("iNt x","iNt,x,<EOF>",114))

    def test_keyword_boolean(self):
        self.assertTrue(TestLexer.test("bOoLean x","bOoLean,x,<EOF>",115))

    def test_keyword_string(self):
        self.assertTrue(TestLexer.test("String x","String,x,<EOF>",116))

    def test_keyword_float(self):
        self.assertTrue(TestLexer.test("REAL x","REAL,x,<EOF>",117))

    def test_keyword_true(self):
        self.assertTrue(TestLexer.test("x := true","x,:=,true,<EOF>",118))

    def test_keyword_false(self):
        self.assertTrue(TestLexer.test("x := false","x,:=,false,<EOF>",119))

    def test_keyword_break(self):
        self.assertTrue(TestLexer.test("break","break,<EOF>",120))

    def test_keyword_if(self):
        self.assertTrue(TestLexer.test("if","if,<EOF>",121))

    def test_keyword_for(self):
        self.assertTrue(TestLexer.test("for","for,<EOF>",122))

    def test_keyword_else(self):
        self.assertTrue(TestLexer.test("else","else,<EOF>",123))

    def test_keyword_continue(self):
        self.assertTrue(TestLexer.test("continue;","continue,;,<EOF>",124))

    def test_keyword_while(self):
        self.assertTrue(TestLexer.test("while (x<y)","while,(,x,<,y,),<EOF>",125))

    def test_keyword_do(self):
        self.assertTrue(TestLexer.test("do x:=1;","do,x,:=,1,;,<EOF>",126))

    def test_keyword_return(self):
        self.assertTrue(TestLexer.test("return 0;","return,0,;,<EOF>",127))

    def test_keywords_and_other_tokens_1(self):
        self.assertTrue(TestLexer.test("if(x=1) then return 0;","if,(,x,=,1,),then,return,0,;,<EOF>",128))
   
    def test_keywords_and_other_tokens_2(self):
        self.assertTrue(TestLexer.test("while(x>1) do x=x-1;","while,(,x,>,1,),do,x,=,x,-,1,;,<EOF>",129))
   
    def test_Add_operator(self):
        self.assertTrue(TestLexer.test("1+1=2","1,+,1,=,2,<EOF>",130))

    def test_Sub_operator(self):
        self.assertTrue(TestLexer.test("2-1=1","2,-,1,=,1,<EOF>",131))
     
    def test_Mul_operator(self):
        self.assertTrue(TestLexer.test("3*4=12","3,*,4,=,12,<EOF>",132))
    
    def test_Divide_operator(self):
        self.assertTrue(TestLexer.test("12/4=3","12,/,4,=,3,<EOF>",133))
      
    def test_Mod_operator(self):
        self.assertTrue(TestLexer.test("4 MOD 3 = 1","4,MOD,3,=,1,<EOF>",134))
    
    def test_Not_operator(self):
        self.assertTrue(TestLexer.test("not false=true","not,false,=,true,<EOF>",135))

    def test_Or_operator(self):
        self.assertTrue(TestLexer.test("x or y","x,or,y,<EOF>",136))

    def test_And_operator(self):
        self.assertTrue(TestLexer.test("x and y","x,and,y,<EOF>",137))

    def test_Equal_operator(self):
        self.assertTrue(TestLexer.test("x = y","x,=,y,<EOF>",138))

    def test_Not_Equal_operator(self):
        self.assertTrue(TestLexer.test("x <> y","x,<>,y,<EOF>",139))

    def test_Less_than_operator(self):
        self.assertTrue(TestLexer.test("x < 4","x,<,4,<EOF>",140))

    def test_Greater_than_operator(self):
        self.assertTrue(TestLexer.test("4 > y","4,>,y,<EOF>",141))

    def test_less_than_or_equal_operator(self):
        self.assertTrue(TestLexer.test("x <= y","x,<=,y,<EOF>",142))

    def test_greater_than_or_equal_operator(self):
        self.assertTrue(TestLexer.test("x >= y","x,>=,y,<EOF>",143))

    def test_integer_div_operator(self):
        self.assertTrue(TestLexer.test("x Div y","x,Div,y,<EOF>",144))

    def test_assign_operator(self):
        self.assertTrue(TestLexer.test("x := y","x,:=,y,<EOF>",145))

    def test_AndThen_operators(self):
        self.assertTrue(TestLexer.test("x > 1 and     then x < 2","x,>,1,and,then,x,<,2,<EOF>",146))

    def test_OrElse_operators(self):
        self.assertTrue(TestLexer.test("x > 1 or else x < 2","x,>,1,or,else,x,<,2,<EOF>",147))   

    def test_operators_and_other_tokens1(self):
        self.assertTrue(TestLexer.test("if(x>3) then return 0;","if,(,x,>,3,),then,return,0,;,<EOF>",148))    

    def test_operators_and_other_tokens2(self):
        self.assertTrue(TestLexer.test("while(ifTrue=true) Do x=x+1;","while,(,ifTrue,=,true,),Do,x,=,x,+,1,;,<EOF>",149))

    def test_operators_and_other_tokens3(self):
        self.assertTrue(TestLexer.test("For x := 1 to 3 do a = x -1;","For,x,:=,1,to,3,do,a,=,x,-,1,;,<EOF>",150))

    def test_operators_and_other_tokens4(self):
        self.assertTrue(TestLexer.test("if(x=true or y =true) then x=x+y;","if,(,x,=,true,or,y,=,true,),then,x,=,x,+,y,;,<EOF>",151))

    def test_open_and_close_brackets(self):
        self.assertTrue(TestLexer.test("(abc)","(,abc,),<EOF>",152))

    def test_open_and_close_square_brackets(self):
        self.assertTrue(TestLexer.test("[abc]","[,abc,],<EOF>",153))

    def test_open_and_close_paretheses(self):
        self.assertTrue(TestLexer.test("}{","},{,<EOF>",154))

    def test_semicolon(self):
        self.assertTrue(TestLexer.test("abc;","abc,;,<EOF>",155))

    def test_comma(self):
        self.assertTrue(TestLexer.test("ab,c","ab,,,c,<EOF>",156))

    def test_seperators_and_other_tokens1(self):
        self.assertTrue(TestLexer.test("var i: integer;","var,i,:,integer,;,<EOF>",157))

    def test_seperators_and_other_tokens2(self):
        self.assertTrue(TestLexer.test(" VAR a, b: real; c, d: array [1 .. 10] of integer;","VAR,a,,,b,:,real,;,c,,,d,:,array,[,1,..,10,],of,integer,;,<EOF>",158))

    def test_simple_integer(self):
        self.assertTrue(TestLexer.test("123","123,<EOF>",159))

    def test_float_literal1(self):
        self.assertTrue(TestLexer.test("12.3","12.3,<EOF>",160))

    def test_float_literal2(self):
        self.assertTrue(TestLexer.test("4.","4.,<EOF>",161))

    def test_float_literal3(self):
        self.assertTrue(TestLexer.test(".8",".8,<EOF>",162))

    def test_float_literal4(self):
        self.assertTrue(TestLexer.test("12e4","12e4,<EOF>",163))

    def test_float_literal5(self):
        self.assertTrue(TestLexer.test("4e-12","4e-12,<EOF>",164))

    def test_float_literal6(self):
        self.assertTrue(TestLexer.test("1.2e3","1.2e3,<EOF>",165))

    def test_float_literal7(self):
        self.assertTrue(TestLexer.test("2.e5","2.e5,<EOF>",166))

    def test_float_literal8(self):
        self.assertTrue(TestLexer.test("4.e-2","4.e-2,<EOF>",167))

    def test_float_literal9(self):
        self.assertTrue(TestLexer.test("1.5e-6","1.5e-6,<EOF>",168))

    def test_float_literal10(self):
        self.assertTrue(TestLexer.test("6E2","6E2,<EOF>",169))

    def test_float_literal11(self):
        self.assertTrue(TestLexer.test("2E-3","2E-3,<EOF>",170))

    def test_float_literal12(self):
        self.assertTrue(TestLexer.test("4.E1","4.E1,<EOF>",171))

    def test_float_literal13(self):
        self.assertTrue(TestLexer.test("1.2E3","1.2E3,<EOF>",172))

    def test_float_literal14(self):
        self.assertTrue(TestLexer.test("3.E-4","3.E-4,<EOF>",173))

    def test_float_literal15(self):
        self.assertTrue(TestLexer.test("9.2E-4","9.2E-4,<EOF>",174))

    def test_simple_string1(self):
        self.assertTrue(TestLexer.test('"abc"',"abc,<EOF>",175))

    def test_simple_string2(self):
        self.assertTrue(TestLexer.test('"123"',"123,<EOF>",176))

    def test_simple_string3(self):
        self.assertTrue(TestLexer.test('"a123"',"a123,<EOF>",177))

    def test_backspace_in_string(self):
        self.assertTrue(TestLexer.test('"abc\\b"',"abc\\b,<EOF>",178))

    def test_horizontal_tab_in_string(self):
        self.assertTrue(TestLexer.test('"ab\\tc"',"ab\\tc,<EOF>",179))   

    def test_formfeed_in_string(self):
        self.assertTrue(TestLexer.test('"ab\\fc"',"ab\\fc,<EOF>",180))

    def test_backslash_in_string(self):
        self.assertTrue(TestLexer.test('"ab\\\\c"',"ab\\\\c,<EOF>",181))

    def test_newline_in_string(self):
        self.assertTrue(TestLexer.test('"abc\\n"',"abc\\n,<EOF>",182))

    def test_single_quote_in_string(self):
        self.assertTrue(TestLexer.test(""" "abc\\'" ""","abc\\',<EOF>",183))

    def test_double_quotes_in_string(self):
        self.assertTrue(TestLexer.test(""" "abc\\"" """,'abc\\",<EOF>',184))

    def test_error_token1(self):
        self.assertTrue(TestLexer.test("$","Error Token $",185))

    def test_error_token2(self):
        self.assertTrue(TestLexer.test("x^","x,Error Token ^",186))

    def test_error_token3(self):
        self.assertTrue(TestLexer.test("x1@","x1,Error Token @",187))

    def test_error_token4(self):
        self.assertTrue(TestLexer.test("#","Error Token #",188))

    def test_error_token5(self):
        self.assertTrue(TestLexer.test("12 ~","12,Error Token ~",189))

    def test_error_token6(self):
        self.assertTrue(TestLexer.test("|","Error Token |",190))

    def test_error_token7(self):
        self.assertTrue(TestLexer.test("&","Error Token &",191))

    def test_error_token8(self):
        self.assertTrue(TestLexer.test("!","Error Token !",192))

    def test_unclose_string1(self):
        self.assertTrue(TestLexer.test('"a','Unclosed String: a',193))

    def test_unclose_string2(self):
        self.assertTrue(TestLexer.test('123"abc','123,Unclosed String: abc',194))

    def test_unclose_string3(self):
        self.assertTrue(TestLexer.test('"c\\nde','Unclosed String: c\\nde',195))

    def test_unclose_string4(self):
        self.assertTrue(TestLexer.test('"cde\\b','Unclosed String: cde\\b',196))

    def test_unclose_string5(self):
        self.assertTrue(TestLexer.test('"cde\\t','Unclosed String: cde\\t',197))

    def test_illegal_escape_in_string1(self):
        self.assertTrue(TestLexer.test('"a\\m"','Illegal Escape In String: a\\m',198))

    def test_illegal_escape_in_string2(self):
        self.assertTrue(TestLexer.test('"a\\c"','Illegal Escape In String: a\\c',199))

    def test_illegal_escape_in_string3(self):
        self.assertTrue(TestLexer.test('"a\\x"','Illegal Escape In String: a\\x',200))


























