# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#deff.
    def visitDeff(self, ctx:MPParser.DeffContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#nor_type.
    def visitNor_type(self, ctx:MPParser.Nor_typeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#arraytype.
    def visitArraytype(self, ctx:MPParser.ArraytypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#vartype.
    def visitVartype(self, ctx:MPParser.VartypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#vardec.
    def visitVardec(self, ctx:MPParser.VardecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#varlist.
    def visitVarlist(self, ctx:MPParser.VarlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#id_list.
    def visitId_list(self, ctx:MPParser.Id_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcdec.
    def visitFuncdec(self, ctx:MPParser.FuncdecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procdec.
    def visitProcdec(self, ctx:MPParser.ProcdecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paradec.
    def visitParadec(self, ctx:MPParser.ParadecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#body.
    def visitBody(self, ctx:MPParser.BodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#statement.
    def visitStatement(self, ctx:MPParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#assign_statement.
    def visitAssign_statement(self, ctx:MPParser.Assign_statementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#lhs.
    def visitLhs(self, ctx:MPParser.LhsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#if_s.
    def visitIf_s(self, ctx:MPParser.If_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#while_s.
    def visitWhile_s(self, ctx:MPParser.While_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#for_s.
    def visitFor_s(self, ctx:MPParser.For_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#with_s.
    def visitWith_s(self, ctx:MPParser.With_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcall.
    def visitFuncall(self, ctx:MPParser.FuncallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exprcall.
    def visitExprcall(self, ctx:MPParser.ExprcallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#func_list.
    def visitFunc_list(self, ctx:MPParser.Func_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#br_s.
    def visitBr_s(self, ctx:MPParser.Br_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#cont_s.
    def visitCont_s(self, ctx:MPParser.Cont_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#rt_s.
    def visitRt_s(self, ctx:MPParser.Rt_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr.
    def visitExpr(self, ctx:MPParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr0.
    def visitExpr0(self, ctx:MPParser.Expr0Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr1.
    def visitExpr1(self, ctx:MPParser.Expr1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr2.
    def visitExpr2(self, ctx:MPParser.Expr2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr3.
    def visitExpr3(self, ctx:MPParser.Expr3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr4.
    def visitExpr4(self, ctx:MPParser.Expr4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr5.
    def visitExpr5(self, ctx:MPParser.Expr5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#literal.
    def visitLiteral(self, ctx:MPParser.LiteralContext):
        return self.visitChildren(ctx)



del MPParser