# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2=")
        buf.write("\u024d\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6")
        buf.write("\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r")
        buf.write("\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22")
        buf.write("\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30")
        buf.write("\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\34\5\34")
        buf.write("\u00e7\n\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3")
        buf.write("\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3!\3!\3!\3!\3!\3!")
        buf.write("\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3")
        buf.write("$\3$\3$\3$\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3")
        buf.write("(\3(\3(\3(\3(\3(\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3+\3+\3")
        buf.write("+\3+\3+\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3.\3.\3.\3.\3.\3")
        buf.write(".\3.\3.\3.\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3\60\3\60\3\60")
        buf.write("\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3\63")
        buf.write("\3\63\3\63\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67")
        buf.write("\3\67\38\38\38\38\39\39\39\39\3:\3:\3:\3;\3;\3;\3;\3<")
        buf.write("\3<\3<\3=\3=\3>\3>\3?\3?\3@\3@\3@\3A\3A\3A\3B\3B\3B\3")
        buf.write("B\3C\3C\3C\3D\3D\3E\3E\3F\3F\3G\3G\3H\3H\3I\3I\3J\3J\3")
        buf.write("K\3K\3L\3L\3L\3M\3M\3N\6N\u01b1\nN\rN\16N\u01b2\3N\3N")
        buf.write("\7N\u01b7\nN\fN\16N\u01ba\13N\3N\3N\5N\u01be\nN\3N\6N")
        buf.write("\u01c1\nN\rN\16N\u01c2\3N\3N\7N\u01c7\nN\fN\16N\u01ca")
        buf.write("\13N\3N\3N\5N\u01ce\nN\3N\6N\u01d1\nN\rN\16N\u01d2\5N")
        buf.write("\u01d5\nN\3N\3N\6N\u01d9\nN\rN\16N\u01da\3N\3N\5N\u01df")
        buf.write("\nN\3N\6N\u01e2\nN\rN\16N\u01e3\5N\u01e6\nN\5N\u01e8\n")
        buf.write("N\3O\6O\u01eb\nO\rO\16O\u01ec\3P\3P\3P\3P\3P\3P\3P\3P")
        buf.write("\3P\3P\3P\5P\u01fa\nP\3Q\3Q\7Q\u01fe\nQ\fQ\16Q\u0201\13")
        buf.write("Q\3Q\3Q\3Q\3R\6R\u0207\nR\rR\16R\u0208\3R\7R\u020c\nR")
        buf.write("\fR\16R\u020f\13R\3S\6S\u0212\nS\rS\16S\u0213\3S\3S\3")
        buf.write("T\3T\3T\7T\u021b\nT\fT\16T\u021e\13T\3T\3T\3T\3T\3T\3")
        buf.write("T\7T\u0226\nT\fT\16T\u0229\13T\3T\3T\7T\u022d\nT\fT\16")
        buf.write("T\u0230\13T\3T\5T\u0233\nT\3T\3T\3U\3U\7U\u0239\nU\fU")
        buf.write("\16U\u023c\13U\3U\3U\3U\3U\3V\3V\7V\u0244\nV\fV\16V\u0247")
        buf.write("\13V\3V\3V\3W\3W\3W\2\2X\3\2\5\2\7\2\t\2\13\2\r\2\17\2")
        buf.write("\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'\2")
        buf.write(")\2+\2-\2/\2\61\2\63\2\65\2\67\29\3;\4=\5?\6A\7C\bE\t")
        buf.write("G\nI\13K\fM\rO\16Q\17S\20U\21W\22Y\23[\24]\25_\26a\27")
        buf.write("c\30e\31g\32i\33k\34m\35o\36q\37s u!w\"y#{$}%\177&\u0081")
        buf.write("\'\u0083(\u0085)\u0087*\u0089+\u008b,\u008d-\u008f.\u0091")
        buf.write("/\u0093\60\u0095\61\u0097\62\u0099\63\u009b\64\u009d\65")
        buf.write("\u009f\66\u00a1\67\u00a38\u00a59\u00a7:\u00a9;\u00ab<")
        buf.write("\u00ad=\3\2$\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4")
        buf.write("\2HHhh\4\2IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNn")
        buf.write("n\4\2OOoo\4\2PPpp\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2")
        buf.write("UUuu\4\2VVvv\4\2WWww\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4")
        buf.write("\2\\\\||\n\2$$))^^ddhhppttvv\7\2\n\f\16\17$$))^^\3\2\62")
        buf.write(";\3\2//\5\2C\\aac|\6\2\62;C\\aac|\5\2\13\f\17\17\"\"\4")
        buf.write("\2\f\f\17\17\2\u024d\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2")
        buf.write("\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2")
        buf.write("\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2")
        buf.write("\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3")
        buf.write("\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e")
        buf.write("\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2")
        buf.write("o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2")
        buf.write("\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081")
        buf.write("\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2")
        buf.write("\2\2\u0089\3\2\2\2\2\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f")
        buf.write("\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2")
        buf.write("\2\2\u0097\3\2\2\2\2\u0099\3\2\2\2\2\u009b\3\2\2\2\2\u009d")
        buf.write("\3\2\2\2\2\u009f\3\2\2\2\2\u00a1\3\2\2\2\2\u00a3\3\2\2")
        buf.write("\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2\2\2\u00ab")
        buf.write("\3\2\2\2\2\u00ad\3\2\2\2\3\u00af\3\2\2\2\5\u00b1\3\2\2")
        buf.write("\2\7\u00b3\3\2\2\2\t\u00b5\3\2\2\2\13\u00b7\3\2\2\2\r")
        buf.write("\u00b9\3\2\2\2\17\u00bb\3\2\2\2\21\u00bd\3\2\2\2\23\u00bf")
        buf.write("\3\2\2\2\25\u00c1\3\2\2\2\27\u00c3\3\2\2\2\31\u00c5\3")
        buf.write("\2\2\2\33\u00c7\3\2\2\2\35\u00c9\3\2\2\2\37\u00cb\3\2")
        buf.write("\2\2!\u00cd\3\2\2\2#\u00cf\3\2\2\2%\u00d1\3\2\2\2\'\u00d3")
        buf.write("\3\2\2\2)\u00d5\3\2\2\2+\u00d7\3\2\2\2-\u00d9\3\2\2\2")
        buf.write("/\u00db\3\2\2\2\61\u00dd\3\2\2\2\63\u00df\3\2\2\2\65\u00e1")
        buf.write("\3\2\2\2\67\u00e6\3\2\2\29\u00e8\3\2\2\2;\u00f0\3\2\2")
        buf.write("\2=\u00f8\3\2\2\2?\u00ff\3\2\2\2A\u0104\3\2\2\2C\u010a")
        buf.write("\3\2\2\2E\u0113\3\2\2\2G\u0118\3\2\2\2I\u011c\3\2\2\2")
        buf.write("K\u011f\3\2\2\2M\u0126\3\2\2\2O\u0129\3\2\2\2Q\u012f\3")
        buf.write("\2\2\2S\u0132\3\2\2\2U\u0139\3\2\2\2W\u013e\3\2\2\2Y\u0144")
        buf.write("\3\2\2\2[\u0148\3\2\2\2]\u0151\3\2\2\2_\u015b\3\2\2\2")
        buf.write("a\u015f\3\2\2\2c\u0165\3\2\2\2e\u0168\3\2\2\2g\u016d\3")
        buf.write("\2\2\2i\u016f\3\2\2\2k\u0171\3\2\2\2m\u0173\3\2\2\2o\u0175")
        buf.write("\3\2\2\2q\u0179\3\2\2\2s\u017d\3\2\2\2u\u0180\3\2\2\2")
        buf.write("w\u0184\3\2\2\2y\u0187\3\2\2\2{\u0189\3\2\2\2}\u018b\3")
        buf.write("\2\2\2\177\u018d\3\2\2\2\u0081\u0190\3\2\2\2\u0083\u0193")
        buf.write("\3\2\2\2\u0085\u0197\3\2\2\2\u0087\u019a\3\2\2\2\u0089")
        buf.write("\u019c\3\2\2\2\u008b\u019e\3\2\2\2\u008d\u01a0\3\2\2\2")
        buf.write("\u008f\u01a2\3\2\2\2\u0091\u01a4\3\2\2\2\u0093\u01a6\3")
        buf.write("\2\2\2\u0095\u01a8\3\2\2\2\u0097\u01aa\3\2\2\2\u0099\u01ad")
        buf.write("\3\2\2\2\u009b\u01e7\3\2\2\2\u009d\u01ea\3\2\2\2\u009f")
        buf.write("\u01f9\3\2\2\2\u00a1\u01fb\3\2\2\2\u00a3\u0206\3\2\2\2")
        buf.write("\u00a5\u0211\3\2\2\2\u00a7\u0232\3\2\2\2\u00a9\u0236\3")
        buf.write("\2\2\2\u00ab\u0241\3\2\2\2\u00ad\u024a\3\2\2\2\u00af\u00b0")
        buf.write("\t\2\2\2\u00b0\4\3\2\2\2\u00b1\u00b2\t\3\2\2\u00b2\6\3")
        buf.write("\2\2\2\u00b3\u00b4\t\4\2\2\u00b4\b\3\2\2\2\u00b5\u00b6")
        buf.write("\t\5\2\2\u00b6\n\3\2\2\2\u00b7\u00b8\t\6\2\2\u00b8\f\3")
        buf.write("\2\2\2\u00b9\u00ba\t\7\2\2\u00ba\16\3\2\2\2\u00bb\u00bc")
        buf.write("\t\b\2\2\u00bc\20\3\2\2\2\u00bd\u00be\t\t\2\2\u00be\22")
        buf.write("\3\2\2\2\u00bf\u00c0\t\n\2\2\u00c0\24\3\2\2\2\u00c1\u00c2")
        buf.write("\t\13\2\2\u00c2\26\3\2\2\2\u00c3\u00c4\t\f\2\2\u00c4\30")
        buf.write("\3\2\2\2\u00c5\u00c6\t\r\2\2\u00c6\32\3\2\2\2\u00c7\u00c8")
        buf.write("\t\16\2\2\u00c8\34\3\2\2\2\u00c9\u00ca\t\17\2\2\u00ca")
        buf.write("\36\3\2\2\2\u00cb\u00cc\t\20\2\2\u00cc \3\2\2\2\u00cd")
        buf.write("\u00ce\t\21\2\2\u00ce\"\3\2\2\2\u00cf\u00d0\t\22\2\2\u00d0")
        buf.write("$\3\2\2\2\u00d1\u00d2\t\23\2\2\u00d2&\3\2\2\2\u00d3\u00d4")
        buf.write("\t\24\2\2\u00d4(\3\2\2\2\u00d5\u00d6\t\25\2\2\u00d6*\3")
        buf.write("\2\2\2\u00d7\u00d8\t\26\2\2\u00d8,\3\2\2\2\u00d9\u00da")
        buf.write("\t\27\2\2\u00da.\3\2\2\2\u00db\u00dc\t\30\2\2\u00dc\60")
        buf.write("\3\2\2\2\u00dd\u00de\t\31\2\2\u00de\62\3\2\2\2\u00df\u00e0")
        buf.write("\t\32\2\2\u00e0\64\3\2\2\2\u00e1\u00e2\t\33\2\2\u00e2")
        buf.write("\66\3\2\2\2\u00e3\u00e4\7^\2\2\u00e4\u00e7\t\34\2\2\u00e5")
        buf.write("\u00e7\n\35\2\2\u00e6\u00e3\3\2\2\2\u00e6\u00e5\3\2\2")
        buf.write("\2\u00e78\3\2\2\2\u00e8\u00e9\5\23\n\2\u00e9\u00ea\5\35")
        buf.write("\17\2\u00ea\u00eb\5)\25\2\u00eb\u00ec\5\13\6\2\u00ec\u00ed")
        buf.write("\5\17\b\2\u00ed\u00ee\5\13\6\2\u00ee\u00ef\5%\23\2\u00ef")
        buf.write(":\3\2\2\2\u00f0\u00f1\5\5\3\2\u00f1\u00f2\5\37\20\2\u00f2")
        buf.write("\u00f3\5\37\20\2\u00f3\u00f4\5\31\r\2\u00f4\u00f5\5\13")
        buf.write("\6\2\u00f5\u00f6\5\3\2\2\u00f6\u00f7\5\35\17\2\u00f7<")
        buf.write("\3\2\2\2\u00f8\u00f9\5\'\24\2\u00f9\u00fa\5)\25\2\u00fa")
        buf.write("\u00fb\5%\23\2\u00fb\u00fc\5\23\n\2\u00fc\u00fd\5\35\17")
        buf.write("\2\u00fd\u00fe\5\17\b\2\u00fe>\3\2\2\2\u00ff\u0100\5%")
        buf.write("\23\2\u0100\u0101\5\13\6\2\u0101\u0102\5\3\2\2\u0102\u0103")
        buf.write("\5\31\r\2\u0103@\3\2\2\2\u0104\u0105\5\5\3\2\u0105\u0106")
        buf.write("\5%\23\2\u0106\u0107\5\13\6\2\u0107\u0108\5\3\2\2\u0108")
        buf.write("\u0109\5\27\f\2\u0109B\3\2\2\2\u010a\u010b\5\7\4\2\u010b")
        buf.write("\u010c\5\37\20\2\u010c\u010d\5\35\17\2\u010d\u010e\5)")
        buf.write("\25\2\u010e\u010f\5\23\n\2\u010f\u0110\5\35\17\2\u0110")
        buf.write("\u0111\5+\26\2\u0111\u0112\5\13\6\2\u0112D\3\2\2\2\u0113")
        buf.write("\u0114\5\13\6\2\u0114\u0115\5\31\r\2\u0115\u0116\5\'\24")
        buf.write("\2\u0116\u0117\5\13\6\2\u0117F\3\2\2\2\u0118\u0119\5\r")
        buf.write("\7\2\u0119\u011a\5\37\20\2\u011a\u011b\5%\23\2\u011bH")
        buf.write("\3\2\2\2\u011c\u011d\5\23\n\2\u011d\u011e\5\r\7\2\u011e")
        buf.write("J\3\2\2\2\u011f\u0120\5%\23\2\u0120\u0121\5\13\6\2\u0121")
        buf.write("\u0122\5)\25\2\u0122\u0123\5+\26\2\u0123\u0124\5%\23\2")
        buf.write("\u0124\u0125\5\35\17\2\u0125L\3\2\2\2\u0126\u0127\5\t")
        buf.write("\5\2\u0127\u0128\5\37\20\2\u0128N\3\2\2\2\u0129\u012a")
        buf.write("\5/\30\2\u012a\u012b\5\21\t\2\u012b\u012c\5\23\n\2\u012c")
        buf.write("\u012d\5\31\r\2\u012d\u012e\5\13\6\2\u012eP\3\2\2\2\u012f")
        buf.write("\u0130\5)\25\2\u0130\u0131\5\37\20\2\u0131R\3\2\2\2\u0132")
        buf.write("\u0133\5\t\5\2\u0133\u0134\5\37\20\2\u0134\u0135\5/\30")
        buf.write("\2\u0135\u0136\5\35\17\2\u0136\u0137\5)\25\2\u0137\u0138")
        buf.write("\5\37\20\2\u0138T\3\2\2\2\u0139\u013a\5)\25\2\u013a\u013b")
        buf.write("\5\21\t\2\u013b\u013c\5\13\6\2\u013c\u013d\5\35\17\2\u013d")
        buf.write("V\3\2\2\2\u013e\u013f\5\5\3\2\u013f\u0140\5\13\6\2\u0140")
        buf.write("\u0141\5\17\b\2\u0141\u0142\5\23\n\2\u0142\u0143\5\35")
        buf.write("\17\2\u0143X\3\2\2\2\u0144\u0145\5\13\6\2\u0145\u0146")
        buf.write("\5\35\17\2\u0146\u0147\5\t\5\2\u0147Z\3\2\2\2\u0148\u0149")
        buf.write("\5\r\7\2\u0149\u014a\5+\26\2\u014a\u014b\5\35\17\2\u014b")
        buf.write("\u014c\5\7\4\2\u014c\u014d\5)\25\2\u014d\u014e\5\23\n")
        buf.write("\2\u014e\u014f\5\37\20\2\u014f\u0150\5\35\17\2\u0150\\")
        buf.write("\3\2\2\2\u0151\u0152\5!\21\2\u0152\u0153\5%\23\2\u0153")
        buf.write("\u0154\5\37\20\2\u0154\u0155\5\7\4\2\u0155\u0156\5\13")
        buf.write("\6\2\u0156\u0157\5\t\5\2\u0157\u0158\5+\26\2\u0158\u0159")
        buf.write("\5%\23\2\u0159\u015a\5\13\6\2\u015a^\3\2\2\2\u015b\u015c")
        buf.write("\5-\27\2\u015c\u015d\5\3\2\2\u015d\u015e\5%\23\2\u015e")
        buf.write("`\3\2\2\2\u015f\u0160\5\3\2\2\u0160\u0161\5%\23\2\u0161")
        buf.write("\u0162\5%\23\2\u0162\u0163\5\3\2\2\u0163\u0164\5\63\32")
        buf.write("\2\u0164b\3\2\2\2\u0165\u0166\5\37\20\2\u0166\u0167\5")
        buf.write("\r\7\2\u0167d\3\2\2\2\u0168\u0169\5/\30\2\u0169\u016a")
        buf.write("\5\23\n\2\u016a\u016b\5)\25\2\u016b\u016c\5\21\t\2\u016c")
        buf.write("f\3\2\2\2\u016d\u016e\7-\2\2\u016eh\3\2\2\2\u016f\u0170")
        buf.write("\7/\2\2\u0170j\3\2\2\2\u0171\u0172\7,\2\2\u0172l\3\2\2")
        buf.write("\2\u0173\u0174\7\61\2\2\u0174n\3\2\2\2\u0175\u0176\5\35")
        buf.write("\17\2\u0176\u0177\5\37\20\2\u0177\u0178\5)\25\2\u0178")
        buf.write("p\3\2\2\2\u0179\u017a\5\33\16\2\u017a\u017b\5\37\20\2")
        buf.write("\u017b\u017c\5\t\5\2\u017cr\3\2\2\2\u017d\u017e\5\37\20")
        buf.write("\2\u017e\u017f\5%\23\2\u017ft\3\2\2\2\u0180\u0181\5\3")
        buf.write("\2\2\u0181\u0182\5\35\17\2\u0182\u0183\5\t\5\2\u0183v")
        buf.write("\3\2\2\2\u0184\u0185\7>\2\2\u0185\u0186\7@\2\2\u0186x")
        buf.write("\3\2\2\2\u0187\u0188\7?\2\2\u0188z\3\2\2\2\u0189\u018a")
        buf.write("\7>\2\2\u018a|\3\2\2\2\u018b\u018c\7@\2\2\u018c~\3\2\2")
        buf.write("\2\u018d\u018e\7>\2\2\u018e\u018f\7?\2\2\u018f\u0080\3")
        buf.write("\2\2\2\u0190\u0191\7@\2\2\u0191\u0192\7?\2\2\u0192\u0082")
        buf.write("\3\2\2\2\u0193\u0194\5\t\5\2\u0194\u0195\5\23\n\2\u0195")
        buf.write("\u0196\5-\27\2\u0196\u0084\3\2\2\2\u0197\u0198\7<\2\2")
        buf.write("\u0198\u0199\7?\2\2\u0199\u0086\3\2\2\2\u019a\u019b\7")
        buf.write("]\2\2\u019b\u0088\3\2\2\2\u019c\u019d\7_\2\2\u019d\u008a")
        buf.write("\3\2\2\2\u019e\u019f\7*\2\2\u019f\u008c\3\2\2\2\u01a0")
        buf.write("\u01a1\7+\2\2\u01a1\u008e\3\2\2\2\u01a2\u01a3\7}\2\2\u01a3")
        buf.write("\u0090\3\2\2\2\u01a4\u01a5\7\177\2\2\u01a5\u0092\3\2\2")
        buf.write("\2\u01a6\u01a7\7=\2\2\u01a7\u0094\3\2\2\2\u01a8\u01a9")
        buf.write("\7.\2\2\u01a9\u0096\3\2\2\2\u01aa\u01ab\7\60\2\2\u01ab")
        buf.write("\u01ac\7\60\2\2\u01ac\u0098\3\2\2\2\u01ad\u01ae\7<\2\2")
        buf.write("\u01ae\u009a\3\2\2\2\u01af\u01b1\t\36\2\2\u01b0\u01af")
        buf.write("\3\2\2\2\u01b1\u01b2\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2")
        buf.write("\u01b3\3\2\2\2\u01b3\u01d4\3\2\2\2\u01b4\u01b8\7\60\2")
        buf.write("\2\u01b5\u01b7\t\36\2\2\u01b6\u01b5\3\2\2\2\u01b7\u01ba")
        buf.write("\3\2\2\2\u01b8\u01b6\3\2\2\2\u01b8\u01b9\3\2\2\2\u01b9")
        buf.write("\u01bb\3\2\2\2\u01ba\u01b8\3\2\2\2\u01bb\u01bd\t\6\2\2")
        buf.write("\u01bc\u01be\t\37\2\2\u01bd\u01bc\3\2\2\2\u01bd\u01be")
        buf.write("\3\2\2\2\u01be\u01c0\3\2\2\2\u01bf\u01c1\t\36\2\2\u01c0")
        buf.write("\u01bf\3\2\2\2\u01c1\u01c2\3\2\2\2\u01c2\u01c0\3\2\2\2")
        buf.write("\u01c2\u01c3\3\2\2\2\u01c3\u01d5\3\2\2\2\u01c4\u01c8\7")
        buf.write("\60\2\2\u01c5\u01c7\t\36\2\2\u01c6\u01c5\3\2\2\2\u01c7")
        buf.write("\u01ca\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c8\u01c9\3\2\2\2")
        buf.write("\u01c9\u01d5\3\2\2\2\u01ca\u01c8\3\2\2\2\u01cb\u01cd\t")
        buf.write("\6\2\2\u01cc\u01ce\t\37\2\2\u01cd\u01cc\3\2\2\2\u01cd")
        buf.write("\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf\u01d1\t\36\2")
        buf.write("\2\u01d0\u01cf\3\2\2\2\u01d1\u01d2\3\2\2\2\u01d2\u01d0")
        buf.write("\3\2\2\2\u01d2\u01d3\3\2\2\2\u01d3\u01d5\3\2\2\2\u01d4")
        buf.write("\u01b4\3\2\2\2\u01d4\u01c4\3\2\2\2\u01d4\u01cb\3\2\2\2")
        buf.write("\u01d5\u01e8\3\2\2\2\u01d6\u01d8\7\60\2\2\u01d7\u01d9")
        buf.write("\t\36\2\2\u01d8\u01d7\3\2\2\2\u01d9\u01da\3\2\2\2\u01da")
        buf.write("\u01d8\3\2\2\2\u01da\u01db\3\2\2\2\u01db\u01e5\3\2\2\2")
        buf.write("\u01dc\u01de\t\6\2\2\u01dd\u01df\t\37\2\2\u01de\u01dd")
        buf.write("\3\2\2\2\u01de\u01df\3\2\2\2\u01df\u01e1\3\2\2\2\u01e0")
        buf.write("\u01e2\t\36\2\2\u01e1\u01e0\3\2\2\2\u01e2\u01e3\3\2\2")
        buf.write("\2\u01e3\u01e1\3\2\2\2\u01e3\u01e4\3\2\2\2\u01e4\u01e6")
        buf.write("\3\2\2\2\u01e5\u01dc\3\2\2\2\u01e5\u01e6\3\2\2\2\u01e6")
        buf.write("\u01e8\3\2\2\2\u01e7\u01b0\3\2\2\2\u01e7\u01d6\3\2\2\2")
        buf.write("\u01e8\u009c\3\2\2\2\u01e9\u01eb\t\36\2\2\u01ea\u01e9")
        buf.write("\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u01ea\3\2\2\2\u01ec")
        buf.write("\u01ed\3\2\2\2\u01ed\u009e\3\2\2\2\u01ee\u01ef\5)\25\2")
        buf.write("\u01ef\u01f0\5%\23\2\u01f0\u01f1\5+\26\2\u01f1\u01f2\5")
        buf.write("\13\6\2\u01f2\u01fa\3\2\2\2\u01f3\u01f4\5\r\7\2\u01f4")
        buf.write("\u01f5\5\3\2\2\u01f5\u01f6\5\31\r\2\u01f6\u01f7\5\'\24")
        buf.write("\2\u01f7\u01f8\5\13\6\2\u01f8\u01fa\3\2\2\2\u01f9\u01ee")
        buf.write("\3\2\2\2\u01f9\u01f3\3\2\2\2\u01fa\u00a0\3\2\2\2\u01fb")
        buf.write("\u01ff\7$\2\2\u01fc\u01fe\5\67\34\2\u01fd\u01fc\3\2\2")
        buf.write("\2\u01fe\u0201\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff\u0200")
        buf.write("\3\2\2\2\u0200\u0202\3\2\2\2\u0201\u01ff\3\2\2\2\u0202")
        buf.write("\u0203\7$\2\2\u0203\u0204\bQ\2\2\u0204\u00a2\3\2\2\2\u0205")
        buf.write("\u0207\t \2\2\u0206\u0205\3\2\2\2\u0207\u0208\3\2\2\2")
        buf.write("\u0208\u0206\3\2\2\2\u0208\u0209\3\2\2\2\u0209\u020d\3")
        buf.write("\2\2\2\u020a\u020c\t!\2\2\u020b\u020a\3\2\2\2\u020c\u020f")
        buf.write("\3\2\2\2\u020d\u020b\3\2\2\2\u020d\u020e\3\2\2\2\u020e")
        buf.write("\u00a4\3\2\2\2\u020f\u020d\3\2\2\2\u0210\u0212\t\"\2\2")
        buf.write("\u0211\u0210\3\2\2\2\u0212\u0213\3\2\2\2\u0213\u0211\3")
        buf.write("\2\2\2\u0213\u0214\3\2\2\2\u0214\u0215\3\2\2\2\u0215\u0216")
        buf.write("\bS\3\2\u0216\u00a6\3\2\2\2\u0217\u0218\7*\2\2\u0218\u021c")
        buf.write("\7,\2\2\u0219\u021b\13\2\2\2\u021a\u0219\3\2\2\2\u021b")
        buf.write("\u021e\3\2\2\2\u021c\u021a\3\2\2\2\u021c\u021d\3\2\2\2")
        buf.write("\u021d\u021f\3\2\2\2\u021e\u021c\3\2\2\2\u021f\u0220\7")
        buf.write(",\2\2\u0220\u0233\7+\2\2\u0221\u0222\7\61\2\2\u0222\u0223")
        buf.write("\7\61\2\2\u0223\u0227\3\2\2\2\u0224\u0226\n#\2\2\u0225")
        buf.write("\u0224\3\2\2\2\u0226\u0229\3\2\2\2\u0227\u0225\3\2\2\2")
        buf.write("\u0227\u0228\3\2\2\2\u0228\u0233\3\2\2\2\u0229\u0227\3")
        buf.write("\2\2\2\u022a\u022e\7}\2\2\u022b\u022d\13\2\2\2\u022c\u022b")
        buf.write("\3\2\2\2\u022d\u0230\3\2\2\2\u022e\u022c\3\2\2\2\u022e")
        buf.write("\u022f\3\2\2\2\u022f\u0231\3\2\2\2\u0230\u022e\3\2\2\2")
        buf.write("\u0231\u0233\7\177\2\2\u0232\u0217\3\2\2\2\u0232\u0221")
        buf.write("\3\2\2\2\u0232\u022a\3\2\2\2\u0233\u0234\3\2\2\2\u0234")
        buf.write("\u0235\bT\3\2\u0235\u00a8\3\2\2\2\u0236\u023a\7$\2\2\u0237")
        buf.write("\u0239\5\67\34\2\u0238\u0237\3\2\2\2\u0239\u023c\3\2\2")
        buf.write("\2\u023a\u0238\3\2\2\2\u023a\u023b\3\2\2\2\u023b\u023d")
        buf.write("\3\2\2\2\u023c\u023a\3\2\2\2\u023d\u023e\7^\2\2\u023e")
        buf.write("\u023f\n\34\2\2\u023f\u0240\bU\4\2\u0240\u00aa\3\2\2\2")
        buf.write("\u0241\u0245\7$\2\2\u0242\u0244\5\67\34\2\u0243\u0242")
        buf.write("\3\2\2\2\u0244\u0247\3\2\2\2\u0245\u0243\3\2\2\2\u0245")
        buf.write("\u0246\3\2\2\2\u0246\u0248\3\2\2\2\u0247\u0245\3\2\2\2")
        buf.write("\u0248\u0249\bV\5\2\u0249\u00ac\3\2\2\2\u024a\u024b\13")
        buf.write("\2\2\2\u024b\u024c\bW\6\2\u024c\u00ae\3\2\2\2\35\2\u00e6")
        buf.write("\u01b2\u01b8\u01bd\u01c2\u01c8\u01cd\u01d2\u01d4\u01da")
        buf.write("\u01de\u01e3\u01e5\u01e7\u01ec\u01f9\u01ff\u0208\u020d")
        buf.write("\u0213\u021c\u0227\u022e\u0232\u023a\u0245\7\3Q\2\b\2")
        buf.write("\2\3U\3\3V\4\3W\5")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INTTYPE = 1
    BOOLEANTYPE = 2
    STRINGTYPE = 3
    FLOATTYPE = 4
    BREAK = 5
    CONTINUE = 6
    ELSE = 7
    FOR = 8
    IF = 9
    RETURN = 10
    DO = 11
    WHILE = 12
    TO = 13
    DOWNTO = 14
    THEN = 15
    BEGIN = 16
    END = 17
    FUNCTION = 18
    PROCEDURE = 19
    VAR = 20
    ARRAY = 21
    OF = 22
    WITH = 23
    OP1 = 24
    OP2 = 25
    OP3 = 26
    OP4 = 27
    OP5 = 28
    OP6 = 29
    OP7 = 30
    OP8 = 31
    OP9 = 32
    OP10 = 33
    OP11 = 34
    OP12 = 35
    OP13 = 36
    OP14 = 37
    OP15 = 38
    OP16 = 39
    LSB = 40
    RSB = 41
    LB = 42
    RB = 43
    LP = 44
    RP = 45
    SEMI = 46
    COMMA = 47
    DOUBLEDOT = 48
    COLON = 49
    FLOATLIT = 50
    INTLIT = 51
    BOOLLIT = 52
    STRINGLIT = 53
    ID = 54
    WS = 55
    COMMENT = 56
    ILLEGAL_ESCAPE = 57
    UNCLOSE_STRING = 58
    ERROR_CHAR = 59

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'+'", "'-'", "'*'", "'/'", "'<>'", "'='", "'<'", "'>'", "'<='", 
            "'>='", "':='", "'['", "']'", "'('", "')'", "'{'", "'}'", "';'", 
            "','", "'..'", "':'" ]

    symbolicNames = [ "<INVALID>",
            "INTTYPE", "BOOLEANTYPE", "STRINGTYPE", "FLOATTYPE", "BREAK", 
            "CONTINUE", "ELSE", "FOR", "IF", "RETURN", "DO", "WHILE", "TO", 
            "DOWNTO", "THEN", "BEGIN", "END", "FUNCTION", "PROCEDURE", "VAR", 
            "ARRAY", "OF", "WITH", "OP1", "OP2", "OP3", "OP4", "OP5", "OP6", 
            "OP7", "OP8", "OP9", "OP10", "OP11", "OP12", "OP13", "OP14", 
            "OP15", "OP16", "LSB", "RSB", "LB", "RB", "LP", "RP", "SEMI", 
            "COMMA", "DOUBLEDOT", "COLON", "FLOATLIT", "INTLIT", "BOOLLIT", 
            "STRINGLIT", "ID", "WS", "COMMENT", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", 
            "ERROR_CHAR" ]

    ruleNames = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", 
                  "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", 
                  "W", "X", "Y", "Z", "Char", "INTTYPE", "BOOLEANTYPE", 
                  "STRINGTYPE", "FLOATTYPE", "BREAK", "CONTINUE", "ELSE", 
                  "FOR", "IF", "RETURN", "DO", "WHILE", "TO", "DOWNTO", 
                  "THEN", "BEGIN", "END", "FUNCTION", "PROCEDURE", "VAR", 
                  "ARRAY", "OF", "WITH", "OP1", "OP2", "OP3", "OP4", "OP5", 
                  "OP6", "OP7", "OP8", "OP9", "OP10", "OP11", "OP12", "OP13", 
                  "OP14", "OP15", "OP16", "LSB", "RSB", "LB", "RB", "LP", 
                  "RP", "SEMI", "COMMA", "DOUBLEDOT", "COLON", "FLOATLIT", 
                  "INTLIT", "BOOLLIT", "STRINGLIT", "ID", "WS", "COMMENT", 
                  "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "ERROR_CHAR" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[79] = self.STRINGLIT_action 
            actions[83] = self.ILLEGAL_ESCAPE_action 
            actions[84] = self.UNCLOSE_STRING_action 
            actions[85] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            self.text =  self.text[1:-1]
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:
             raise IllegalEscape(self.text[1:]) 
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:
             raise UncloseString(self.text[1:]) 
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
             raise ErrorToken(self.text) 
     


