'''
 *   @author Nguyen Hua Phung
 *   @version 1.0
 *   23/10/2015
 *   This file provides a simple version of code generator
 *
'''
from Utils import *
from StaticCheck import *
from StaticError import *
from Emitter import Emitter
from Frame import Frame
from abc import ABC, abstractmethod

class CodeGenerator(Utils):
    
    def __init__(self):
        self.libName = "io"

    def init(self):
        return [Symbol("getInt", MType(list(), IntType()), CName(self.libName)),
                Symbol("putInt", MType([IntType()], VoidType()), CName(self.libName)),
                Symbol("putIntLn", MType([IntType()], VoidType()), CName(self.libName)),
                Symbol("getFloat", MType(list(), FloatType()), CName(self.libName)),
                Symbol("putFloat", MType([FloatType()], VoidType()), CName(self.libName)),
                Symbol("putFloatLn", MType([FloatType()], VoidType()), CName(self.libName)),
                Symbol("putBool", MType([BoolType()], VoidType()), CName(self.libName)),
                Symbol("putBoolLn", MType([BoolType()], VoidType()), CName(self.libName)),
                Symbol("putString", MType([StringType()], VoidType()), CName(self.libName)),
                Symbol("putStringLn", MType([StringType()], VoidType()), CName(self.libName)),
                Symbol("putLn", MType(list(), VoidType()), CName(self.libName))]

    def gen(self, ast, dir_):
        #ast: AST
        #dir_: String

        gl = self.init()
        gc = CodeGenVisitor(ast, gl, dir_)
        gc.visit(ast, None)

# class StringType(Type):
    
#     def __str__(self):
#         return "StringType"

#     def accept(self, v, param):
#         return None

class ArrayPointerType(Type):
    def __init__(self, ctype):
        #cname: String
        self.eleType = ctype

    def __str__(self):
        return "ArrayPointerType({0})".format(str(self.eleType))

    def accept(self, v, param):
        return None

class ClassType(Type):
    def __init__(self,cname):
        self.cname = cname
    def __str__(self):
        return "Class({0})".format(str(self.cname))
    def accept(self, v, param):
        return None
        
class SubBody():
    def __init__(self, frame, sym):
        #frame: Frame
        #sym: List[Symbol]

        self.frame = frame
        self.sym = sym

class Access():
    def __init__(self, frame, sym, isLeft, isFirst):
        #frame: Frame
        #sym: List[Symbol]
        #isLeft: Boolean
        #isFirst: Boolean

        self.frame = frame
        self.sym = sym
        self.isLeft = isLeft
        self.isFirst = isFirst

class Val(ABC):
    pass

class Index(Val):
    def __init__(self, value):
        #value: Int

        self.value = value

class CName(Val):
    def __init__(self, value):
        #value: String

        self.value = value

class CodeGenVisitor(BaseVisitor, Utils):
    

    def __init__(self, astTree, env, dir_):
        #astTree: AST
        #env: List[Symbol]
        #dir_: File

        self.astTree = astTree
        self.env = env
        self.className = "MPClass"
        self.path = dir_
        self.emit = Emitter(self.path + "/" + self.className + ".j")

        check_type = 0
        check_program = 0
        
    def visitProgram(self, ast, o):
        #ast: Program
        #o: Any

        CodeGenVisitor.check_type = 0
        CodeGenVisitor.check_program = 0
        self.emit.printout(self.emit.emitPROLOG(self.className, "java.lang.Object"))

        e = SubBody(None, self.env)

        for x in ast.decl:
            if type(x) is VarDecl:
                temp = self.visit(x, e)

        for x in ast.decl:
            if type(x) is FuncDecl:
                temp = self.visit(x, e)

        CodeGenVisitor.check_program = 1

        for x in ast.decl:
            if type(x) is FuncDecl:
                temp = self.visit(x, e)

        # generate default constructor
        self.genMETHOD(FuncDecl(Id("<init>"), list(), list(), list(),None), o, Frame("<init>", VoidType))
        self.emit.emitEPILOG()
        return o

    def visitVarDecl(self,ast,o):
        if CodeGenVisitor.check_program == 0:
            subctxt = o
            frame = Frame(ast.variable,ast.varType)
            subctxt.sym = [Symbol(ast.variable.name, ast.varType, CName(self.className))] + subctxt.sym
            self.emit.printout(self.emit.emitATTRIBUTE(ast.variable.name,ast.varType,False,None))
            return SubBody(frame, subctxt.sym)

    def visitFuncDecl(self, ast, o):
        #ast: FuncDecl
        #o: Any
        if CodeGenVisitor.check_program == 0:
            subctxt = o
            frame = Frame(ast.name, ast.returnType)
            if ast.name.name != "main":
                subctxt.sym = [Symbol(ast.name.name, MType([x.varType for x in ast.param], ast.returnType), CName(self.className))] + subctxt.sym
                self.genMETHOD(ast, subctxt.sym, frame)
                return SubBody(frame, subctxt.sym)
        else: 
            subctxt = o
            frame = Frame(ast.name, ast.returnType)
            if ast.name.name == "main":
                subctxt.sym = [Symbol(ast.name.name, MType([x.varType for x in ast.param], ast.returnType), CName(self.className))] + subctxt.sym
                self.genMETHOD(ast, subctxt.sym, frame)
                return SubBody(frame, subctxt.sym)

    def genMETHOD(self, consdecl, o, frame):
        #consdecl: FuncDecl
        #o: Any
        #frame: Frame

        isInit = consdecl.returnType is None
        isMain = consdecl.name.name == "main" and len(consdecl.param) == 0 and type(consdecl.returnType) is VoidType
        returnType = VoidType() if isInit else consdecl.returnType
        methodName = "<init>" if isInit else consdecl.name.name
        intype = [ArrayPointerType(StringType())] if isMain else list(map(lambda x: x.varType,consdecl.param))
        mtype = MType(intype, returnType)

        self.emit.printout(self.emit.emitMETHOD(methodName, mtype, not isInit, frame))
        frame.enterScope(True)

        # Generate code for parameter declarations
        if isInit:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "this", ClassType(self.className), frame.getStartLabel(), frame.getEndLabel(), frame))
        if isMain:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "args", ArrayPointerType(StringType()), frame.getStartLabel(), frame.getEndLabel(), frame))

        inside = []
        
        for i in consdecl.param:
            idx = frame.getNewIndex()
            self.emit.printout(self.emit.emitVAR(idx, i.variable.name,i.varType, frame.getStartLabel(), frame.getEndLabel(),frame))
            inside.append(Symbol(i.variable.name, i.varType, idx))

        for i in consdecl.local:
            idx = frame.getNewIndex()
            self.emit.printout(self.emit.emitVAR(idx, i.variable.name,i.varType, frame.getStartLabel(), frame.getEndLabel(),frame))
            inside.append(Symbol(i.variable.name, i.varType, idx))

        body = consdecl.body
        self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(), frame))

        # Generate code for statements
        if isInit:
            self.emit.printout(self.emit.emitREADVAR("this", ClassType(self.className), 0, frame))
            self.emit.printout(self.emit.emitINVOKESPECIAL(frame))
        
        temp = list(map(lambda x: self.visit(x, SubBody(frame, inside + o)), body))
     
        self.emit.printout(self.emit.emitLABEL(frame.getEndLabel(), frame))
        
        self.emit.printout(self.emit.emitRETURN(VoidType(), frame))

        self.emit.printout(self.emit.emitENDMETHOD(frame))

        frame.exitScope();

    def visitId(self, ast, o):
        ctxt = o
        frame = ctxt.frame
        env =ctxt.sym
        sym = self.lookup(ast.name, env, lambda x: x.name)

        if o.isLeft:
            return "", sym.mtype
        else:
            if type (sym.value) is CName:
                return self.emit.emitGETSTATIC(self.className+"/" +sym.name,sym.mtype, frame), sym.mtype
            else:
                return self.emit.emitREADVAR(sym.name,sym.mtype,sym.value,frame), sym.mtype

    def visitAssign(self,ast,o):
        ctxt = o
        frame = ctxt.frame
        env = ctxt.sym
        lc,lt = self.visit(ast.lhs,Access(frame,ctxt.sym,True,True))
        rc,rt = self.visit(ast.exp,Access(frame,ctxt.sym,False,True))
       
        if type(ast.lhs) is Id:
            sym = self.lookup(ast.lhs.name, env, lambda x: x.name)
            if type(lt) is type(rt):
                if type(sym.value) is not CName:
                    self.emit.printout(rc + self.emit.emitDUP(frame) + self.emit.emitWRITEVAR(sym.name, sym.mtype, sym.value, frame)+ self.emit.emitPOP(frame))
                else:
                    self.emit.printout(rc + self.emit.emitDUP(frame) + self.emit.emitPUTSTATIC(sym.value.value+"/" +sym.name,sym.mtype, frame)+ self.emit.emitPOP(frame))
            elif (type(lt) is FloatType and type(rt) is IntType):
                if type(sym.value) is not CName:
                    self.emit.printout(rc + self.emit.emitI2F(frame) + self.emit.emitDUP(frame) + self.emit.emitWRITEVAR(sym.name, sym.mtype, sym.value, frame)+  self.emit.emitPOP(frame))
                else: 
                    self.emit.printout(rc + self.emit.emitI2F(frame) + self.emit.emitDUP(frame) + self.emit.emitPUTSTATIC(sym.value.value+"/" +sym.name,sym.mtype, frame)+ self.emit.emitPOP(frame))

    def visitCallStmt(self, ast, o):
        #ast: CallStmt
        #o: Any
        ctxt = o
        frame = ctxt.frame
        env = ctxt.sym

        global_func = self.lookup(ast.method.name, env, lambda x: x.name)
        if global_func is not None:
            CodeGenVisitor.check_type = 0
            for x in global_func.mtype.partype:
                CodeGenVisitor.check_type = x

        sym = self.lookup(ast.method.name, env, lambda x: x.name)

        cname = sym.value.value
    
        ctype = sym.mtype

        typePara = []
        
        for x in ast.param:
            str1, typ1 = self.visit(x, Access(frame, env, False, True))
            typePara.append(typ1)

        compare = list(zip(sym.mtype.partype, typePara))
    
        in_ = ("", list())
        index = 0
        for x in ast.param:
            str1, typ1 = self.visit(x, Access(frame, env, False, True))
            check = [typ1]

            temp = compare[index]
            if type(temp[0]) is FloatType and type(temp[1]) is IntType:
                in_ = (in_[0] + str1 + self.emit.emitI2F(frame), in_[1]+check)
            else:
                in_ = (in_[0] + str1, in_[1]+check)

            index +=1

        self.emit.printout(in_[0])
        self.emit.printout(self.emit.emitINVOKESTATIC(cname + "/" + ast.method.name, ctype, frame))
        
    def visitCallExpr(self, ast, o):
        #ast: CallStmt
        #o: Any
        ctxt = o
        frame = ctxt.frame
        env = ctxt.sym

        global_func = self.lookup(ast.method.name, env, lambda x: x.name)
        if global_func is not None:
            CodeGenVisitor.check_type = 0
            for x in global_func.mtype.partype:
                CodeGenVisitor.check_type = x

        sym = self.lookup(ast.method.name,  env, lambda x: x.name)
    
        cname = sym.value.value
    
        ctype = sym.mtype

        typePara = []
        
        for x in ast.param:
            str1, typ1 = self.visit(x, Access(frame, env, False, True))
            typePara.append(typ1)

        compare = list(zip(sym.mtype.partype, typePara))

        in_ = ("", list())
        index = 0
        for x in ast.param:
            str1, typ1 = self.visit(x, Access(frame, env, False, True))
            check = [typ1]

            temp = compare[index]
            if type(temp[0]) is FloatType and type(temp[1]) is IntType:
                in_ = (in_[0] + str1 + self.emit.emitI2F(frame), in_[1]+check)
            else:
                in_ = (in_[0] + str1, in_[1]+check)

            index +=1

        self.emit.printout(in_[0])
        self.emit.printout(self.emit.emitINVOKESTATIC(cname + "/" + ast.method.name, ctype, frame))
        return (in_[0]) + (self.emit.emitINVOKESTATIC(cname + "/" + ast.method.name, ctype, frame)), ctype.rettype

    def visitIf (self, ast, o):
        subctxt = o
        frame = subctxt.frame
        env = subctxt.sym
        expr1, expr2 = self.visit(ast.expr, Access(frame, env,False, True))
        false_lb = frame.getNewLabel()
        end_lb = frame.getNewLabel()
        branchfalse = self.emit.emitIFEQ(false_lb, frame)
        flb = self.emit.emitLABEL(false_lb, frame)
        goTo = self.emit.emitGOTO(end_lb, frame)

        if ast.elseStmt is not None:
            elb = self.emit.emitLABEL(end_lb, frame)
            self.emit.printout(expr1)
            self.emit.printout(branchfalse)
            then_stmt = list(map(lambda x: self.visit(x, SubBody(frame, env)), ast.thenStmt))
            self.emit.printout(goTo)
            self.emit.printout(flb)
            else_stmt = list(map(lambda x: self.visit(x, SubBody(frame, env)), ast.elseStmt))
            self.emit.printout(elb)
        else:
            self.emit.printout(expr1)
            self.emit.printout(branchfalse)
            then_stmt = list(map(lambda x: self.visit(x, SubBody(frame, env)), ast.thenStmt))
            self.emit.printout(flb)
            
    def visitFor (self, ast, o):
        subctxt = o
        frame = subctxt.frame
        env = subctxt.sym
       
        frame.enterLoop()
        brk_lb=frame.getBreakLabel()
        con_lb=frame.getContinueLabel()
        rep_lb=frame.getNewLabel()

        id_1, id_2 = self.visit(ast.id, Access(frame, env, False, True))
        exp1_1, exp1_2 = self.visit(ast.expr1, Access(frame, env, False, True))
        exp2_1, exp2_2 = self.visit(ast.expr2, Access(frame, env, False, True))
        up = ast.up

        #initial state
        if type(ast.id) is Id:
            sym = self.lookup(ast.id.name, env, lambda x: x.name)
            if id_2 == exp1_2 and type(id_2) is IntType:
                if type(sym.value) is not CName:
                    self.emit.printout(exp1_1 + self.emit.emitDUP(frame) + self.emit.emitWRITEVAR(sym.name, sym.mtype, sym.value, frame))
                else:
                    self.emit.printout(exp1_1 + self.emit.emitDUP(frame) + self.emit.emitPUTSTATIC(sym.value.value+"/" +sym.name,sym.mtype, frame))

        self.emit.printout(self.emit.emitPOP(frame))
        rlb = self.emit.emitLABEL(rep_lb,frame)
        self.emit.printout(rlb)
        if up is True:
            self.emit.printout(id_1 + exp2_1 + self.emit.emitREOP("<=", BoolType(), frame))
        else:
            self.emit.printout(id_1 + exp2_1 + self.emit.emitREOP(">=", BoolType(), frame))

        branch_false = self.emit.emitIFEQ(brk_lb,frame)
        self.emit.printout(branch_false)
        loop_stmt = list(map(lambda x: self.visit(x, SubBody(frame, env)), ast.loop))
        clb = self.emit.emitLABEL(con_lb,frame)
        self.emit.printout(clb)

        if up is True:
            self.emit.printout(id_1 + self.emit.emitPUSHICONST("1", frame) +self.emit.emitADDOP("+",id_2,frame))
        else: 
            self.emit.printout(id_1 + self.emit.emitPUSHICONST("1", frame) +self.emit.emitADDOP("-",id_2,frame))

        sym = self.lookup(ast.id.name, env, lambda x: x.name)
        if type(sym.value) is not CName:
            self.emit.printout(self.emit.emitDUP(frame) + self.emit.emitWRITEVAR(sym.name, sym.mtype, sym.value, frame))
        else:
            self.emit.printout(self.emit.emitDUP(frame) + self.emit.emitPUTSTATIC(sym.value.value+"/" +sym.name,sym.mtype, frame))
        self.emit.printout(self.emit.emitPOP(frame))
        branch_rep= self.emit.emitGOTO(rep_lb,frame)
        self.emit.printout(branch_rep)
        branch_end= self.emit.emitLABEL(brk_lb,frame)
        self.emit.printout(branch_end)
        frame.exitLoop()
        
    def visitWhile(self,ast,o):
        ctxt = o
        frame = o.frame
        env = ctxt.sym

        frame.enterLoop()
        brk_lb = frame.getBreakLabel()
        con_lb = frame.getContinueLabel()
        rep_lb = frame.getNewLabel()
        clb = self.emit.emitLABEL(con_lb,frame)
        rlb = self.emit.emitLABEL(rep_lb,frame)
        self.emit.printout(rlb)
        exp, typ = self.visit(ast.exp, Access(frame, ctxt.sym, False, True))
        self.emit.printout(exp)
        branch_false = self.emit.emitIFEQ(brk_lb,frame)
        self.emit.printout(branch_false)
        sl = list(map(lambda x: self.visit(x, SubBody(frame, env)), ast.sl))
        self.emit.printout(clb)
        branch_rep= self.emit.emitGOTO(rep_lb,frame)
        self.emit.printout(branch_rep)
        branch_end= self.emit.emitLABEL(brk_lb,frame)
        self.emit.printout(branch_end)
        frame.exitLoop()

    def visitWith(self,ast,o):
        ctxt = o
        frame = o.frame
        env = ctxt.sym

        frame.enterScope(False)
        param = list(map(lambda x: Symbol(x.variable.name, x.varType), ast.decl))
        startLb = self.emit.emitLABEL(frame.getStartLabel(),frame)
        self.emit.printout(startLb)

        inside = []
        for i in ast.decl:
            idx = frame.getNewIndex()
            self.emit.printout(self.emit.emitVAR(idx, i.variable.name,i.varType, frame.getStartLabel(), frame.getEndLabel(),frame))
            inside.append(Symbol(i.variable.name, i.varType, idx))
        temp = list(map(lambda x: self.visit(x, SubBody(frame, inside + env)), ast.stmt))
        endLb = self.emit.emitLABEL(frame.getEndLabel(),frame)
        self.emit.printout(endLb)
        frame.exitScope()

    def visitBinaryOp(self, ast, o):
        frame = o.frame

        if ast.op in ['+','-']:
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))

            if type(CodeGenVisitor.check_type) is IntType:
                if type(lt) == type(rt) and type(lt) is IntType:
                    return lc + rc + self.emit.emitADDOP(ast.op, IntType(), frame), IntType()
            elif type(CodeGenVisitor.check_type) is FloatType:
                if type(lt) == type(rt):
                    if type(lt) is FloatType:
                        return lc + rc + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()
                    else:
                        return lc + self.emit.emitI2F(frame) + rc + self.emit.emitI2F(frame) + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()
                elif type(lt) is FloatType and type(rt) is IntType:
                    return lc  + rc + self.emit.emitI2F(frame) + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()

                elif type(lt) is IntType and type(rt) is FloatType:
                    return lc + self.emit.emitI2F(frame) + rc + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()
            else:
                if type(lt) == type(rt):
                    if type(lt) is IntType:
                        return lc + rc + self.emit.emitADDOP(ast.op, IntType(), frame), IntType()
                    else: 
                        return lc + rc + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()
                    
                elif type(lt) is FloatType and type(rt) is IntType:
                    return lc  + rc + self.emit.emitI2F(frame) + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()

                elif type(lt) is IntType and type(rt) is FloatType:
                    return lc + self.emit.emitI2F(frame) + rc + self.emit.emitADDOP(ast.op, FloatType(), frame), FloatType()

        elif ast.op in ['*','/']:
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            if type(CodeGenVisitor.check_type) is IntType:
                if type(lt) == type(rt) and type(lt) is IntType:
                    return lc + rc + self.emit.emitMULOP(ast.op, IntType(), frame), IntType()
            elif type(CodeGenVisitor.check_type) is FloatType:
                if type(lt) == type(rt):
                    if type(lt) is FloatType:
                        return lc + rc + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
                    else:
                        return lc + self.emit.emitI2F(frame) + rc + self.emit.emitI2F(frame) + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
                elif type(lt) is FloatType and type(rt) is IntType:
                    return lc  + rc + self.emit.emitI2F(frame) + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
                elif type(lt) is IntType and type(rt) is FloatType:
                    return lc + self.emit.emitI2F(frame) + rc + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
            else:
                if type(lt) == type(rt): 
                    if type(lt) is IntType:
                        return lc + rc + self.emit.emitMULOP(ast.op, IntType(), frame), IntType()
                    else:
                        return lc + rc + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
                elif type(lt) is FloatType and type(rt) is IntType:
                    return lc  + rc + self.emit.emitI2F(frame) + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()
                elif type(lt) is IntType and type(rt) is FloatType:
                    return lc + self.emit.emitI2F(frame) + rc + self.emit.emitMULOP(ast.op, FloatType(), frame), FloatType()

        elif ast.op.lower() == 'div':
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            return lc + rc + self.emit.emitDIV(frame) , lt

        elif ast.op.lower() == 'mod':
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            return lc + rc + self.emit.emitMOD(frame) , lt

        elif ast.op in [">", ">=", "<", "<=", "<>", "="]:
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            if type(lt) == type(rt) and type(lt) is IntType:
                return lc + rc + self.emit.emitREOP(ast.op, IntType(), frame) , BoolType()
            if type(lt) == type(rt) and type(lt) is FloatType:
                return lc + rc + self.emit.emitREOP(ast.op, FloatType(), frame) , BoolType()
            elif type(lt) is FloatType and type(rt) is IntType:
                return lc  + rc + self.emit.emitI2F(frame) + self.emit.emitREOP(ast.op, FloatType(), frame), BoolType()
            elif type(lt) is IntType and type(rt) is FloatType:
                return lc + self.emit.emitI2F(frame) + rc + self.emit.emitREOP(ast.op, FloatType(), frame), BoolType()
            else:
                return lc + rc + self.emit.emitREOP(ast.op, BoolType() , frame), BoolType()

        elif ast.op.lower() in ["or", "orelse"]:
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            skip_lb = frame.getNewLabel()
            brach_skip=self.emit.emitIFTRUE(skip_lb,frame)
            slb = self.emit.emitLABEL(skip_lb,frame)
            return lc + self.emit.emitDUP(frame)+ brach_skip + rc + self.emit.emitOROP(frame) + slb, BoolType()

        elif ast.op.lower() in ["and", "andthen"]:
            lc, lt = self.visit(ast.left, Access(frame, o.sym, False, True))
            rc, rt = self.visit(ast.right, Access(frame, o.sym, False, False))
            skip_lb = frame.getNewLabel()
            brach_skip = self.emit.emitIFEQ(skip_lb,frame)
            slb = self.emit.emitLABEL(skip_lb,frame)
            return lc + self.emit.emitDUP(frame)+ brach_skip + rc + self.emit.emitANDOP(frame) + slb,  BoolType()

    def visitUnaryOp (self, ast, o):
        ctxt = o
        frame = ctxt.frame
        body_c, body_t = self.visit(ast.body, Access(frame,o.sym,False,True))

        if ast.op == "-":
            return body_c + self.emit.emitNEGOP(body_t,frame), body_t
        elif ast.op.lower() == "not":
            return body_c + self.emit.emitNOT(body_t,frame), body_t

    def visitIntLiteral(self, ast, o):
        ctxt = o
        frame = ctxt.frame
        return self.emit.emitPUSHICONST(str(ast.value), frame), IntType()

    def visitFloatLiteral(self, ast, o):
        ctxt = o
        frame = ctxt.frame
        return self.emit.emitPUSHFCONST(str(ast.value), frame), FloatType()

    def visitBooleanLiteral(self, ast, o):
        ctxt = o
        frame = ctxt.frame
        return self.emit.emitPUSHICONST(str(ast.value).lower(), frame), BoolType()

    def visitStringLiteral(self,ast, o):
        ctxt = o
        frame = ctxt.frame
        return self.emit.emitPUSHCONST("\""+ast.value+"\"",StringType(),frame), StringType ()

    def visitBreak(self, ast, o):
        ctxt = o
        frame = ctxt.frame
        brk = frame.getBreakLabel()
        return self.emit.printout(self.emit.emitGOTO(brk,frame)), VoidType()
        
    def visitContinue(self, ast, o):
        ctxt = o
        frame =ctxt.frame
        conlb=frame.getContinueLabel()
        return self.emit.printout(self.emit.emitGOTO(conlb,frame)), VoidType()
        
    def visitReturn (self, ast, o):
        ctxt = o
        frame = ctxt.frame
        if ast.expr is not None:
            content, typ = self.visit(ast.expr,Access(o.frame,o.sym,False,True))
            self.emit.printout(content)
            self.emit.printout(self.emit.emitRETURN(typ,frame))
            return (content, typ)
        else:
            return ("",VoidType())
            