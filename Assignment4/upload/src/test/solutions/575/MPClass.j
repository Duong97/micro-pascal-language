.source MPClass.java
.class public MPClass
.super java.lang.Object
.field static z Z

.method public static sum(II)I
.var 0 is a I from Label0 to Label1
.var 1 is b I from Label0 to Label1
.var 2 is result I from Label0 to Label1
Label0:
	iload_0
	iload_1
	iadd
	dup
	istore_2
	pop
	iload_2
	ireturn
Label1:
	return
.limit stack 2
.limit locals 3
.end method

.method public static mul(II)I
.var 0 is a I from Label0 to Label1
.var 1 is b I from Label0 to Label1
.var 2 is result I from Label0 to Label1
Label0:
	iload_0
	iload_1
	imul
	dup
	istore_2
	pop
	iload_2
	ireturn
Label1:
	return
.limit stack 2
.limit locals 3
.end method

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
.var 1 is a I from Label0 to Label1
.var 2 is b I from Label0 to Label1
.var 3 is c F from Label0 to Label1
Label0:
	iconst_2
	dup
	istore_1
	pop
	iconst_3
	dup
	istore_2
	pop
	iload_1
	iload_2
	if_icmple Label2
	iconst_1
	goto Label3
Label2:
	iconst_0
Label3:
	ifeq Label4
	iload_1
	iload_2
	invokestatic MPClass/sum(II)I
	iload_1
	iload_2
	invokestatic MPClass/sum(II)I
	i2f
	dup
	fstore_3
	pop
	goto Label5
Label4:
	iload_1
	iload_2
	invokestatic MPClass/mul(II)I
	iload_1
	iload_2
	invokestatic MPClass/mul(II)I
	i2f
	dup
	fstore_3
	pop
Label5:
	fload_3
	invokestatic io/putFloat(F)V
Label1:
	return
.limit stack 9
.limit locals 4
.end method

.method public <init>()V
.var 0 is this LMPClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method
