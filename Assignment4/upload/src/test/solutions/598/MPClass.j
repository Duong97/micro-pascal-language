.source MPClass.java
.class public MPClass
.super java.lang.Object

.method public static ifBiggerAndDivBy2(II)Z
.var 0 is a I from Label0 to Label1
.var 1 is b I from Label0 to Label1
Label0:
	iload_0
	iload_1
	if_icmple Label2
	iconst_1
	goto Label3
Label2:
	iconst_0
Label3:
	dup
	ifeq Label6
	iload_0
	iconst_2
	irem
	iconst_0
	if_icmpne Label4
	iconst_1
	goto Label5
Label4:
	iconst_0
Label5:
	iand
Label6:
	ireturn
Label1:
	return
.limit stack 8
.limit locals 2
.end method

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
Label0:
	iconst_5
	iconst_2
	invokestatic MPClass/ifBiggerAndDivBy2(II)Z
	iconst_5
	iconst_2
	invokestatic MPClass/ifBiggerAndDivBy2(II)Z
	iconst_5
	iconst_2
	invokestatic MPClass/ifBiggerAndDivBy2(II)Z
	invokestatic io/putBool(Z)V
Label1:
	return
.limit stack 14
.limit locals 1
.end method

.method public <init>()V
.var 0 is this LMPClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method
