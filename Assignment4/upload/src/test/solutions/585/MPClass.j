.source MPClass.java
.class public MPClass
.super java.lang.Object

.method public static getPower(II)I
.var 0 is a I from Label0 to Label1
.var 1 is b I from Label0 to Label1
.var 2 is i I from Label0 to Label1
.var 3 is c I from Label0 to Label1
Label0:
	iconst_1
	dup
	istore_3
	pop
	iconst_0
	dup
	istore_2
	pop
Label4:
	iload_2
	iload_1
	if_icmpgt Label5
	iconst_1
	goto Label6
Label5:
	iconst_0
Label6:
	ifeq Label3
	iload_3
	iload_0
	imul
	dup
	istore_3
	pop
Label2:
	iload_2
	iconst_1
	iadd
	dup
	istore_2
	pop
	goto Label4
Label3:
	iload_3
	ireturn
Label1:
	return
.limit stack 6
.limit locals 4
.end method

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
.var 1 is a I from Label0 to Label1
Label0:
	iconst_3
	iconst_5
	invokestatic MPClass/getPower(II)I
	iconst_3
	iconst_5
	invokestatic MPClass/getPower(II)I
	dup
	istore_1
	pop
	iload_1
	invokestatic io/putIntLn(I)V
Label1:
	return
.limit stack 8
.limit locals 2
.end method

.method public <init>()V
.var 0 is this LMPClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method
