import unittest
from TestUtils import TestCodeGen
from AST import *


class CheckCodeGenSuite(unittest.TestCase):
    
    def test_print_an_int(self):
        input = """
        procedure main();
        begin
            putInt(1);
        end
        
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,500))

    def test_print_an_float(self):
        input = """
        procedure main();
        begin
            putFloat(1.3);
        end

        """
        expect = "1.3"
        self.assertTrue(TestCodeGen.test(input,expect,501))

    def test_print_another_float(self):
        input = """
        procedure main();
        begin
            putFloatLn(1);
        end

        """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,502))

    def test_print_a_string(self):
        input = """
        procedure main();
        begin
            putString("abc");
        end

        """
        expect = "abc"
        self.assertTrue(TestCodeGen.test(input,expect,503))

    def test_print_a_boolean(self):
        input = """
        procedure main();
        begin
            putBool(tRue);
        end

        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,504))

    def test_print_a_global_int_variable_without_assign(self):
        input = """
        var a: integer;
        procedure main();
        begin
            putInt(a);
        end

        """
        expect = "0"
        self.assertTrue(TestCodeGen.test(input,expect,505))

    def test_print_a_global_float_variable_without_assign(self):
        input = """
        var a: reaL;
        procedure main();
        begin
            putFloat(a);
        end

        """
        expect = "0.0"
        self.assertTrue(TestCodeGen.test(input,expect,506))

    def test_print_a_global_string_variable_without_assign(self):
        input = """
        var a: string;
        procedure main();
        begin
            putString(a);
        end

        """
        expect = "null"
        self.assertTrue(TestCodeGen.test(input,expect,507))

    def test_print_a_global_boolean_variable_without_assign(self):
        input = """
        var a: boolean;
        procedure main();
        begin
            putBool(a);
        end

        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,508))

    def test_print_another_global_int(self):
        input = """
        var a,b: integer;
        procedure main();
        begin
            a := b;
            putInt(a);
        end

        """
        expect = "0"
        self.assertTrue(TestCodeGen.test(input,expect,509))

    def test_print_another_global_float_with_assign_by_a_integer_number(self):
        input = """
        var a: real;
        procedure main();
        begin
            a := 1;
            putFloat(a);
        end

        """
        expect = "1.0"
        self.assertTrue(TestCodeGen.test(input,expect,510))

    def test_print_a_negative_integer(self):
        input = """
        procedure main();
        begin
            
            putInt(-1);
        end

        """
        expect = "-1"
        self.assertTrue(TestCodeGen.test(input,expect,511))

    def test_print_a_negative_float(self):
        input = """
        procedure main();
        begin
            
            putFloat(-12.11);
        end

        """
        expect = "-12.11"
        self.assertTrue(TestCodeGen.test(input,expect,512))

    def test_print_a_not_boolean(self):
        input = """
        procedure main();
        begin

            putBool(nOt False);
        end

        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,513))

    def test_print_a_assigned_int_varible(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 1;

            putInt(a);
        end

        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,514))

    def test_print_a_assigned_float_varible(self):
        input = """
        procedure main();
        var a: real;
        begin
            a := 1.2;

            putFloat(a);
        end

        """
        expect = "1.2"
        self.assertTrue(TestCodeGen.test(input,expect,515))

    def test_print_another_assigned_float_varible(self):
        input = """
        procedure main();
        var a: real;
        begin
            a := 1;

            putFloatLn(a);
        end

        """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,516))

    def test_print_a_assigned_string_varible(self):
        input = """
        procedure main();
        var a: string;
        begin
            a := "haha";

            putString(a);
        end

        """
        expect = "haha"
        self.assertTrue(TestCodeGen.test(input,expect,517))

    def test_print_another_assigned_string_varible(self):
        input = """
        procedure main();
        var a, b: string;
        begin
            b := "haha";
            a := b;

            putString(a);
        end

        """
        expect = "haha"
        self.assertTrue(TestCodeGen.test(input,expect,518))

    def test_print_result_of_a_more_complex_assignment_for_int(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
            a := b := 1;

            putInt(b);
        end

        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,519))

    def test_print_result_of_another_complex_assignment_for_float(self):
        input = """
        procedure main();
        var a, b: real;
        begin
            b := 2;
            a := b := b + 1;

            putFloatLn(a);
            putFloatLn(b);
        end

        """
        expect = "3.0\n3.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,520))

    def test_print_result_of_another_complex_assignment(self):
        input = """
        procedure main();
        var b: real; a: integer;
        begin
            b := a := 1;
            putFloatLn(b);
           
        end

        """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,521))

    def test_print_a_result_of_simple_binary_expr_of_plus_and_between_int_int(self):
        input = """
        procedure main();
        begin
            putIntLn(3+2);
        end

        """
        expect = "5\n"
        self.assertTrue(TestCodeGen.test(input,expect,522))

    def test_print_a_result_of_more_complex_binary_expr_of_plus_between_int_int(self):
        input = """
        procedure main();
        begin
            putIntLn(1+2+1+1);
        end

        """
        expect = "5\n"
        self.assertTrue(TestCodeGen.test(input,expect,523))

    def test_print_a_result_of_simple_binary_expr_of_minus_between_int(self):
        input = """
        procedure main();
        begin
            putIntLn(3-1);
        end

        """
        expect = "2\n"
        self.assertTrue(TestCodeGen.test(input,expect,524))

    def test_print_a_result_of_more_complex_binary_expr_of_minus_between_int_int(self):
        input = """
        procedure main();
        begin
            putIntLn(17-2-4);
        end

        """
        expect = "11\n"
        self.assertTrue(TestCodeGen.test(input,expect,525))

    def test_print_a_result_of_simple_binary_expr_of_plus_between_float_int(self):
        input = """
        procedure main();
        begin
            putFloat(3.1+1);
        end

        """
        expect = "4.1"
        self.assertTrue(TestCodeGen.test(input,expect,526))

    def test_print_a_result_of_more_complex_binary_expr_of_plus_between_float_int(self):
        input = """
        procedure main();
        begin
            putFloat(3.1+1+4.4+6);
        end

        """
        expect = "14.5"
        self.assertTrue(TestCodeGen.test(input,expect,527))

    def test_print_a_result_of_simple_binary_expr_of_minus_between_float_int(self):
        input = """
        procedure main();
        begin
            putFloat(3-1.2);
        end

        """
        expect = "1.8"
        self.assertTrue(TestCodeGen.test(input,expect,528))

    def test_print_a_result_of_more_complex_binary_expr_of_plus_minus_between_int(self):
        input = """
        procedure main();
        begin
            putInt(4 - 2 + 1);
        end

        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,529))

    def test_print_a_result_of_more_complex_binary_expr_of_plus_minus_between_float_int(self):
        input = """
        procedure main();
        begin
            putFloat(4-2+1+2.3+5);
        end

        """
        expect = "10.3"
        self.assertTrue(TestCodeGen.test(input,expect,530))

    def test_print_a_result_of_binary_expr_of_mul_between_int(self):
        input = """
        procedure main();
        begin
            putInt(4*2*3);
        end

        """
        expect = "24"
        self.assertTrue(TestCodeGen.test(input,expect,531))

    def test_print_a_result_of_binary_expr_of_divide_between_int(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 2;
            putInt(42/a/3);
        end

        """
        expect = "7"
        self.assertTrue(TestCodeGen.test(input,expect,532))

    def test_print_a_result_of_binary_expr_of_divide_between_float(self):
        input = """
        procedure main();
        var b: rEaL;
        begin
            b := 36.6;
            putFloat(b/3/2);
        end

        """
        expect = "6.1"
        self.assertTrue(TestCodeGen.test(input,expect,533))

    def test_print_a_result_of_more_complex_binary_expr_of_mul_divide_between_int(self):
        input = """
        procedure main();
        begin
            putInt(36/3/2*4);
        end

        """
        expect = "24"
        self.assertTrue(TestCodeGen.test(input,expect,534))

    def test_print_a_result_of_more_complex_binary_expr_of_mul_divide_between_float(self):
        input = """
        procedure main();
        begin
            putFloat(2*3.5*2/6*9);
        end

        """
        expect = "21.0"
        self.assertTrue(TestCodeGen.test(input,expect,535))

    def test_print_a_result_of_more_complex_binary_expr_of_add_minus_mul_divide_between_int_float(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
            a := b := 2;
            putFloat(a*3.5*b/6*9);
        end

        """
        expect = "21.0"
        self.assertTrue(TestCodeGen.test(input,expect,536))

    def test_print_a_result_of_a_simple_binary_expr_of_MOD(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 5;
            putInt(5 moD 2);
        end

        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,537))

    def test_print_a_result_of_binary_expr_of_MOD_and_other_binary_ops(self):
        input = """
        procedure main();
        var a, b: integer;
            c: real;
        begin
            a := 13;
            b := 3;
            c := a mod b + 1.0/2; 
            putFloat(c);
        end

        """
        expect = "1.5"
        self.assertTrue(TestCodeGen.test(input,expect,538))

    def test_print_a_result_of_more_complex_binary_expr_of_DIV_MOD_and_other_binary_ops(self):
        input = """
        procedure main();
        var a, b: integer;
            c: real;
        begin
            a := 13;
            b := 3;
            c := (a Div b + 5) MoD 4;
            putFloat(c);
        end

        """
        expect = "1.0"
        self.assertTrue(TestCodeGen.test(input,expect,539))

    def test_print_another_result_of_more_complex_binary_expr_of_DIV_MOD_and_other_binary_ops(self):
        input = """
        procedure main();
        var a, b: integer;
            c: real;
        begin
            a := 13;
            b := 3;
            c := (a Div b + 5) MoD 4;
            putFloat(c);
        end

        """
        expect = "1.0"
        self.assertTrue(TestCodeGen.test(input,expect,540))

    def test_print_a_result_of_a_more_complex_simple_binary_expr_of_MOD_and_other_binary_ops(self):
        input = """
        procedure main();
        var a, b: integer;
            c: real;
        begin
            a := 13;
            b := 3;
            c := (a mod b + 5) mod 5;
            putFloat(c);
        end

        """
        expect = "1.0"
        self.assertTrue(TestCodeGen.test(input,expect,541))

    def test_print_a_result_of_simple_binary_expr_of_LargerThan_between_int(self):
        input = """
        var a: integer;
        procedure main();
        var b: integer;
        begin
            a := b := 2;
            b := 3 * a - 3;
            putBool(a < b);
        end

        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,542))

    def test_print_a_result_of_simple_binary_expr_of_AND_between_simple_binary_expressions(self):
        input = """
        var a: real;
        procedure main();
        var b: integer;
        begin
            a := b := 2 - 1;
            putBool((a<b+1.2) and (a+3>b));
        end

        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,543))

    def test_print_a_result_of_a_more_complex_expr_of_AND_between_element_of_bool_type(self):
        input = """
        var a, d: boolean;
        procedure main();
        var b, c: integer;
        begin
            a := trUe;
            b := 4;
            c := 2*b - 3;
            putBool((b>2) and (c<10) and a and d);
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,544))

    def test_print_another_result_of_a_more_complex_expr_of_AND_between_element_of_bool_type(self):
        input = """
        
        procedure main();
        var a: real; b: integer;
        begin
           a := 0;
           b := 1;
           if (a >= b and then b < 0) then putInt(1);

        end
        """
        expect = ""
        self.assertTrue(TestCodeGen.test(input,expect,545))

    def test_print_a_result_of_simple_binary_expr_of_OR_between_simple_binary_expressions(self):
        input = """
        
        procedure main();
        var a, b: integer;
        begin
           b := a := 1;
           a := b := 2*3 - a;
           putBool((a+1>2) or (b-1<3));

        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,546))

    def test_print_a_result_of_a_more_complex_expr_of_OR_between_element_of_bool_type(self):
        input = """
        var a: boolean;
        procedure main();
        var b, c: integer;
        begin
           a := true;
           b := 4;
           c := 2*b-3;
           putBool((b<2) or (c<10) or a);
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,547))

    def test_print_another_result_of_a_more_complex_expr_of_OR_between_element_of_bool_type(self):
        input = """
        var a: boolean;
        procedure main();
        var b, c: integer;
        begin
           a := true;
           b := 4;
           c := 2*b-3;
           putBool((b<2) or else (c<10) or else a);
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,548))

    def test_print_another_result_of_a_more_complex_expr_of_OR_AND_between_element_of_bool_type(self):
        input = """
        var a: boolean;
        procedure main();
        var b, c: integer;
        begin
           a := false;
           b := 4;
           c := 2*b-3;
           putBool((b<2) or else (c<10) AND a);
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,549))

    def test_print_result_of_simple_binary_expr_of_EQUAL(self):
        input = """
        
        procedure main();
        var b, a: integer;
        begin
           a := 1;
           b := 3*a - 1;
           putBool(b = 2);
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,550))

    def test_print_result_of_more_complex_binary_expr_of_EQUAL_with_other_binary_operations(self):
        input = """
        
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 3*a - 1;
           putBool((b = 2) and (b mod 2 = 0));
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,551))

    def test_print_result_of_simple_binary_expr_of_DIFFERENT(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 3*a - 1;
           putBool(b <> 2);
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,552))

    def test_print_result_of_more_complex_simple_binary_expr_of_DIFFERENT_with_other_binary_operations(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 3*a - 1;
           putBool((b <> 1) anD (b <> 3));
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,553))

    def test_print_result_of_a_complex_binary_binary_expr_of_DIFFERENT_and_EQUAL_and_other_binary_operations(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 2;
           putBool( (b <> 1) = (a <> 1));
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,554))

    def test_print_result_of_another_a_complex_binary_binary_expr_of_DIFFERENT_and_EQUAL_and_other_binary_operations_1(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 2;
           putBool( (b <> 1) <> (a <> 1));
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,555))

    def test_print_result_of_another_a_complex_binary_binary_expr_of_DIFFERENT_and_EQUAL_and_other_binary_operations_2(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
           a := 1;
           b := 2;
           putBool( (b <> 1) <> (a <> 1));
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,556))

    def test_print_1_using_a_function_call_with_no_input(self):
        input = """
        procedure print1 ();
        begin 
            putInt(1);
        end

        procedure main();
        begin
           print1();
        end
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,557))

    def test_print_sum_of_2_number_using_a_function_call_with_no_return(self):
        input = """
        procedure printSum (a: integer; b: integer);
        begin
            putInt(a+b);
        end

        procedure main ();
        begin
            printSum(3,4);
        end
        """
        expect = "7"
        self.assertTrue(TestCodeGen.test(input,expect,558))

    def test_print_result_of_a_sum_of_interger_using_function_return_result(self):
        input = """
        function sum(a: integer;b: integer): integer;
        begin
            return a+b;
        end
        procedure main ();
        var a, b: integer;
        begin
            a := b := 1;
            b := a+1;
            putInt(sum(a,b));
        end
        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,559))

    def test_print_result_of_a_multiplication_of_float_using_function(self):
        input = """
        procedure main ();
        var a, b: real;
        begin
            b := 3;
            a := mul(1.5,2.0);
            putFloat(mul(b,a));
        end

        function mul(a: real; b: real): real;
        begin
            return a*b;
        end
        """
        expect = "9.0"
        self.assertTrue(TestCodeGen.test(input,expect,560))

    def test_print_result_of_test_using_integer_into_float_input_function(self):
        input = """
        function x2Float(a: rEAL): reaL;
        var i: integer;
        begin
            i := 2;
            return a*i;
        end

        procedure main();
        var a: integer;
        begin
            a := 3;
            putFloat(x2Float(a));
        end
        """
        expect = "6.0"
        self.assertTrue(TestCodeGen.test(input,expect,561))

    def test_print_a_first_element_of_array_that_is_assigned_through_calling_function(self):
        input = """
        function foo(): integer;
        var i: integer;
        begin
            i := 2;
            return i;
        end

        procedure main();
        begin
            
            putInt(foo());
        end
        """
        expect = "2"
        self.assertTrue(TestCodeGen.test(input,expect,562))

    def test_print_another_first_element_of_array_that_is_assigned_through_calling_function(self):
        input = """
        function foo(i: real): real;
        begin
            return i;
        end
        var a: integer;
        procedure main();
        begin
            a := 3;
            putFloat(foo(3));
        end
        """
        expect = "3.0"
        self.assertTrue(TestCodeGen.test(input,expect,563))

    def test_print_result_of_test_using_a_result_of_a_function_as_input_to_call_other_function(self):
        input = """
        function add(a: real): real;
        begin
            return a+1;
        end
        procedure main();
        var a: integer;
        begin
            a := 3;
            putFloatLn(add(add(3)));
        end
        """
        expect = "5.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,564))

    def test_print_list_of_smaller_numbers_than_the_input_value_using_a_simple_recursive_function(self):
        input = """
        procedure printNum(a: integer);
        begin
            if (a>0) then
            begin
                putInt(a);
                a := a-1;
                printNum(a);
            end
        end

        procedure main();
        begin
            printNum(7);
        end
        """
        expect = "7654321"
        self.assertTrue(TestCodeGen.test(input,expect,565))

    def test_print_result_of_a_recursive_function_call(self):
        input = """
        procedure printInt (n: integer);
        begin
            if(n > 0) then printInt(n - 1);
            putInt(n);
        end
        procedure main(); 
        begin
            printInt(9);
        end
        """
        expect = "0123456789"
        self.assertTrue(TestCodeGen.test(input,expect,566))

    def test_print_result_of_sum_of_two_return_result_of_call_expr(self):
        input = """
        function mul(a: integer; b: integer): integer;
        begin
            return a*b;
        end

        procedure main();
        begin
            putInt(mul(2,3)+mul(4,5));
        end
        """
        expect = "26"
        self.assertTrue(TestCodeGen.test(input,expect,567))

    def test_print_result_of_and_of_two_return_result_of_call_expr(self):
        input = """
        function bigger(a: integer; b: integer): boolean;
        begin
            return a>b;
        end

        procedure main();
        begin
            putBool(bigger(4,3) and bigger(2,5));
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,568))

    def test_print_result_of_or_of_two_return_result_of_call_expr(self):
        input = """
        function divBy2(a: integer): boolean;
        begin
            return (a mod 2) = 0;
        end

        procedure main();
        begin
            putBool (divBy2(5) or divBy2(4));
        end
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,569))

    def test_print_value_of_integer_variable_inside_the_With(self):
        input = """
        var b: real;
        procedure main();
        var a: integer;
        begin
            a := 3;
            with
                b: integer;
            do
            begin
                b := a;
                putIntLn(b);
            end
            putFloatLn(b);
        end
        """
        expect = "3\n0.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,570))

    def test_print_value_of_float_variable_inside_the_With(self):
        input = """
        var a, b: integer;
        procedure main();
        begin
            a := 4;
            with
                b: real;
            do
            begin
                b := a;
                putFloat(b);
            end
        end
        """
        expect = "4.0"
        self.assertTrue(TestCodeGen.test(input,expect,571))

    def test_print_value_of_variable_inside_the_another_With(self):
        input = """
        procedure main();
        var a: integer;
        begin
            witH
                b: integer;
            do
            begin
                b := 12;
                wiTh
                    a: real;
                do
                begin
                    a := b;
                    putFloat(a);
                end
            end
        end
        """
        expect = "12.0"
        self.assertTrue(TestCodeGen.test(input,expect,572))

    def test_print_result_of_assigned_varible_using_a_if_stmt_without_else(self):
        input = """
        var a: integer;
        procedure main();
        begin
            if((a < 3) and (a = 0)) then a := 2;
            putInt(a);
        end
        """
        expect = "2"
        self.assertTrue(TestCodeGen.test(input,expect,573))

    def test_print_result_of_assigned_variable_using_a_more_complex_if_stmt_without_else(self):
        input = """
        var a: integer;
        procedure main();
        begin
            a := 1;
            if((a<3) aNd (a=1) and (a>0)) then a := 5;
            putFloat(a);
        end
        """
        expect = "5.0"
        self.assertTrue(TestCodeGen.test(input,expect,574))

    def test_print_result_of_calling_function_using_a_if_stmt_with_else(self):
        input = """

        procedure main();
        var a,b: integer; c: real;
        begin
            a := 2;
            b := 3;
            if (a > b) then 
                c := sum(a,b);
            else 
                c := mul(a,b);
            putFloat(c);
        end

        function sum (a: integer; b: integer): integer;
        var result: integer;
        begin
            result := a + b;
            return result;
        end

        var z: boolean;

        function mul (a: integer; b: integer): integer;
        var result: integer;
        begin
            result := a * b;
            return result;
        end

        

        

        

        """
        expect = "6.0"
        self.assertTrue(TestCodeGen.test(input,expect,575))

    def test_print_2_result_of_a_more_complex_if_with_else(self):
        input = """
        procedure main();
        var a, b: integer;
        begin
            b := 2;
            a := 2*b-1;
            if ((a<b) and ((a+2*b) >=2)) then 
            begin
                a := b;
                b := 2*a+1;
            end
            else
            begin
                a := 2;
                b := 3;
                b := b + 1;
            end
            putIntLn(a);
            putIntLn(b);
        end
        
        """
        expect = "2\n4\n"
        self.assertTrue(TestCodeGen.test(input,expect,576))

    def test_print_result_varible_modified_with_if_statement_inside_another_if_stmt_without_else(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 4;
            
            if(a>-1) then 
            begin
                if(a>1) then
                    if((a mod 2) =0) then
                        a := 3;
            end
            putInt(a);
        end
        
        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,577))

    def test_print_result_varible_modified_with_another_if_statement(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a := 2;
            if (a > 1) then
            begin
                a := a + 1;
                putInt(a);
            end
            else
                putInt(a - 1);
        end
        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,578))

    def test_print_result_of_variable_modified_with_if_statement_inside_a_With_statment(self):
        input = """
        var a: boolean;
        procedure main();
        var b: real;
        begin
            with
            do
            begin
                if (not a) then
                begin
                    b := 1+2.5;
                    putFloat(b);
                end
            end
        end
        """
        expect = "3.5"
        self.assertTrue(TestCodeGen.test(input,expect,579))

    def test_print_result_of_boolean_using_call_expr_to_check_if_the_input_is_odd_number(self):
        input = """
        

        procedure main();
        begin
            putBool( checkIfOdd(12) );
        end

        function checkIfOdd (num: integer): boolean;
        begin
            if ((num mod 2) = 0) then begin return 0; end
            return 1;
        end
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,580))

    def test_print_resul_of_varible_after_modified_with_a_simple_for_stmt(self):
        input = """
        procedure main();
        var a:integer;
        begin
            for a := 0 to 5 do
            begin
                putInt(a);
            end
        end
        """
        expect = "012345"
        self.assertTrue(TestCodeGen.test(input,expect,581))

    def test_print_result_of_variable_after_modified_with_for_stmt_that_contains_if_stmt(self):
        input = """
        procedure main();
        var a, i: integer;
        begin
            a := 1;
            
            for i := 0 to 4 do a := a +2;
        
            putFloat(a);
        end
        """
        expect = "11.0"
        self.assertTrue(TestCodeGen.test(input,expect,582))

    def test_print_result_of_variable_after_modified_with_for_stmt_that_contains_another_for_stmt(self):
        input = """
        procedure main();
        var a, i: integer;
        begin
            for i := 0 to 2 do 
                for a := 0 to 2 do 
                    putInt(a);
        end
        """
        expect = "012012012"
        self.assertTrue(TestCodeGen.test(input,expect,583))

    def test_print_result_of_variable_after_modified_with_for_stmt_inside_a_block_stmt(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 1;
            with
                i, j: integer;
            do
            begin
                for i := 1 to 3 do 
                    for j := 1 to 3 do 
                        a := a + i*j;
            end
            

            
            putInt(a);
        end
        """
        expect = "37"
        self.assertTrue(TestCodeGen.test(input,expect,584))

    def test_print_return_result_of_a_function_that_contain_for_stmtt(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := getPower(3,5);
            putIntLn(a);
        end

        function getPower(a: integer; b: integer): integer;
        var i, c: integer;
        begin
            c := 1;
            for i := 0 to b do c := c*a;
            
            return c;
        end
        """
        expect = "729\n"
        self.assertTrue(TestCodeGen.test(input,expect,585))

    def test_print_result_of_variable_after_modified_with_a_simple_while_stmt(self):
        input = """
        procedure main();
        var a: integer;
        begin
            a := 1;
            while (a < 5) do
            begin
                putInt(a);
                a := a + 1;
            end
        end

        
        """
        expect = "1234"
        self.assertTrue(TestCodeGen.test(input,expect,586))

    def test_print_result_of_variable_after_modified_with_while_stmt_that_contain_another_while_stmt(self):
        input = """
        procedure main();
        var a, b, c: integer;
        begin
            a := 1;
            c := 2;
            while (a < 5) do
            begin
                b := 1;
                while (b < 5) do 
                begin
                    c := c * 2;
                    b := b + 1;
                end
                a := a + 1;
            end
            putInt(c);
            
        end

        
        """
        expect = "131072"
        self.assertTrue(TestCodeGen.test(input,expect,587))

    def test_print_result_of_variable_after_modified_with_a_simple_do_while_stmt_inside_a_block_stmt(self):
        input = """
        procedure main();
        var b, c: integer;
        begin
            c := 0;
            
            with 
                c: integer;
            do
            begin
                c := 2;
                b := 1;
                while (b < 5) do 
                begin
                    c := c * 2;
                    b := b + 1;
                end
                putIntLn(c);
    
            end
            putIntLn(c);
        end
        """
        expect = "32\n0\n"
        self.assertTrue(TestCodeGen.test(input,expect,588))

    def test_print_result_of_a_variable_after_modified_with_a_simple_for_statement_that_contain_break_stmt(self):
        input = """
        procedure main();
        var i: integer;
        begin
            for i := 5 downto 0 do break;
            
            putInt(i);
        end
        """
        expect = "5"
        self.assertTrue(TestCodeGen.test(input,expect,589))

    def test_print_result_of_a_variable_after_modified_with_a_more_complex_for_statement_that_contain_break_stmt(self):
        input = """
        procedure main();
        var i: integer;
        begin
            for i:=0 to 12 do
            begin
                if(i>8) then
                    break;
                else 
                    if(i<5) then 
                        i:=2*i+1;
            end
            putIntLn(i);
        end
        """
        expect = "9\n"
        self.assertTrue(TestCodeGen.test(input,expect,590))

    def test_print_result_of_a_variable_after_modified_with_a_simple_while_statement_that_contain_break_stmt(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a := 1;

            while a <= 10 do
            begin
                a := a + 1;
                break;
            end
            putInt(a);
        end
        """
        expect = "2"
        self.assertTrue(TestCodeGen.test(input,expect,591))

    def test_print_result_of_a_variable_after_modified_with_a_more_complex_while_statement_that_contain_break_stmt(self):
        input = """
        var x: integer;
        procedure main();
        begin
            x := 1;
            while (x<9) do
            begin
                x:=x+1;
                if( ((x mod 2) = 0) and (x>4)) then 
                    break;
            end
            
            putInt(x);
        end
        """
        expect = "6"
        self.assertTrue(TestCodeGen.test(input,expect,592))

    def test_print_result_of_a_variable_after_modified_with_a_more_complex_while_statement_that_contain_break_stmt_inside_a_Withstmt(self):
        input = """
        procedure main();
        var x: integer;
        begin
          
            x:=1;
            while (x<9) do
            begin 
                x:=x+1;
                if (((x mod 2) = 0) and (x>4)) then
                begin
                    x:=x+1;
                    with
                    do
                    begin
                        break;
                    end
                end
            end
            putIntLn(x);
        end
        """
        expect = "7\n"
        self.assertTrue(TestCodeGen.test(input,expect,593))

    def test_print_result_of_a_variable_after_modified_with_a_simple_for_statement_that_contain_continue_stmt(self):
        input = """
        procedure main();
        var x,i: integer;
        begin
            i:=0;
            for x := 1 to 10 do
            begin
                if ( (x mod 2) = 0) then continue;
                i:=i+1;
            end
            putIntLn(i);
        end
        """
        expect = "5\n"
        self.assertTrue(TestCodeGen.test(input,expect,594))

    def test_print_result_of_variable_after_modified_with_a_more_complex_for_statement_that_contain_continue_stmt(self):
        input = """
        procedure main();
        var a:integer;
        begin
            for a:= 1 to 5 do
            begin
                continue;
            end
            putInt(a);
        end

        """
        expect = "6"
        self.assertTrue(TestCodeGen.test(input,expect,595))

    def test_print_result_of_variable_after_modified_with_a_more_complex_while_statement_that_contain_continue_stmt(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a:= 1;

            while a <= 10 do
            begin
                a := a + 1;
                for a := 5 to 12 do continue;
                break;
            end
            putInt(a);
        end


        """
        expect = "13"
        self.assertTrue(TestCodeGen.test(input,expect,596))

    def test_print_the_result_of_return_in_function_that_return_the_result_of_another_function(self):
        input = """
        function getDouble(a: integer): integer;
        begin
            return a*2;
        end

        procedure main();
        begin
            putInt(getMaxDouble(5,2));
        end

        function getMaxDouble(a: integer; b: integer): integer;
        begin
            if(a>b) then
                return getDouble(a);
            return getDouble(b);
        end


        """
        expect = "10"
        self.assertTrue(TestCodeGen.test(input,expect,597))

    def test_print_the_result_of_return_in_function_that_using_binary_operation_AND(self):
        input = """
        function ifBiggerAndDivBy2(a: integer; b: integer): boolean;
        begin
            return ((a>b) and ((a mod 2) = 0));
        end

        procedure main();
        begin
            putBool(ifBiggerAndDivBy2(5,2));
        end

        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,598))

    def test_print_the_result_of_return_in_function_that_using_binary_operation_OR(self):
        input = """
        function ifBiggerAndDivBy2(a: integer; b: integer): boolean;
        begin
            return ((a>b) or ((a mod 2) = 0));
        end

        procedure main();
        begin
            putBool(ifBiggerAndDivBy2(5,2));
        end

        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,599))
        